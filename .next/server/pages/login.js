module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./pages/login/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./pages/login/index.js":
/*!******************************!*\
  !*** ./pages/login/index.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Login; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-bootstrap */ "react-bootstrap");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next/router */ "next/router");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "sweetalert2");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _userContext__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../userContext */ "./userContext.js");
/* harmony import */ var react_google_login__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-google-login */ "react-google-login");
/* harmony import */ var react_google_login__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react_google_login__WEBPACK_IMPORTED_MODULE_6__);

var _jsxFileName = "E:\\batch-92\\capstone3\\budget_tracker\\pages\\login\\index.js";






function Login() {
  const {
    user,
    setUser
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useContext"])(_userContext__WEBPACK_IMPORTED_MODULE_5__["default"]); // console.log(user)

  const {
    0: email,
    1: setEmail
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("");
  const {
    0: password,
    1: setPassword
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("");
  const {
    0: isActive,
    1: setIsActive
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(true);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    if (email !== "" && password !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  function authenticate(e) {
    fetch("http://localhost:8000/api/users/login", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        email: email,
        password: password
      })
    }).then(res => res.json()).then(data => {
      // console.log(data)
      if (data.accessToken) {
        localStorage.setItem("token", data.accessToken);
        fetch("http://localhost:8000/api/users/details", {
          method: "GET",
          headers: {
            Authorization: `Bearer ${data.accessToken}`
          }
        }).then(res => res.json()).then(data => {
          console.log(data);
          localStorage.setItem("email", data.email);
          localStorage.setItem("id", data._id);
          setUser({
            email: data.email,
            id: data._id
          });
        });
        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
          icon: "success",
          title: "Successfully Logged In.",
          text: "Thank you for logging in."
        });
        next_router__WEBPACK_IMPORTED_MODULE_3___default.a.push("/courses");
      } else {
        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
          icon: "error",
          title: "Login Failed.",
          text: "User authentication has failed."
        });
      }
    });
    e.preventDefault();
    setEmail("");
    setPassword("");
  }

  function authenticateGoogleToken(response) {
    console.log(response);
    fetch("http://localhost:8000/api/users/verify-google-id-token", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        tokenId: response.tokenId,
        accessToken: response.accessToken
      })
    }).then(res => res.json()).then(data => {
      console.log(data);

      if (typeof data.accessToken !== "undefined") {
        localStorage.setItem("token", data.accessToken);
        fetch("http://localhost:8000/api/users/details", {
          headers: {
            Authorization: `Bearer ${data.accessToken}`
          }
        }).then(res => res.json()).then(data => {
          localStorage.setItem("email", data.email);
          localStorage.setItem("id", data._id);
          setUser({
            email: data.email,
            id: data._id
          });
          sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
            icon: "success",
            title: "Successfully Login"
          });
          next_router__WEBPACK_IMPORTED_MODULE_3___default.a.push("/profile");
        });
      } else {
        if (data.error === "google-auth-error") {
          sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
            icon: "error",
            title: "Google Authentication Failed"
          });
        } else if (data.error === "login-type-error") {
          sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
            icon: "error",
            title: "Login Failed",
            text: "You may have registered through a different procedure"
          });
        }
      }
    });
  }

  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Container"], {
    className: "m-auto",
    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"], {
      onSubmit: e => authenticate(e),
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Group, {
        controlId: "userEmail",
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Label, {
          children: "Email:"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 146,
          columnNumber: 11
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Control, {
          type: "email",
          placeholder: "Enter Email",
          value: email,
          onChange: e => setEmail(e.target.value),
          required: true
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 147,
          columnNumber: 11
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 145,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Group, {
        controlId: "userPassword",
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Label, {
          children: "Password:"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 156,
          columnNumber: 11
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Control, {
          type: "password",
          placeholder: "Enter Password",
          value: password,
          onChange: e => setPassword(e.target.value),
          required: true
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 157,
          columnNumber: 11
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 155,
        columnNumber: 9
      }, this), isActive ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Button"], {
        variant: "primary",
        type: "submit",
        className: "btn-block",
        children: "Submit"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 166,
        columnNumber: 11
      }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Button"], {
        variant: "primary",
        disabled: true,
        className: " btn-block",
        children: "Submit"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 170,
        columnNumber: 11
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_google_login__WEBPACK_IMPORTED_MODULE_6__["GoogleLogin"], {
        clientId: "683580523595-jj134jrfl2dav33rf6di827q5u1isieq.apps.googleusercontent.com",
        buttonText: "Login Using Google",
        onSuccess: authenticateGoogleToken,
        onFailure: authenticateGoogleToken,
        cookiePolicy: "single_host_origin",
        className: "w-100 text-center my-4 d-flex justify-content-center"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 174,
        columnNumber: 9
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 144,
      columnNumber: 7
    }, this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 143,
    columnNumber: 5
  }, this);
}

/***/ }),

/***/ "./userContext.js":
/*!************************!*\
  !*** ./userContext.js ***!
  \************************/
/*! exports provided: UserProvider, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserProvider", function() { return UserProvider; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

const UserContext = /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createContext();
const UserProvider = UserContext.Provider;
/* harmony default export */ __webpack_exports__["default"] = (UserContext);

/***/ }),

/***/ "next/router":
/*!******************************!*\
  !*** external "next/router" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/router");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "react-bootstrap":
/*!**********************************!*\
  !*** external "react-bootstrap" ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-bootstrap");

/***/ }),

/***/ "react-google-login":
/*!*************************************!*\
  !*** external "react-google-login" ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-google-login");

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react/jsx-dev-runtime");

/***/ }),

/***/ "sweetalert2":
/*!******************************!*\
  !*** external "sweetalert2" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("sweetalert2");

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvbG9naW4vaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vVXNlckNvbnRleHQuanMiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwibmV4dC9yb3V0ZXJcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJyZWFjdFwiIiwid2VicGFjazovLy9leHRlcm5hbCBcInJlYWN0LWJvb3RzdHJhcFwiIiwid2VicGFjazovLy9leHRlcm5hbCBcInJlYWN0LWdvb2dsZS1sb2dpblwiIiwid2VicGFjazovLy9leHRlcm5hbCBcInJlYWN0L2pzeC1kZXYtcnVudGltZVwiIiwid2VicGFjazovLy9leHRlcm5hbCBcInN3ZWV0YWxlcnQyXCIiXSwibmFtZXMiOlsiTG9naW4iLCJ1c2VyIiwic2V0VXNlciIsInVzZUNvbnRleHQiLCJVc2VyQ29udGV4dCIsImVtYWlsIiwic2V0RW1haWwiLCJ1c2VTdGF0ZSIsInBhc3N3b3JkIiwic2V0UGFzc3dvcmQiLCJpc0FjdGl2ZSIsInNldElzQWN0aXZlIiwidXNlRWZmZWN0IiwiYXV0aGVudGljYXRlIiwiZSIsImZldGNoIiwibWV0aG9kIiwiaGVhZGVycyIsImJvZHkiLCJKU09OIiwic3RyaW5naWZ5IiwidGhlbiIsInJlcyIsImpzb24iLCJkYXRhIiwiYWNjZXNzVG9rZW4iLCJsb2NhbFN0b3JhZ2UiLCJzZXRJdGVtIiwiQXV0aG9yaXphdGlvbiIsImNvbnNvbGUiLCJsb2ciLCJfaWQiLCJpZCIsIlN3YWwiLCJmaXJlIiwiaWNvbiIsInRpdGxlIiwidGV4dCIsIlJvdXRlciIsInB1c2giLCJwcmV2ZW50RGVmYXVsdCIsImF1dGhlbnRpY2F0ZUdvb2dsZVRva2VuIiwicmVzcG9uc2UiLCJ0b2tlbklkIiwiZXJyb3IiLCJ0YXJnZXQiLCJ2YWx1ZSIsIlJlYWN0IiwiY3JlYXRlQ29udGV4dCIsIlVzZXJQcm92aWRlciIsIlByb3ZpZGVyIl0sIm1hcHBpbmdzIjoiOztRQUFBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0EsSUFBSTtRQUNKO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7OztRQUdBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwwQ0FBMEMsZ0NBQWdDO1FBQzFFO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0Esd0RBQXdELGtCQUFrQjtRQUMxRTtRQUNBLGlEQUFpRCxjQUFjO1FBQy9EOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQSx5Q0FBeUMsaUNBQWlDO1FBQzFFLGdIQUFnSCxtQkFBbUIsRUFBRTtRQUNySTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDJCQUEyQiwwQkFBMEIsRUFBRTtRQUN2RCxpQ0FBaUMsZUFBZTtRQUNoRDtRQUNBO1FBQ0E7O1FBRUE7UUFDQSxzREFBc0QsK0RBQStEOztRQUVySDtRQUNBOzs7UUFHQTtRQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN4RkE7QUFDQTtBQUVBO0FBRUE7QUFFQTtBQUVBO0FBRWUsU0FBU0EsS0FBVCxHQUFpQjtBQUM5QixRQUFNO0FBQUVDLFFBQUY7QUFBUUM7QUFBUixNQUFvQkMsd0RBQVUsQ0FBQ0Msb0RBQUQsQ0FBcEMsQ0FEOEIsQ0FFOUI7O0FBQ0EsUUFBTTtBQUFBLE9BQUNDLEtBQUQ7QUFBQSxPQUFRQztBQUFSLE1BQW9CQyxzREFBUSxDQUFDLEVBQUQsQ0FBbEM7QUFDQSxRQUFNO0FBQUEsT0FBQ0MsUUFBRDtBQUFBLE9BQVdDO0FBQVgsTUFBMEJGLHNEQUFRLENBQUMsRUFBRCxDQUF4QztBQUVBLFFBQU07QUFBQSxPQUFDRyxRQUFEO0FBQUEsT0FBV0M7QUFBWCxNQUEwQkosc0RBQVEsQ0FBQyxJQUFELENBQXhDO0FBRUFLLHlEQUFTLENBQUMsTUFBTTtBQUNkLFFBQUlQLEtBQUssS0FBSyxFQUFWLElBQWdCRyxRQUFRLEtBQUssRUFBakMsRUFBcUM7QUFDbkNHLGlCQUFXLENBQUMsSUFBRCxDQUFYO0FBQ0QsS0FGRCxNQUVPO0FBQ0xBLGlCQUFXLENBQUMsS0FBRCxDQUFYO0FBQ0Q7QUFDRixHQU5RLEVBTU4sQ0FBQ04sS0FBRCxFQUFRRyxRQUFSLENBTk0sQ0FBVDs7QUFRQSxXQUFTSyxZQUFULENBQXNCQyxDQUF0QixFQUF5QjtBQUN2QkMsU0FBSyxDQUFDLHVDQUFELEVBQTBDO0FBQzdDQyxZQUFNLEVBQUUsTUFEcUM7QUFFN0NDLGFBQU8sRUFBRTtBQUNQLHdCQUFnQjtBQURULE9BRm9DO0FBSzdDQyxVQUFJLEVBQUVDLElBQUksQ0FBQ0MsU0FBTCxDQUFlO0FBQ25CZixhQUFLLEVBQUVBLEtBRFk7QUFFbkJHLGdCQUFRLEVBQUVBO0FBRlMsT0FBZjtBQUx1QyxLQUExQyxDQUFMLENBVUdhLElBVkgsQ0FVU0MsR0FBRCxJQUFTQSxHQUFHLENBQUNDLElBQUosRUFWakIsRUFXR0YsSUFYSCxDQVdTRyxJQUFELElBQVU7QUFDZDtBQUVBLFVBQUlBLElBQUksQ0FBQ0MsV0FBVCxFQUFzQjtBQUNwQkMsb0JBQVksQ0FBQ0MsT0FBYixDQUFxQixPQUFyQixFQUE4QkgsSUFBSSxDQUFDQyxXQUFuQztBQUNBVixhQUFLLENBQUMseUNBQUQsRUFBNEM7QUFDL0NDLGdCQUFNLEVBQUUsS0FEdUM7QUFFL0NDLGlCQUFPLEVBQUU7QUFDUFcseUJBQWEsRUFBRyxVQUFTSixJQUFJLENBQUNDLFdBQVk7QUFEbkM7QUFGc0MsU0FBNUMsQ0FBTCxDQU1HSixJQU5ILENBTVNDLEdBQUQsSUFBU0EsR0FBRyxDQUFDQyxJQUFKLEVBTmpCLEVBT0dGLElBUEgsQ0FPU0csSUFBRCxJQUFVO0FBQ2RLLGlCQUFPLENBQUNDLEdBQVIsQ0FBWU4sSUFBWjtBQUNBRSxzQkFBWSxDQUFDQyxPQUFiLENBQXFCLE9BQXJCLEVBQThCSCxJQUFJLENBQUNuQixLQUFuQztBQUNBcUIsc0JBQVksQ0FBQ0MsT0FBYixDQUFxQixJQUFyQixFQUEyQkgsSUFBSSxDQUFDTyxHQUFoQztBQUVBN0IsaUJBQU8sQ0FBQztBQUNORyxpQkFBSyxFQUFFbUIsSUFBSSxDQUFDbkIsS0FETjtBQUVOMkIsY0FBRSxFQUFFUixJQUFJLENBQUNPO0FBRkgsV0FBRCxDQUFQO0FBSUQsU0FoQkg7QUFrQkFFLDBEQUFJLENBQUNDLElBQUwsQ0FBVTtBQUNSQyxjQUFJLEVBQUUsU0FERTtBQUVSQyxlQUFLLEVBQUUseUJBRkM7QUFHUkMsY0FBSSxFQUFFO0FBSEUsU0FBVjtBQUtBQywwREFBTSxDQUFDQyxJQUFQLENBQVksVUFBWjtBQUNELE9BMUJELE1BMEJPO0FBQ0xOLDBEQUFJLENBQUNDLElBQUwsQ0FBVTtBQUNSQyxjQUFJLEVBQUUsT0FERTtBQUVSQyxlQUFLLEVBQUUsZUFGQztBQUdSQyxjQUFJLEVBQUU7QUFIRSxTQUFWO0FBS0Q7QUFDRixLQS9DSDtBQWlEQXZCLEtBQUMsQ0FBQzBCLGNBQUY7QUFFQWxDLFlBQVEsQ0FBQyxFQUFELENBQVI7QUFDQUcsZUFBVyxDQUFDLEVBQUQsQ0FBWDtBQUNEOztBQUVELFdBQVNnQyx1QkFBVCxDQUFpQ0MsUUFBakMsRUFBMkM7QUFDekNiLFdBQU8sQ0FBQ0MsR0FBUixDQUFZWSxRQUFaO0FBRUEzQixTQUFLLENBQUMsd0RBQUQsRUFBMkQ7QUFDOURDLFlBQU0sRUFBRSxNQURzRDtBQUU5REMsYUFBTyxFQUFFO0FBQ1Asd0JBQWdCO0FBRFQsT0FGcUQ7QUFLOURDLFVBQUksRUFBRUMsSUFBSSxDQUFDQyxTQUFMLENBQWU7QUFDbkJ1QixlQUFPLEVBQUVELFFBQVEsQ0FBQ0MsT0FEQztBQUVuQmxCLG1CQUFXLEVBQUVpQixRQUFRLENBQUNqQjtBQUZILE9BQWY7QUFMd0QsS0FBM0QsQ0FBTCxDQVVHSixJQVZILENBVVNDLEdBQUQsSUFBU0EsR0FBRyxDQUFDQyxJQUFKLEVBVmpCLEVBV0dGLElBWEgsQ0FXU0csSUFBRCxJQUFVO0FBQ2RLLGFBQU8sQ0FBQ0MsR0FBUixDQUFZTixJQUFaOztBQUVBLFVBQUksT0FBT0EsSUFBSSxDQUFDQyxXQUFaLEtBQTRCLFdBQWhDLEVBQTZDO0FBQzNDQyxvQkFBWSxDQUFDQyxPQUFiLENBQXFCLE9BQXJCLEVBQThCSCxJQUFJLENBQUNDLFdBQW5DO0FBRUFWLGFBQUssQ0FBQyx5Q0FBRCxFQUE0QztBQUMvQ0UsaUJBQU8sRUFBRTtBQUNQVyx5QkFBYSxFQUFHLFVBQVNKLElBQUksQ0FBQ0MsV0FBWTtBQURuQztBQURzQyxTQUE1QyxDQUFMLENBS0dKLElBTEgsQ0FLU0MsR0FBRCxJQUFTQSxHQUFHLENBQUNDLElBQUosRUFMakIsRUFNR0YsSUFOSCxDQU1TRyxJQUFELElBQVU7QUFDZEUsc0JBQVksQ0FBQ0MsT0FBYixDQUFxQixPQUFyQixFQUE4QkgsSUFBSSxDQUFDbkIsS0FBbkM7QUFDQXFCLHNCQUFZLENBQUNDLE9BQWIsQ0FBcUIsSUFBckIsRUFBMkJILElBQUksQ0FBQ08sR0FBaEM7QUFFQTdCLGlCQUFPLENBQUM7QUFDTkcsaUJBQUssRUFBRW1CLElBQUksQ0FBQ25CLEtBRE47QUFFTjJCLGNBQUUsRUFBRVIsSUFBSSxDQUFDTztBQUZILFdBQUQsQ0FBUDtBQUtBRSw0REFBSSxDQUFDQyxJQUFMLENBQVU7QUFDUkMsZ0JBQUksRUFBRSxTQURFO0FBRVJDLGlCQUFLLEVBQUU7QUFGQyxXQUFWO0FBSUFFLDREQUFNLENBQUNDLElBQVAsQ0FBWSxVQUFaO0FBQ0QsU0FwQkg7QUFxQkQsT0F4QkQsTUF3Qk87QUFDTCxZQUFJZixJQUFJLENBQUNvQixLQUFMLEtBQWUsbUJBQW5CLEVBQXdDO0FBQ3RDWCw0REFBSSxDQUFDQyxJQUFMLENBQVU7QUFDUkMsZ0JBQUksRUFBRSxPQURFO0FBRVJDLGlCQUFLLEVBQUU7QUFGQyxXQUFWO0FBSUQsU0FMRCxNQUtPLElBQUlaLElBQUksQ0FBQ29CLEtBQUwsS0FBZSxrQkFBbkIsRUFBdUM7QUFDNUNYLDREQUFJLENBQUNDLElBQUwsQ0FBVTtBQUNSQyxnQkFBSSxFQUFFLE9BREU7QUFFUkMsaUJBQUssRUFBRSxjQUZDO0FBR1JDLGdCQUFJLEVBQUU7QUFIRSxXQUFWO0FBS0Q7QUFDRjtBQUNGLEtBcERIO0FBcUREOztBQUVELHNCQUNFLHFFQUFDLHlEQUFEO0FBQVcsYUFBUyxFQUFDLFFBQXJCO0FBQUEsMkJBQ0UscUVBQUMsb0RBQUQ7QUFBTSxjQUFRLEVBQUd2QixDQUFELElBQU9ELFlBQVksQ0FBQ0MsQ0FBRCxDQUFuQztBQUFBLDhCQUNFLHFFQUFDLG9EQUFELENBQU0sS0FBTjtBQUFZLGlCQUFTLEVBQUMsV0FBdEI7QUFBQSxnQ0FDRSxxRUFBQyxvREFBRCxDQUFNLEtBQU47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBREYsZUFFRSxxRUFBQyxvREFBRCxDQUFNLE9BQU47QUFDRSxjQUFJLEVBQUMsT0FEUDtBQUVFLHFCQUFXLEVBQUMsYUFGZDtBQUdFLGVBQUssRUFBRVQsS0FIVDtBQUlFLGtCQUFRLEVBQUdTLENBQUQsSUFBT1IsUUFBUSxDQUFDUSxDQUFDLENBQUMrQixNQUFGLENBQVNDLEtBQVYsQ0FKM0I7QUFLRSxrQkFBUTtBQUxWO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBRkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBREYsZUFXRSxxRUFBQyxvREFBRCxDQUFNLEtBQU47QUFBWSxpQkFBUyxFQUFDLGNBQXRCO0FBQUEsZ0NBQ0UscUVBQUMsb0RBQUQsQ0FBTSxLQUFOO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQURGLGVBRUUscUVBQUMsb0RBQUQsQ0FBTSxPQUFOO0FBQ0UsY0FBSSxFQUFDLFVBRFA7QUFFRSxxQkFBVyxFQUFDLGdCQUZkO0FBR0UsZUFBSyxFQUFFdEMsUUFIVDtBQUlFLGtCQUFRLEVBQUdNLENBQUQsSUFBT0wsV0FBVyxDQUFDSyxDQUFDLENBQUMrQixNQUFGLENBQVNDLEtBQVYsQ0FKOUI7QUFLRSxrQkFBUTtBQUxWO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBRkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBWEYsRUFxQkdwQyxRQUFRLGdCQUNQLHFFQUFDLHNEQUFEO0FBQVEsZUFBTyxFQUFDLFNBQWhCO0FBQTBCLFlBQUksRUFBQyxRQUEvQjtBQUF3QyxpQkFBUyxFQUFDLFdBQWxEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBRE8sZ0JBS1AscUVBQUMsc0RBQUQ7QUFBUSxlQUFPLEVBQUMsU0FBaEI7QUFBMEIsZ0JBQVEsTUFBbEM7QUFBbUMsaUJBQVMsRUFBQyxZQUE3QztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQTFCSixlQThCRSxxRUFBQyw4REFBRDtBQUNFLGdCQUFRLEVBQUMsMEVBRFg7QUFFRSxrQkFBVSxFQUFDLG9CQUZiO0FBR0UsaUJBQVMsRUFBRStCLHVCQUhiO0FBSUUsaUJBQVMsRUFBRUEsdUJBSmI7QUFLRSxvQkFBWSxFQUFFLG9CQUxoQjtBQU1FLGlCQUFTLEVBQUM7QUFOWjtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBOUJGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsVUFERjtBQTJDRCxDOzs7Ozs7Ozs7Ozs7QUN4TEQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBLE1BQU1yQyxXQUFXLGdCQUFHMkMsNENBQUssQ0FBQ0MsYUFBTixFQUFwQjtBQUNPLE1BQU1DLFlBQVksR0FBRzdDLFdBQVcsQ0FBQzhDLFFBQWpDO0FBQ1E5QywwRUFBZixFOzs7Ozs7Ozs7OztBQ0hBLHdDOzs7Ozs7Ozs7OztBQ0FBLGtDOzs7Ozs7Ozs7OztBQ0FBLDRDOzs7Ozs7Ozs7OztBQ0FBLCtDOzs7Ozs7Ozs7OztBQ0FBLGtEOzs7Ozs7Ozs7OztBQ0FBLHdDIiwiZmlsZSI6InBhZ2VzL2xvZ2luLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSByZXF1aXJlKCcuLi9zc3ItbW9kdWxlLWNhY2hlLmpzJyk7XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdHZhciB0aHJldyA9IHRydWU7XG4gXHRcdHRyeSB7XG4gXHRcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG4gXHRcdFx0dGhyZXcgPSBmYWxzZTtcbiBcdFx0fSBmaW5hbGx5IHtcbiBcdFx0XHRpZih0aHJldykgZGVsZXRlIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdO1xuIFx0XHR9XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZ2V0dGVyIH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSBmdW5jdGlvbihleHBvcnRzKSB7XG4gXHRcdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuIFx0XHR9XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG4gXHR9O1xuXG4gXHQvLyBjcmVhdGUgYSBmYWtlIG5hbWVzcGFjZSBvYmplY3RcbiBcdC8vIG1vZGUgJiAxOiB2YWx1ZSBpcyBhIG1vZHVsZSBpZCwgcmVxdWlyZSBpdFxuIFx0Ly8gbW9kZSAmIDI6IG1lcmdlIGFsbCBwcm9wZXJ0aWVzIG9mIHZhbHVlIGludG8gdGhlIG5zXG4gXHQvLyBtb2RlICYgNDogcmV0dXJuIHZhbHVlIHdoZW4gYWxyZWFkeSBucyBvYmplY3RcbiBcdC8vIG1vZGUgJiA4fDE6IGJlaGF2ZSBsaWtlIHJlcXVpcmVcbiBcdF9fd2VicGFja19yZXF1aXJlX18udCA9IGZ1bmN0aW9uKHZhbHVlLCBtb2RlKSB7XG4gXHRcdGlmKG1vZGUgJiAxKSB2YWx1ZSA9IF9fd2VicGFja19yZXF1aXJlX18odmFsdWUpO1xuIFx0XHRpZihtb2RlICYgOCkgcmV0dXJuIHZhbHVlO1xuIFx0XHRpZigobW9kZSAmIDQpICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiYgdmFsdWUgJiYgdmFsdWUuX19lc01vZHVsZSkgcmV0dXJuIHZhbHVlO1xuIFx0XHR2YXIgbnMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIobnMpO1xuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkobnMsICdkZWZhdWx0JywgeyBlbnVtZXJhYmxlOiB0cnVlLCB2YWx1ZTogdmFsdWUgfSk7XG4gXHRcdGlmKG1vZGUgJiAyICYmIHR5cGVvZiB2YWx1ZSAhPSAnc3RyaW5nJykgZm9yKHZhciBrZXkgaW4gdmFsdWUpIF9fd2VicGFja19yZXF1aXJlX18uZChucywga2V5LCBmdW5jdGlvbihrZXkpIHsgcmV0dXJuIHZhbHVlW2tleV07IH0uYmluZChudWxsLCBrZXkpKTtcbiBcdFx0cmV0dXJuIG5zO1xuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IFwiLi9wYWdlcy9sb2dpbi9pbmRleC5qc1wiKTtcbiIsImltcG9ydCB7IHVzZUVmZmVjdCwgdXNlU3RhdGUsIHVzZUNvbnRleHQgfSBmcm9tIFwicmVhY3RcIjtcclxuaW1wb3J0IHsgQ29udGFpbmVyLCBGb3JtLCBCdXR0b24gfSBmcm9tIFwicmVhY3QtYm9vdHN0cmFwXCI7XHJcblxyXG5pbXBvcnQgUm91dGVyIGZyb20gXCJuZXh0L3JvdXRlclwiO1xyXG5cclxuaW1wb3J0IFN3YWwgZnJvbSBcInN3ZWV0YWxlcnQyXCI7XHJcblxyXG5pbXBvcnQgVXNlckNvbnRleHQgZnJvbSBcIi4uLy4uL3VzZXJDb250ZXh0XCI7XHJcblxyXG5pbXBvcnQgeyBHb29nbGVMb2dpbiB9IGZyb20gXCJyZWFjdC1nb29nbGUtbG9naW5cIjtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIExvZ2luKCkge1xyXG4gIGNvbnN0IHsgdXNlciwgc2V0VXNlciB9ID0gdXNlQ29udGV4dChVc2VyQ29udGV4dCk7XHJcbiAgLy8gY29uc29sZS5sb2codXNlcilcclxuICBjb25zdCBbZW1haWwsIHNldEVtYWlsXSA9IHVzZVN0YXRlKFwiXCIpO1xyXG4gIGNvbnN0IFtwYXNzd29yZCwgc2V0UGFzc3dvcmRdID0gdXNlU3RhdGUoXCJcIik7XHJcblxyXG4gIGNvbnN0IFtpc0FjdGl2ZSwgc2V0SXNBY3RpdmVdID0gdXNlU3RhdGUodHJ1ZSk7XHJcblxyXG4gIHVzZUVmZmVjdCgoKSA9PiB7XHJcbiAgICBpZiAoZW1haWwgIT09IFwiXCIgJiYgcGFzc3dvcmQgIT09IFwiXCIpIHtcclxuICAgICAgc2V0SXNBY3RpdmUodHJ1ZSk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBzZXRJc0FjdGl2ZShmYWxzZSk7XHJcbiAgICB9XHJcbiAgfSwgW2VtYWlsLCBwYXNzd29yZF0pO1xyXG5cclxuICBmdW5jdGlvbiBhdXRoZW50aWNhdGUoZSkge1xyXG4gICAgZmV0Y2goXCJodHRwOi8vbG9jYWxob3N0OjgwMDAvYXBpL3VzZXJzL2xvZ2luXCIsIHtcclxuICAgICAgbWV0aG9kOiBcIlBPU1RcIixcclxuICAgICAgaGVhZGVyczoge1xyXG4gICAgICAgIFwiQ29udGVudC1UeXBlXCI6IFwiYXBwbGljYXRpb24vanNvblwiLFxyXG4gICAgICB9LFxyXG4gICAgICBib2R5OiBKU09OLnN0cmluZ2lmeSh7XHJcbiAgICAgICAgZW1haWw6IGVtYWlsLFxyXG4gICAgICAgIHBhc3N3b3JkOiBwYXNzd29yZCxcclxuICAgICAgfSksXHJcbiAgICB9KVxyXG4gICAgICAudGhlbigocmVzKSA9PiByZXMuanNvbigpKVxyXG4gICAgICAudGhlbigoZGF0YSkgPT4ge1xyXG4gICAgICAgIC8vIGNvbnNvbGUubG9nKGRhdGEpXHJcblxyXG4gICAgICAgIGlmIChkYXRhLmFjY2Vzc1Rva2VuKSB7XHJcbiAgICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcInRva2VuXCIsIGRhdGEuYWNjZXNzVG9rZW4pO1xyXG4gICAgICAgICAgZmV0Y2goXCJodHRwOi8vbG9jYWxob3N0OjgwMDAvYXBpL3VzZXJzL2RldGFpbHNcIiwge1xyXG4gICAgICAgICAgICBtZXRob2Q6IFwiR0VUXCIsXHJcbiAgICAgICAgICAgIGhlYWRlcnM6IHtcclxuICAgICAgICAgICAgICBBdXRob3JpemF0aW9uOiBgQmVhcmVyICR7ZGF0YS5hY2Nlc3NUb2tlbn1gLFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgfSlcclxuICAgICAgICAgICAgLnRoZW4oKHJlcykgPT4gcmVzLmpzb24oKSlcclxuICAgICAgICAgICAgLnRoZW4oKGRhdGEpID0+IHtcclxuICAgICAgICAgICAgICBjb25zb2xlLmxvZyhkYXRhKTtcclxuICAgICAgICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImVtYWlsXCIsIGRhdGEuZW1haWwpO1xyXG4gICAgICAgICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwiaWRcIiwgZGF0YS5faWQpO1xyXG5cclxuICAgICAgICAgICAgICBzZXRVc2VyKHtcclxuICAgICAgICAgICAgICAgIGVtYWlsOiBkYXRhLmVtYWlsLFxyXG4gICAgICAgICAgICAgICAgaWQ6IGRhdGEuX2lkLFxyXG4gICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICBTd2FsLmZpcmUoe1xyXG4gICAgICAgICAgICBpY29uOiBcInN1Y2Nlc3NcIixcclxuICAgICAgICAgICAgdGl0bGU6IFwiU3VjY2Vzc2Z1bGx5IExvZ2dlZCBJbi5cIixcclxuICAgICAgICAgICAgdGV4dDogXCJUaGFuayB5b3UgZm9yIGxvZ2dpbmcgaW4uXCIsXHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICAgIFJvdXRlci5wdXNoKFwiL2NvdXJzZXNcIik7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIFN3YWwuZmlyZSh7XHJcbiAgICAgICAgICAgIGljb246IFwiZXJyb3JcIixcclxuICAgICAgICAgICAgdGl0bGU6IFwiTG9naW4gRmFpbGVkLlwiLFxyXG4gICAgICAgICAgICB0ZXh0OiBcIlVzZXIgYXV0aGVudGljYXRpb24gaGFzIGZhaWxlZC5cIixcclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcblxyXG4gICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cclxuICAgIHNldEVtYWlsKFwiXCIpO1xyXG4gICAgc2V0UGFzc3dvcmQoXCJcIik7XHJcbiAgfVxyXG5cclxuICBmdW5jdGlvbiBhdXRoZW50aWNhdGVHb29nbGVUb2tlbihyZXNwb25zZSkge1xyXG4gICAgY29uc29sZS5sb2cocmVzcG9uc2UpO1xyXG5cclxuICAgIGZldGNoKFwiaHR0cDovL2xvY2FsaG9zdDo4MDAwL2FwaS91c2Vycy92ZXJpZnktZ29vZ2xlLWlkLXRva2VuXCIsIHtcclxuICAgICAgbWV0aG9kOiBcIlBPU1RcIixcclxuICAgICAgaGVhZGVyczoge1xyXG4gICAgICAgIFwiQ29udGVudC1UeXBlXCI6IFwiYXBwbGljYXRpb24vanNvblwiLFxyXG4gICAgICB9LFxyXG4gICAgICBib2R5OiBKU09OLnN0cmluZ2lmeSh7XHJcbiAgICAgICAgdG9rZW5JZDogcmVzcG9uc2UudG9rZW5JZCxcclxuICAgICAgICBhY2Nlc3NUb2tlbjogcmVzcG9uc2UuYWNjZXNzVG9rZW4sXHJcbiAgICAgIH0pLFxyXG4gICAgfSlcclxuICAgICAgLnRoZW4oKHJlcykgPT4gcmVzLmpzb24oKSlcclxuICAgICAgLnRoZW4oKGRhdGEpID0+IHtcclxuICAgICAgICBjb25zb2xlLmxvZyhkYXRhKTtcclxuXHJcbiAgICAgICAgaWYgKHR5cGVvZiBkYXRhLmFjY2Vzc1Rva2VuICE9PSBcInVuZGVmaW5lZFwiKSB7XHJcbiAgICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcInRva2VuXCIsIGRhdGEuYWNjZXNzVG9rZW4pO1xyXG5cclxuICAgICAgICAgIGZldGNoKFwiaHR0cDovL2xvY2FsaG9zdDo4MDAwL2FwaS91c2Vycy9kZXRhaWxzXCIsIHtcclxuICAgICAgICAgICAgaGVhZGVyczoge1xyXG4gICAgICAgICAgICAgIEF1dGhvcml6YXRpb246IGBCZWFyZXIgJHtkYXRhLmFjY2Vzc1Rva2VufWAsXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAudGhlbigocmVzKSA9PiByZXMuanNvbigpKVxyXG4gICAgICAgICAgICAudGhlbigoZGF0YSkgPT4ge1xyXG4gICAgICAgICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwiZW1haWxcIiwgZGF0YS5lbWFpbCk7XHJcbiAgICAgICAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJpZFwiLCBkYXRhLl9pZCk7XHJcblxyXG4gICAgICAgICAgICAgIHNldFVzZXIoe1xyXG4gICAgICAgICAgICAgICAgZW1haWw6IGRhdGEuZW1haWwsXHJcbiAgICAgICAgICAgICAgICBpZDogZGF0YS5faWQsXHJcbiAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgIFN3YWwuZmlyZSh7XHJcbiAgICAgICAgICAgICAgICBpY29uOiBcInN1Y2Nlc3NcIixcclxuICAgICAgICAgICAgICAgIHRpdGxlOiBcIlN1Y2Nlc3NmdWxseSBMb2dpblwiLFxyXG4gICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgIFJvdXRlci5wdXNoKFwiL3Byb2ZpbGVcIik7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICBpZiAoZGF0YS5lcnJvciA9PT0gXCJnb29nbGUtYXV0aC1lcnJvclwiKSB7XHJcbiAgICAgICAgICAgIFN3YWwuZmlyZSh7XHJcbiAgICAgICAgICAgICAgaWNvbjogXCJlcnJvclwiLFxyXG4gICAgICAgICAgICAgIHRpdGxlOiBcIkdvb2dsZSBBdXRoZW50aWNhdGlvbiBGYWlsZWRcIixcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICB9IGVsc2UgaWYgKGRhdGEuZXJyb3IgPT09IFwibG9naW4tdHlwZS1lcnJvclwiKSB7XHJcbiAgICAgICAgICAgIFN3YWwuZmlyZSh7XHJcbiAgICAgICAgICAgICAgaWNvbjogXCJlcnJvclwiLFxyXG4gICAgICAgICAgICAgIHRpdGxlOiBcIkxvZ2luIEZhaWxlZFwiLFxyXG4gICAgICAgICAgICAgIHRleHQ6IFwiWW91IG1heSBoYXZlIHJlZ2lzdGVyZWQgdGhyb3VnaCBhIGRpZmZlcmVudCBwcm9jZWR1cmVcIixcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICB9XHJcblxyXG4gIHJldHVybiAoXHJcbiAgICA8Q29udGFpbmVyIGNsYXNzTmFtZT1cIm0tYXV0b1wiPlxyXG4gICAgICA8Rm9ybSBvblN1Ym1pdD17KGUpID0+IGF1dGhlbnRpY2F0ZShlKX0+XHJcbiAgICAgICAgPEZvcm0uR3JvdXAgY29udHJvbElkPVwidXNlckVtYWlsXCI+XHJcbiAgICAgICAgICA8Rm9ybS5MYWJlbD5FbWFpbDo8L0Zvcm0uTGFiZWw+XHJcbiAgICAgICAgICA8Rm9ybS5Db250cm9sXHJcbiAgICAgICAgICAgIHR5cGU9XCJlbWFpbFwiXHJcbiAgICAgICAgICAgIHBsYWNlaG9sZGVyPVwiRW50ZXIgRW1haWxcIlxyXG4gICAgICAgICAgICB2YWx1ZT17ZW1haWx9XHJcbiAgICAgICAgICAgIG9uQ2hhbmdlPXsoZSkgPT4gc2V0RW1haWwoZS50YXJnZXQudmFsdWUpfVxyXG4gICAgICAgICAgICByZXF1aXJlZFxyXG4gICAgICAgICAgLz5cclxuICAgICAgICA8L0Zvcm0uR3JvdXA+XHJcbiAgICAgICAgPEZvcm0uR3JvdXAgY29udHJvbElkPVwidXNlclBhc3N3b3JkXCI+XHJcbiAgICAgICAgICA8Rm9ybS5MYWJlbD5QYXNzd29yZDo8L0Zvcm0uTGFiZWw+XHJcbiAgICAgICAgICA8Rm9ybS5Db250cm9sXHJcbiAgICAgICAgICAgIHR5cGU9XCJwYXNzd29yZFwiXHJcbiAgICAgICAgICAgIHBsYWNlaG9sZGVyPVwiRW50ZXIgUGFzc3dvcmRcIlxyXG4gICAgICAgICAgICB2YWx1ZT17cGFzc3dvcmR9XHJcbiAgICAgICAgICAgIG9uQ2hhbmdlPXsoZSkgPT4gc2V0UGFzc3dvcmQoZS50YXJnZXQudmFsdWUpfVxyXG4gICAgICAgICAgICByZXF1aXJlZFxyXG4gICAgICAgICAgLz5cclxuICAgICAgICA8L0Zvcm0uR3JvdXA+XHJcbiAgICAgICAge2lzQWN0aXZlID8gKFxyXG4gICAgICAgICAgPEJ1dHRvbiB2YXJpYW50PVwicHJpbWFyeVwiIHR5cGU9XCJzdWJtaXRcIiBjbGFzc05hbWU9XCJidG4tYmxvY2tcIj5cclxuICAgICAgICAgICAgU3VibWl0XHJcbiAgICAgICAgICA8L0J1dHRvbj5cclxuICAgICAgICApIDogKFxyXG4gICAgICAgICAgPEJ1dHRvbiB2YXJpYW50PVwicHJpbWFyeVwiIGRpc2FibGVkIGNsYXNzTmFtZT1cIiBidG4tYmxvY2tcIj5cclxuICAgICAgICAgICAgU3VibWl0XHJcbiAgICAgICAgICA8L0J1dHRvbj5cclxuICAgICAgICApfVxyXG4gICAgICAgIDxHb29nbGVMb2dpblxyXG4gICAgICAgICAgY2xpZW50SWQ9XCI2ODM1ODA1MjM1OTUtamoxMzRqcmZsMmRhdjMzcmY2ZGk4MjdxNXUxaXNpZXEuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb21cIlxyXG4gICAgICAgICAgYnV0dG9uVGV4dD1cIkxvZ2luIFVzaW5nIEdvb2dsZVwiXHJcbiAgICAgICAgICBvblN1Y2Nlc3M9e2F1dGhlbnRpY2F0ZUdvb2dsZVRva2VufVxyXG4gICAgICAgICAgb25GYWlsdXJlPXthdXRoZW50aWNhdGVHb29nbGVUb2tlbn1cclxuICAgICAgICAgIGNvb2tpZVBvbGljeT17XCJzaW5nbGVfaG9zdF9vcmlnaW5cIn1cclxuICAgICAgICAgIGNsYXNzTmFtZT1cInctMTAwIHRleHQtY2VudGVyIG15LTQgZC1mbGV4IGp1c3RpZnktY29udGVudC1jZW50ZXJcIlxyXG4gICAgICAgIC8+XHJcbiAgICAgIDwvRm9ybT5cclxuICAgIDwvQ29udGFpbmVyPlxyXG4gICk7XHJcbn1cclxuIiwiaW1wb3J0IFJlYWN0IGZyb20gXCJyZWFjdFwiO1xyXG5jb25zdCBVc2VyQ29udGV4dCA9IFJlYWN0LmNyZWF0ZUNvbnRleHQoKTtcclxuZXhwb3J0IGNvbnN0IFVzZXJQcm92aWRlciA9IFVzZXJDb250ZXh0LlByb3ZpZGVyO1xyXG5leHBvcnQgZGVmYXVsdCBVc2VyQ29udGV4dDtcclxuIiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwibmV4dC9yb3V0ZXJcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwicmVhY3RcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwicmVhY3QtYm9vdHN0cmFwXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlYWN0LWdvb2dsZS1sb2dpblwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWFjdC9qc3gtZGV2LXJ1bnRpbWVcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwic3dlZXRhbGVydDJcIik7Il0sInNvdXJjZVJvb3QiOiIifQ==