module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./pages/charts/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./components/Barchart.js":
/*!********************************!*\
  !*** ./components/Barchart.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return BarCharts; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_chartjs_2__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-chartjs-2 */ "react-chartjs-2");
/* harmony import */ var react_chartjs_2__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_chartjs_2__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-bootstrap */ "react-bootstrap");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! moment */ "moment");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_4__);


var _jsxFileName = "E:\\batch-92\\capstone3\\budget_tracker\\components\\Barchart.js";




function BarCharts() {
  const {
    0: months,
    1: setMonths
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  const {
    0: monthlyIncome,
    1: setMonthlyIncome
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  const {
    0: monthlyExpenses,
    1: setMonthlyExpenses
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  const {
    0: allTransactions,
    1: setAllTransactions
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    fetch("http://localhost:8000/api/users/allTransactions", {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    }).then(res => res.json()).then(data => {
      setAllTransactions(data);
    });
  }, []);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    if (allTransactions.length > 0) {
      let tempMonths = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
      allTransactions.forEach(element => {
        if (!tempMonths.find(month => month === moment__WEBPACK_IMPORTED_MODULE_4___default()(element.recordedOn).format("MMMM"))) {
          tempMonths.push(moment__WEBPACK_IMPORTED_MODULE_4___default()(element.sale_date).format("MMMM"));
        }
      });
      const monthsRef = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
      tempMonths.sort((a, b) => {
        if (monthsRef.indexOf(a) !== -1 && monthsRef.indexOf(b) !== -1) {
          return monthsRef.indexOf(a) - monthsRef.indexOf(b);
        }
      });
      setMonths(tempMonths);
    }
  }, [allTransactions]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    setMonthlyIncome(months.map(month => {
      let income = 0;
      allTransactions.forEach(element => {
        if (moment__WEBPACK_IMPORTED_MODULE_4___default()(element.recordedOn).format("MMMM") === month && element.type === "Income") {
          income += parseInt(element.amount);
        }
      });
      return income;
    }));
  }, [months]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    setMonthlyExpenses(months.map(month => {
      let expense = 0;
      allTransactions.forEach(element => {
        if (moment__WEBPACK_IMPORTED_MODULE_4___default()(element.recordedOn).format("MMMM") === month && element.type === "Expense") {
          expense += parseInt(element.amount);
        }
      });
      return expense;
    }));
  }, [months]);
  const incomeData = {
    labels: months,
    datasets: [{
      label: "Annual Income",
      backgroundColor: "#F0931F",
      borderColor: "white",
      borderWidth: 1,
      hoverBackgroundColor: "#F0931F",
      hoverBorderColor: "#F0931F",
      data: monthlyIncome
    }]
  };
  const expenseData = {
    labels: months,
    datasets: [{
      label: "Annual Expenses",
      backgroundColor: "black",
      borderColor: "white",
      borderWidth: 1,
      hoverBackgroundColor: "black",
      hoverBorderColor: "black",
      data: monthlyExpenses
    }]
  };
  const options = {
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    }
  };
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Row"], {
      className: "mt-5",
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Col"], {
        md: 6,
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h2", {
          className: "text-center",
          children: "Income"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 159,
          columnNumber: 11
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_chartjs_2__WEBPACK_IMPORTED_MODULE_2__["Bar"], {
          data: incomeData,
          options: options
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 160,
          columnNumber: 11
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 158,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Col"], {
        md: 6,
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h2", {
          className: "text-center",
          children: "Expenses"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 163,
          columnNumber: 11
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_chartjs_2__WEBPACK_IMPORTED_MODULE_2__["Bar"], {
          data: expenseData,
          options: options
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 164,
          columnNumber: 11
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 162,
        columnNumber: 9
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 157,
      columnNumber: 7
    }, this)
  }, void 0, false);
}

/***/ }),

/***/ "./components/PieChart.js":
/*!********************************!*\
  !*** ./components/PieChart.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return PieChart; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_chartjs_2__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-chartjs-2 */ "react-chartjs-2");
/* harmony import */ var react_chartjs_2__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_chartjs_2__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-bootstrap */ "react-bootstrap");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! moment */ "moment");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_4__);


var _jsxFileName = "E:\\batch-92\\capstone3\\budget_tracker\\components\\PieChart.js";




function PieChart() {
  const {
    0: totalIncome,
    1: setTotalIncome
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  const {
    0: totalExpenses,
    1: setTotalExpenses
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  const {
    0: allTransactions,
    1: setAllTransactions
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    fetch("http://localhost:8000/api/users/allTransactions", {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    }).then(res => res.json()).then(data => {
      setAllTransactions(data);
    });
  }, []);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    setTotalIncome(allTransactions.map(result => {
      let income = 0;
      allTransactions.forEach(element => {
        if (element.type === "Income") {
          income += parseInt(element.amount);
        }
      });
      return income;
    }));
  }, [allTransactions]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    setTotalExpenses(allTransactions.map(result => {
      let expense = 0;
      allTransactions.forEach(element => {
        if (element.type === "Expense") {
          expense += parseInt(element.amount);
        }
      });
      return expense;
    }));
  }, [allTransactions]);
  const totalIncomeRes = totalIncome[0];
  const totalExpensesRes = totalExpenses[0];
  const data = {
    datasets: [{
      data: [totalIncomeRes, totalExpensesRes],
      backgroundColor: ["#F0931F", "black"]
    }],
    labels: ["Total Income", "Total Expenses"]
  };
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Row"], {
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Col"], {
        md: 6,
        className: "offset-md-3",
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_chartjs_2__WEBPACK_IMPORTED_MODULE_2__["Pie"], {
          data: data
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 70,
          columnNumber: 11
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 69,
        columnNumber: 9
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 68,
      columnNumber: 7
    }, this)
  }, void 0, false);
}

/***/ }),

/***/ "./pages/charts/index.js":
/*!*******************************!*\
  !*** ./pages/charts/index.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Charts; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_Barchart__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../components/Barchart */ "./components/Barchart.js");
/* harmony import */ var _components_PieChart__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../components/PieChart */ "./components/PieChart.js");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-bootstrap */ "react-bootstrap");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__);

var _jsxFileName = "E:\\batch-92\\capstone3\\budget_tracker\\pages\\charts\\index.js";



function Charts() {
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Jumbotron"], {
    className: "text-center",
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h1", {
      children: "Pie Graph"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 8,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_PieChart__WEBPACK_IMPORTED_MODULE_2__["default"], {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 9,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h1", {
      children: "Bar Graph"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 10,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_Barchart__WEBPACK_IMPORTED_MODULE_1__["default"], {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 11,
      columnNumber: 7
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 7,
    columnNumber: 5
  }, this);
}

/***/ }),

/***/ "moment":
/*!*************************!*\
  !*** external "moment" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("moment");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "react-bootstrap":
/*!**********************************!*\
  !*** external "react-bootstrap" ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-bootstrap");

/***/ }),

/***/ "react-chartjs-2":
/*!**********************************!*\
  !*** external "react-chartjs-2" ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-chartjs-2");

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react/jsx-dev-runtime");

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vY29tcG9uZW50cy9CYXJjaGFydC5qcyIsIndlYnBhY2s6Ly8vLi9jb21wb25lbnRzL1BpZUNoYXJ0LmpzIiwid2VicGFjazovLy8uL3BhZ2VzL2NoYXJ0cy9pbmRleC5qcyIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJtb21lbnRcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJyZWFjdFwiIiwid2VicGFjazovLy9leHRlcm5hbCBcInJlYWN0LWJvb3RzdHJhcFwiIiwid2VicGFjazovLy9leHRlcm5hbCBcInJlYWN0LWNoYXJ0anMtMlwiIiwid2VicGFjazovLy9leHRlcm5hbCBcInJlYWN0L2pzeC1kZXYtcnVudGltZVwiIl0sIm5hbWVzIjpbIkJhckNoYXJ0cyIsIm1vbnRocyIsInNldE1vbnRocyIsInVzZVN0YXRlIiwibW9udGhseUluY29tZSIsInNldE1vbnRobHlJbmNvbWUiLCJtb250aGx5RXhwZW5zZXMiLCJzZXRNb250aGx5RXhwZW5zZXMiLCJhbGxUcmFuc2FjdGlvbnMiLCJzZXRBbGxUcmFuc2FjdGlvbnMiLCJ1c2VFZmZlY3QiLCJmZXRjaCIsImhlYWRlcnMiLCJBdXRob3JpemF0aW9uIiwibG9jYWxTdG9yYWdlIiwiZ2V0SXRlbSIsInRoZW4iLCJyZXMiLCJqc29uIiwiZGF0YSIsImxlbmd0aCIsInRlbXBNb250aHMiLCJmb3JFYWNoIiwiZWxlbWVudCIsImZpbmQiLCJtb250aCIsIm1vbWVudCIsInJlY29yZGVkT24iLCJmb3JtYXQiLCJwdXNoIiwic2FsZV9kYXRlIiwibW9udGhzUmVmIiwic29ydCIsImEiLCJiIiwiaW5kZXhPZiIsIm1hcCIsImluY29tZSIsInR5cGUiLCJwYXJzZUludCIsImFtb3VudCIsImV4cGVuc2UiLCJpbmNvbWVEYXRhIiwibGFiZWxzIiwiZGF0YXNldHMiLCJsYWJlbCIsImJhY2tncm91bmRDb2xvciIsImJvcmRlckNvbG9yIiwiYm9yZGVyV2lkdGgiLCJob3ZlckJhY2tncm91bmRDb2xvciIsImhvdmVyQm9yZGVyQ29sb3IiLCJleHBlbnNlRGF0YSIsIm9wdGlvbnMiLCJzY2FsZXMiLCJ5QXhlcyIsInRpY2tzIiwiYmVnaW5BdFplcm8iLCJQaWVDaGFydCIsInRvdGFsSW5jb21lIiwic2V0VG90YWxJbmNvbWUiLCJ0b3RhbEV4cGVuc2VzIiwic2V0VG90YWxFeHBlbnNlcyIsInJlc3VsdCIsInRvdGFsSW5jb21lUmVzIiwidG90YWxFeHBlbnNlc1JlcyIsIkNoYXJ0cyJdLCJtYXBwaW5ncyI6Ijs7UUFBQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBLElBQUk7UUFDSjtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBOzs7UUFHQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMENBQTBDLGdDQUFnQztRQUMxRTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLHdEQUF3RCxrQkFBa0I7UUFDMUU7UUFDQSxpREFBaUQsY0FBYztRQUMvRDs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0EseUNBQXlDLGlDQUFpQztRQUMxRSxnSEFBZ0gsbUJBQW1CLEVBQUU7UUFDckk7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwyQkFBMkIsMEJBQTBCLEVBQUU7UUFDdkQsaUNBQWlDLGVBQWU7UUFDaEQ7UUFDQTtRQUNBOztRQUVBO1FBQ0Esc0RBQXNELCtEQUErRDs7UUFFckg7UUFDQTs7O1FBR0E7UUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3hGQTtBQUNBO0FBQ0E7QUFDQTtBQUVlLFNBQVNBLFNBQVQsR0FBcUI7QUFDbEMsUUFBTTtBQUFBLE9BQUNDLE1BQUQ7QUFBQSxPQUFTQztBQUFULE1BQXNCQyxzREFBUSxDQUFDLEVBQUQsQ0FBcEM7QUFDQSxRQUFNO0FBQUEsT0FBQ0MsYUFBRDtBQUFBLE9BQWdCQztBQUFoQixNQUFvQ0Ysc0RBQVEsQ0FBQyxFQUFELENBQWxEO0FBQ0EsUUFBTTtBQUFBLE9BQUNHLGVBQUQ7QUFBQSxPQUFrQkM7QUFBbEIsTUFBd0NKLHNEQUFRLENBQUMsRUFBRCxDQUF0RDtBQUVBLFFBQU07QUFBQSxPQUFDSyxlQUFEO0FBQUEsT0FBa0JDO0FBQWxCLE1BQXdDTixzREFBUSxDQUFDLEVBQUQsQ0FBdEQ7QUFFQU8seURBQVMsQ0FBQyxNQUFNO0FBQ2RDLFNBQUssQ0FBQyxpREFBRCxFQUFvRDtBQUN2REMsYUFBTyxFQUFFO0FBQ1BDLHFCQUFhLEVBQUcsVUFBU0MsWUFBWSxDQUFDQyxPQUFiLENBQXFCLE9BQXJCLENBQThCO0FBRGhEO0FBRDhDLEtBQXBELENBQUwsQ0FLR0MsSUFMSCxDQUtTQyxHQUFELElBQVNBLEdBQUcsQ0FBQ0MsSUFBSixFQUxqQixFQU1HRixJQU5ILENBTVNHLElBQUQsSUFBVTtBQUNkVix3QkFBa0IsQ0FBQ1UsSUFBRCxDQUFsQjtBQUNELEtBUkg7QUFTRCxHQVZRLEVBVU4sRUFWTSxDQUFUO0FBWUFULHlEQUFTLENBQUMsTUFBTTtBQUNkLFFBQUlGLGVBQWUsQ0FBQ1ksTUFBaEIsR0FBeUIsQ0FBN0IsRUFBZ0M7QUFDOUIsVUFBSUMsVUFBVSxHQUFHLENBQ2YsU0FEZSxFQUVmLFVBRmUsRUFHZixPQUhlLEVBSWYsT0FKZSxFQUtmLEtBTGUsRUFNZixNQU5lLEVBT2YsTUFQZSxFQVFmLFFBUmUsRUFTZixXQVRlLEVBVWYsU0FWZSxFQVdmLFVBWGUsRUFZZixVQVplLENBQWpCO0FBZUFiLHFCQUFlLENBQUNjLE9BQWhCLENBQXlCQyxPQUFELElBQWE7QUFDbkMsWUFDRSxDQUFDRixVQUFVLENBQUNHLElBQVgsQ0FDRUMsS0FBRCxJQUFXQSxLQUFLLEtBQUtDLDZDQUFNLENBQUNILE9BQU8sQ0FBQ0ksVUFBVCxDQUFOLENBQTJCQyxNQUEzQixDQUFrQyxNQUFsQyxDQUR0QixDQURILEVBSUU7QUFDQVAsb0JBQVUsQ0FBQ1EsSUFBWCxDQUFnQkgsNkNBQU0sQ0FBQ0gsT0FBTyxDQUFDTyxTQUFULENBQU4sQ0FBMEJGLE1BQTFCLENBQWlDLE1BQWpDLENBQWhCO0FBQ0Q7QUFDRixPQVJEO0FBVUEsWUFBTUcsU0FBUyxHQUFHLENBQ2hCLFNBRGdCLEVBRWhCLFVBRmdCLEVBR2hCLE9BSGdCLEVBSWhCLE9BSmdCLEVBS2hCLEtBTGdCLEVBTWhCLE1BTmdCLEVBT2hCLE1BUGdCLEVBUWhCLFFBUmdCLEVBU2hCLFdBVGdCLEVBVWhCLFNBVmdCLEVBV2hCLFVBWGdCLEVBWWhCLFVBWmdCLENBQWxCO0FBZUFWLGdCQUFVLENBQUNXLElBQVgsQ0FBZ0IsQ0FBQ0MsQ0FBRCxFQUFJQyxDQUFKLEtBQVU7QUFDeEIsWUFBSUgsU0FBUyxDQUFDSSxPQUFWLENBQWtCRixDQUFsQixNQUF5QixDQUFDLENBQTFCLElBQStCRixTQUFTLENBQUNJLE9BQVYsQ0FBa0JELENBQWxCLE1BQXlCLENBQUMsQ0FBN0QsRUFBZ0U7QUFDOUQsaUJBQU9ILFNBQVMsQ0FBQ0ksT0FBVixDQUFrQkYsQ0FBbEIsSUFBdUJGLFNBQVMsQ0FBQ0ksT0FBVixDQUFrQkQsQ0FBbEIsQ0FBOUI7QUFDRDtBQUNGLE9BSkQ7QUFNQWhDLGVBQVMsQ0FBQ21CLFVBQUQsQ0FBVDtBQUNEO0FBQ0YsR0FsRFEsRUFrRE4sQ0FBQ2IsZUFBRCxDQWxETSxDQUFUO0FBb0RBRSx5REFBUyxDQUFDLE1BQU07QUFDZEwsb0JBQWdCLENBQ2RKLE1BQU0sQ0FBQ21DLEdBQVAsQ0FBWVgsS0FBRCxJQUFXO0FBQ3BCLFVBQUlZLE1BQU0sR0FBRyxDQUFiO0FBRUE3QixxQkFBZSxDQUFDYyxPQUFoQixDQUF5QkMsT0FBRCxJQUFhO0FBQ25DLFlBQ0VHLDZDQUFNLENBQUNILE9BQU8sQ0FBQ0ksVUFBVCxDQUFOLENBQTJCQyxNQUEzQixDQUFrQyxNQUFsQyxNQUE4Q0gsS0FBOUMsSUFDQUYsT0FBTyxDQUFDZSxJQUFSLEtBQWlCLFFBRm5CLEVBR0U7QUFDQUQsZ0JBQU0sSUFBSUUsUUFBUSxDQUFDaEIsT0FBTyxDQUFDaUIsTUFBVCxDQUFsQjtBQUNEO0FBQ0YsT0FQRDtBQVFBLGFBQU9ILE1BQVA7QUFDRCxLQVpELENBRGMsQ0FBaEI7QUFlRCxHQWhCUSxFQWdCTixDQUFDcEMsTUFBRCxDQWhCTSxDQUFUO0FBa0JBUyx5REFBUyxDQUFDLE1BQU07QUFDZEgsc0JBQWtCLENBQ2hCTixNQUFNLENBQUNtQyxHQUFQLENBQVlYLEtBQUQsSUFBVztBQUNwQixVQUFJZ0IsT0FBTyxHQUFHLENBQWQ7QUFFQWpDLHFCQUFlLENBQUNjLE9BQWhCLENBQXlCQyxPQUFELElBQWE7QUFDbkMsWUFDRUcsNkNBQU0sQ0FBQ0gsT0FBTyxDQUFDSSxVQUFULENBQU4sQ0FBMkJDLE1BQTNCLENBQWtDLE1BQWxDLE1BQThDSCxLQUE5QyxJQUNBRixPQUFPLENBQUNlLElBQVIsS0FBaUIsU0FGbkIsRUFHRTtBQUNBRyxpQkFBTyxJQUFJRixRQUFRLENBQUNoQixPQUFPLENBQUNpQixNQUFULENBQW5CO0FBQ0Q7QUFDRixPQVBEO0FBUUEsYUFBT0MsT0FBUDtBQUNELEtBWkQsQ0FEZ0IsQ0FBbEI7QUFlRCxHQWhCUSxFQWdCTixDQUFDeEMsTUFBRCxDQWhCTSxDQUFUO0FBa0JBLFFBQU15QyxVQUFVLEdBQUc7QUFDakJDLFVBQU0sRUFBRTFDLE1BRFM7QUFFakIyQyxZQUFRLEVBQUUsQ0FDUjtBQUNFQyxXQUFLLEVBQUUsZUFEVDtBQUVFQyxxQkFBZSxFQUFFLFNBRm5CO0FBR0VDLGlCQUFXLEVBQUUsT0FIZjtBQUlFQyxpQkFBVyxFQUFFLENBSmY7QUFLRUMsMEJBQW9CLEVBQUUsU0FMeEI7QUFNRUMsc0JBQWdCLEVBQUUsU0FOcEI7QUFPRS9CLFVBQUksRUFBRWY7QUFQUixLQURRO0FBRk8sR0FBbkI7QUFlQSxRQUFNK0MsV0FBVyxHQUFHO0FBQ2xCUixVQUFNLEVBQUUxQyxNQURVO0FBRWxCMkMsWUFBUSxFQUFFLENBQ1I7QUFDRUMsV0FBSyxFQUFFLGlCQURUO0FBRUVDLHFCQUFlLEVBQUUsT0FGbkI7QUFHRUMsaUJBQVcsRUFBRSxPQUhmO0FBSUVDLGlCQUFXLEVBQUUsQ0FKZjtBQUtFQywwQkFBb0IsRUFBRSxPQUx4QjtBQU1FQyxzQkFBZ0IsRUFBRSxPQU5wQjtBQU9FL0IsVUFBSSxFQUFFYjtBQVBSLEtBRFE7QUFGUSxHQUFwQjtBQWVBLFFBQU04QyxPQUFPLEdBQUc7QUFDZEMsVUFBTSxFQUFFO0FBQ05DLFdBQUssRUFBRSxDQUNMO0FBQ0VDLGFBQUssRUFBRTtBQUNMQyxxQkFBVyxFQUFFO0FBRFI7QUFEVCxPQURLO0FBREQ7QUFETSxHQUFoQjtBQVlBLHNCQUNFO0FBQUEsMkJBQ0UscUVBQUMsbURBQUQ7QUFBSyxlQUFTLEVBQUMsTUFBZjtBQUFBLDhCQUNFLHFFQUFDLG1EQUFEO0FBQUssVUFBRSxFQUFFLENBQVQ7QUFBQSxnQ0FDRTtBQUFJLG1CQUFTLEVBQUMsYUFBZDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFERixlQUVFLHFFQUFDLG1EQUFEO0FBQUssY0FBSSxFQUFFZCxVQUFYO0FBQXVCLGlCQUFPLEVBQUVVO0FBQWhDO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBRkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBREYsZUFLRSxxRUFBQyxtREFBRDtBQUFLLFVBQUUsRUFBRSxDQUFUO0FBQUEsZ0NBQ0U7QUFBSSxtQkFBUyxFQUFDLGFBQWQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBREYsZUFFRSxxRUFBQyxtREFBRDtBQUFLLGNBQUksRUFBRUQsV0FBWDtBQUF3QixpQkFBTyxFQUFFQztBQUFqQztBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQUZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQUxGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGLG1CQURGO0FBY0QsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDeEtEO0FBQ0E7QUFDQTtBQUNBO0FBRWUsU0FBU0ssUUFBVCxHQUFvQjtBQUNqQyxRQUFNO0FBQUEsT0FBQ0MsV0FBRDtBQUFBLE9BQWNDO0FBQWQsTUFBZ0N4RCxzREFBUSxDQUFDLEVBQUQsQ0FBOUM7QUFDQSxRQUFNO0FBQUEsT0FBQ3lELGFBQUQ7QUFBQSxPQUFnQkM7QUFBaEIsTUFBb0MxRCxzREFBUSxDQUFDLEVBQUQsQ0FBbEQ7QUFDQSxRQUFNO0FBQUEsT0FBQ0ssZUFBRDtBQUFBLE9BQWtCQztBQUFsQixNQUF3Q04sc0RBQVEsQ0FBQyxFQUFELENBQXREO0FBRUFPLHlEQUFTLENBQUMsTUFBTTtBQUNkQyxTQUFLLENBQUMsaURBQUQsRUFBb0Q7QUFDdkRDLGFBQU8sRUFBRTtBQUNQQyxxQkFBYSxFQUFHLFVBQVNDLFlBQVksQ0FBQ0MsT0FBYixDQUFxQixPQUFyQixDQUE4QjtBQURoRDtBQUQ4QyxLQUFwRCxDQUFMLENBS0dDLElBTEgsQ0FLU0MsR0FBRCxJQUFTQSxHQUFHLENBQUNDLElBQUosRUFMakIsRUFNR0YsSUFOSCxDQU1TRyxJQUFELElBQVU7QUFDZFYsd0JBQWtCLENBQUNVLElBQUQsQ0FBbEI7QUFDRCxLQVJIO0FBU0QsR0FWUSxFQVVOLEVBVk0sQ0FBVDtBQVlBVCx5REFBUyxDQUFDLE1BQU07QUFDZGlELGtCQUFjLENBQ1puRCxlQUFlLENBQUM0QixHQUFoQixDQUFxQjBCLE1BQUQsSUFBWTtBQUM5QixVQUFJekIsTUFBTSxHQUFHLENBQWI7QUFFQTdCLHFCQUFlLENBQUNjLE9BQWhCLENBQXlCQyxPQUFELElBQWE7QUFDbkMsWUFBSUEsT0FBTyxDQUFDZSxJQUFSLEtBQWlCLFFBQXJCLEVBQStCO0FBQzdCRCxnQkFBTSxJQUFJRSxRQUFRLENBQUNoQixPQUFPLENBQUNpQixNQUFULENBQWxCO0FBQ0Q7QUFDRixPQUpEO0FBS0EsYUFBT0gsTUFBUDtBQUNELEtBVEQsQ0FEWSxDQUFkO0FBWUQsR0FiUSxFQWFOLENBQUM3QixlQUFELENBYk0sQ0FBVDtBQWVBRSx5REFBUyxDQUFDLE1BQU07QUFDZG1ELG9CQUFnQixDQUNkckQsZUFBZSxDQUFDNEIsR0FBaEIsQ0FBcUIwQixNQUFELElBQVk7QUFDOUIsVUFBSXJCLE9BQU8sR0FBRyxDQUFkO0FBRUFqQyxxQkFBZSxDQUFDYyxPQUFoQixDQUF5QkMsT0FBRCxJQUFhO0FBQ25DLFlBQUlBLE9BQU8sQ0FBQ2UsSUFBUixLQUFpQixTQUFyQixFQUFnQztBQUM5QkcsaUJBQU8sSUFBSUYsUUFBUSxDQUFDaEIsT0FBTyxDQUFDaUIsTUFBVCxDQUFuQjtBQUNEO0FBQ0YsT0FKRDtBQUtBLGFBQU9DLE9BQVA7QUFDRCxLQVRELENBRGMsQ0FBaEI7QUFZRCxHQWJRLEVBYU4sQ0FBQ2pDLGVBQUQsQ0FiTSxDQUFUO0FBZUEsUUFBTXVELGNBQWMsR0FBR0wsV0FBVyxDQUFDLENBQUQsQ0FBbEM7QUFDQSxRQUFNTSxnQkFBZ0IsR0FBR0osYUFBYSxDQUFDLENBQUQsQ0FBdEM7QUFFQSxRQUFNekMsSUFBSSxHQUFHO0FBQ1h5QixZQUFRLEVBQUUsQ0FDUjtBQUNFekIsVUFBSSxFQUFFLENBQUM0QyxjQUFELEVBQWlCQyxnQkFBakIsQ0FEUjtBQUVFbEIscUJBQWUsRUFBRSxDQUFDLFNBQUQsRUFBWSxPQUFaO0FBRm5CLEtBRFEsQ0FEQztBQU9YSCxVQUFNLEVBQUUsQ0FBQyxjQUFELEVBQWlCLGdCQUFqQjtBQVBHLEdBQWI7QUFVQSxzQkFDRTtBQUFBLDJCQUNFLHFFQUFDLG1EQUFEO0FBQUEsNkJBQ0UscUVBQUMsbURBQUQ7QUFBSyxVQUFFLEVBQUUsQ0FBVDtBQUFZLGlCQUFTLEVBQUMsYUFBdEI7QUFBQSwrQkFDRSxxRUFBQyxtREFBRDtBQUFLLGNBQUksRUFBRXhCO0FBQVg7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREYsbUJBREY7QUFTRCxDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDMUVEO0FBQ0E7QUFDQTtBQUVlLFNBQVM4QyxNQUFULEdBQWtCO0FBQy9CLHNCQUNFLHFFQUFDLHlEQUFEO0FBQVcsYUFBUyxFQUFDLGFBQXJCO0FBQUEsNEJBQ0U7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFERixlQUVFLHFFQUFDLDREQUFEO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFGRixlQUdFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBSEYsZUFJRSxxRUFBQyw0REFBRDtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBSkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFVBREY7QUFRRCxDOzs7Ozs7Ozs7OztBQ2JELG1DOzs7Ozs7Ozs7OztBQ0FBLGtDOzs7Ozs7Ozs7OztBQ0FBLDRDOzs7Ozs7Ozs7OztBQ0FBLDRDOzs7Ozs7Ozs7OztBQ0FBLGtEIiwiZmlsZSI6InBhZ2VzL2NoYXJ0cy5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0gcmVxdWlyZSgnLi4vc3NyLW1vZHVsZS1jYWNoZS5qcycpO1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHR2YXIgdGhyZXcgPSB0cnVlO1xuIFx0XHR0cnkge1xuIFx0XHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuIFx0XHRcdHRocmV3ID0gZmFsc2U7XG4gXHRcdH0gZmluYWxseSB7XG4gXHRcdFx0aWYodGhyZXcpIGRlbGV0ZSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXTtcbiBcdFx0fVxuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBcIi4vcGFnZXMvY2hhcnRzL2luZGV4LmpzXCIpO1xuIiwiaW1wb3J0IHsgdXNlU3RhdGUsIHVzZUVmZmVjdCB9IGZyb20gXCJyZWFjdFwiO1xyXG5pbXBvcnQgeyBCYXIgfSBmcm9tIFwicmVhY3QtY2hhcnRqcy0yXCI7XHJcbmltcG9ydCB7IFJvdywgQ29sIH0gZnJvbSBcInJlYWN0LWJvb3RzdHJhcFwiO1xyXG5pbXBvcnQgbW9tZW50IGZyb20gXCJtb21lbnRcIjtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIEJhckNoYXJ0cygpIHtcclxuICBjb25zdCBbbW9udGhzLCBzZXRNb250aHNdID0gdXNlU3RhdGUoW10pO1xyXG4gIGNvbnN0IFttb250aGx5SW5jb21lLCBzZXRNb250aGx5SW5jb21lXSA9IHVzZVN0YXRlKFtdKTtcclxuICBjb25zdCBbbW9udGhseUV4cGVuc2VzLCBzZXRNb250aGx5RXhwZW5zZXNdID0gdXNlU3RhdGUoW10pO1xyXG5cclxuICBjb25zdCBbYWxsVHJhbnNhY3Rpb25zLCBzZXRBbGxUcmFuc2FjdGlvbnNdID0gdXNlU3RhdGUoW10pO1xyXG5cclxuICB1c2VFZmZlY3QoKCkgPT4ge1xyXG4gICAgZmV0Y2goXCJodHRwOi8vbG9jYWxob3N0OjgwMDAvYXBpL3VzZXJzL2FsbFRyYW5zYWN0aW9uc1wiLCB7XHJcbiAgICAgIGhlYWRlcnM6IHtcclxuICAgICAgICBBdXRob3JpemF0aW9uOiBgQmVhcmVyICR7bG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJ0b2tlblwiKX1gLFxyXG4gICAgICB9LFxyXG4gICAgfSlcclxuICAgICAgLnRoZW4oKHJlcykgPT4gcmVzLmpzb24oKSlcclxuICAgICAgLnRoZW4oKGRhdGEpID0+IHtcclxuICAgICAgICBzZXRBbGxUcmFuc2FjdGlvbnMoZGF0YSk7XHJcbiAgICAgIH0pO1xyXG4gIH0sIFtdKTtcclxuXHJcbiAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgIGlmIChhbGxUcmFuc2FjdGlvbnMubGVuZ3RoID4gMCkge1xyXG4gICAgICBsZXQgdGVtcE1vbnRocyA9IFtcclxuICAgICAgICBcIkphbnVhcnlcIixcclxuICAgICAgICBcIkZlYnJ1YXJ5XCIsXHJcbiAgICAgICAgXCJNYXJjaFwiLFxyXG4gICAgICAgIFwiQXByaWxcIixcclxuICAgICAgICBcIk1heVwiLFxyXG4gICAgICAgIFwiSnVuZVwiLFxyXG4gICAgICAgIFwiSnVseVwiLFxyXG4gICAgICAgIFwiQXVndXN0XCIsXHJcbiAgICAgICAgXCJTZXB0ZW1iZXJcIixcclxuICAgICAgICBcIk9jdG9iZXJcIixcclxuICAgICAgICBcIk5vdmVtYmVyXCIsXHJcbiAgICAgICAgXCJEZWNlbWJlclwiLFxyXG4gICAgICBdO1xyXG5cclxuICAgICAgYWxsVHJhbnNhY3Rpb25zLmZvckVhY2goKGVsZW1lbnQpID0+IHtcclxuICAgICAgICBpZiAoXHJcbiAgICAgICAgICAhdGVtcE1vbnRocy5maW5kKFxyXG4gICAgICAgICAgICAobW9udGgpID0+IG1vbnRoID09PSBtb21lbnQoZWxlbWVudC5yZWNvcmRlZE9uKS5mb3JtYXQoXCJNTU1NXCIpXHJcbiAgICAgICAgICApXHJcbiAgICAgICAgKSB7XHJcbiAgICAgICAgICB0ZW1wTW9udGhzLnB1c2gobW9tZW50KGVsZW1lbnQuc2FsZV9kYXRlKS5mb3JtYXQoXCJNTU1NXCIpKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG5cclxuICAgICAgY29uc3QgbW9udGhzUmVmID0gW1xyXG4gICAgICAgIFwiSmFudWFyeVwiLFxyXG4gICAgICAgIFwiRmVicnVhcnlcIixcclxuICAgICAgICBcIk1hcmNoXCIsXHJcbiAgICAgICAgXCJBcHJpbFwiLFxyXG4gICAgICAgIFwiTWF5XCIsXHJcbiAgICAgICAgXCJKdW5lXCIsXHJcbiAgICAgICAgXCJKdWx5XCIsXHJcbiAgICAgICAgXCJBdWd1c3RcIixcclxuICAgICAgICBcIlNlcHRlbWJlclwiLFxyXG4gICAgICAgIFwiT2N0b2JlclwiLFxyXG4gICAgICAgIFwiTm92ZW1iZXJcIixcclxuICAgICAgICBcIkRlY2VtYmVyXCIsXHJcbiAgICAgIF07XHJcblxyXG4gICAgICB0ZW1wTW9udGhzLnNvcnQoKGEsIGIpID0+IHtcclxuICAgICAgICBpZiAobW9udGhzUmVmLmluZGV4T2YoYSkgIT09IC0xICYmIG1vbnRoc1JlZi5pbmRleE9mKGIpICE9PSAtMSkge1xyXG4gICAgICAgICAgcmV0dXJuIG1vbnRoc1JlZi5pbmRleE9mKGEpIC0gbW9udGhzUmVmLmluZGV4T2YoYik7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuXHJcbiAgICAgIHNldE1vbnRocyh0ZW1wTW9udGhzKTtcclxuICAgIH1cclxuICB9LCBbYWxsVHJhbnNhY3Rpb25zXSk7XHJcblxyXG4gIHVzZUVmZmVjdCgoKSA9PiB7XHJcbiAgICBzZXRNb250aGx5SW5jb21lKFxyXG4gICAgICBtb250aHMubWFwKChtb250aCkgPT4ge1xyXG4gICAgICAgIGxldCBpbmNvbWUgPSAwO1xyXG5cclxuICAgICAgICBhbGxUcmFuc2FjdGlvbnMuZm9yRWFjaCgoZWxlbWVudCkgPT4ge1xyXG4gICAgICAgICAgaWYgKFxyXG4gICAgICAgICAgICBtb21lbnQoZWxlbWVudC5yZWNvcmRlZE9uKS5mb3JtYXQoXCJNTU1NXCIpID09PSBtb250aCAmJlxyXG4gICAgICAgICAgICBlbGVtZW50LnR5cGUgPT09IFwiSW5jb21lXCJcclxuICAgICAgICAgICkge1xyXG4gICAgICAgICAgICBpbmNvbWUgKz0gcGFyc2VJbnQoZWxlbWVudC5hbW91bnQpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHJldHVybiBpbmNvbWU7XHJcbiAgICAgIH0pXHJcbiAgICApO1xyXG4gIH0sIFttb250aHNdKTtcclxuXHJcbiAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgIHNldE1vbnRobHlFeHBlbnNlcyhcclxuICAgICAgbW9udGhzLm1hcCgobW9udGgpID0+IHtcclxuICAgICAgICBsZXQgZXhwZW5zZSA9IDA7XHJcblxyXG4gICAgICAgIGFsbFRyYW5zYWN0aW9ucy5mb3JFYWNoKChlbGVtZW50KSA9PiB7XHJcbiAgICAgICAgICBpZiAoXHJcbiAgICAgICAgICAgIG1vbWVudChlbGVtZW50LnJlY29yZGVkT24pLmZvcm1hdChcIk1NTU1cIikgPT09IG1vbnRoICYmXHJcbiAgICAgICAgICAgIGVsZW1lbnQudHlwZSA9PT0gXCJFeHBlbnNlXCJcclxuICAgICAgICAgICkge1xyXG4gICAgICAgICAgICBleHBlbnNlICs9IHBhcnNlSW50KGVsZW1lbnQuYW1vdW50KTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgICByZXR1cm4gZXhwZW5zZTtcclxuICAgICAgfSlcclxuICAgICk7XHJcbiAgfSwgW21vbnRoc10pO1xyXG5cclxuICBjb25zdCBpbmNvbWVEYXRhID0ge1xyXG4gICAgbGFiZWxzOiBtb250aHMsXHJcbiAgICBkYXRhc2V0czogW1xyXG4gICAgICB7XHJcbiAgICAgICAgbGFiZWw6IFwiQW5udWFsIEluY29tZVwiLFxyXG4gICAgICAgIGJhY2tncm91bmRDb2xvcjogXCIjRjA5MzFGXCIsXHJcbiAgICAgICAgYm9yZGVyQ29sb3I6IFwid2hpdGVcIixcclxuICAgICAgICBib3JkZXJXaWR0aDogMSxcclxuICAgICAgICBob3ZlckJhY2tncm91bmRDb2xvcjogXCIjRjA5MzFGXCIsXHJcbiAgICAgICAgaG92ZXJCb3JkZXJDb2xvcjogXCIjRjA5MzFGXCIsXHJcbiAgICAgICAgZGF0YTogbW9udGhseUluY29tZSxcclxuICAgICAgfSxcclxuICAgIF0sXHJcbiAgfTtcclxuXHJcbiAgY29uc3QgZXhwZW5zZURhdGEgPSB7XHJcbiAgICBsYWJlbHM6IG1vbnRocyxcclxuICAgIGRhdGFzZXRzOiBbXHJcbiAgICAgIHtcclxuICAgICAgICBsYWJlbDogXCJBbm51YWwgRXhwZW5zZXNcIixcclxuICAgICAgICBiYWNrZ3JvdW5kQ29sb3I6IFwiYmxhY2tcIixcclxuICAgICAgICBib3JkZXJDb2xvcjogXCJ3aGl0ZVwiLFxyXG4gICAgICAgIGJvcmRlcldpZHRoOiAxLFxyXG4gICAgICAgIGhvdmVyQmFja2dyb3VuZENvbG9yOiBcImJsYWNrXCIsXHJcbiAgICAgICAgaG92ZXJCb3JkZXJDb2xvcjogXCJibGFja1wiLFxyXG4gICAgICAgIGRhdGE6IG1vbnRobHlFeHBlbnNlcyxcclxuICAgICAgfSxcclxuICAgIF0sXHJcbiAgfTtcclxuXHJcbiAgY29uc3Qgb3B0aW9ucyA9IHtcclxuICAgIHNjYWxlczoge1xyXG4gICAgICB5QXhlczogW1xyXG4gICAgICAgIHtcclxuICAgICAgICAgIHRpY2tzOiB7XHJcbiAgICAgICAgICAgIGJlZ2luQXRaZXJvOiB0cnVlLFxyXG4gICAgICAgICAgfSxcclxuICAgICAgICB9LFxyXG4gICAgICBdLFxyXG4gICAgfSxcclxuICB9O1xyXG5cclxuICByZXR1cm4gKFxyXG4gICAgPD5cclxuICAgICAgPFJvdyBjbGFzc05hbWU9XCJtdC01XCI+XHJcbiAgICAgICAgPENvbCBtZD17Nn0+XHJcbiAgICAgICAgICA8aDIgY2xhc3NOYW1lPVwidGV4dC1jZW50ZXJcIj5JbmNvbWU8L2gyPlxyXG4gICAgICAgICAgPEJhciBkYXRhPXtpbmNvbWVEYXRhfSBvcHRpb25zPXtvcHRpb25zfSAvPlxyXG4gICAgICAgIDwvQ29sPlxyXG4gICAgICAgIDxDb2wgbWQ9ezZ9PlxyXG4gICAgICAgICAgPGgyIGNsYXNzTmFtZT1cInRleHQtY2VudGVyXCI+RXhwZW5zZXM8L2gyPlxyXG4gICAgICAgICAgPEJhciBkYXRhPXtleHBlbnNlRGF0YX0gb3B0aW9ucz17b3B0aW9uc30gLz5cclxuICAgICAgICA8L0NvbD5cclxuICAgICAgPC9Sb3c+XHJcbiAgICA8Lz5cclxuICApO1xyXG59XHJcbiIsImltcG9ydCB7IHVzZVN0YXRlLCB1c2VFZmZlY3QgfSBmcm9tIFwicmVhY3RcIjtcclxuaW1wb3J0IHsgUGllIH0gZnJvbSBcInJlYWN0LWNoYXJ0anMtMlwiO1xyXG5pbXBvcnQgeyBSb3csIENvbCB9IGZyb20gXCJyZWFjdC1ib290c3RyYXBcIjtcclxuaW1wb3J0IG1vbWVudCBmcm9tIFwibW9tZW50XCI7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBQaWVDaGFydCgpIHtcclxuICBjb25zdCBbdG90YWxJbmNvbWUsIHNldFRvdGFsSW5jb21lXSA9IHVzZVN0YXRlKFtdKTtcclxuICBjb25zdCBbdG90YWxFeHBlbnNlcywgc2V0VG90YWxFeHBlbnNlc10gPSB1c2VTdGF0ZShbXSk7XHJcbiAgY29uc3QgW2FsbFRyYW5zYWN0aW9ucywgc2V0QWxsVHJhbnNhY3Rpb25zXSA9IHVzZVN0YXRlKFtdKTtcclxuXHJcbiAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgIGZldGNoKFwiaHR0cDovL2xvY2FsaG9zdDo4MDAwL2FwaS91c2Vycy9hbGxUcmFuc2FjdGlvbnNcIiwge1xyXG4gICAgICBoZWFkZXJzOiB7XHJcbiAgICAgICAgQXV0aG9yaXphdGlvbjogYEJlYXJlciAke2xvY2FsU3RvcmFnZS5nZXRJdGVtKFwidG9rZW5cIil9YCxcclxuICAgICAgfSxcclxuICAgIH0pXHJcbiAgICAgIC50aGVuKChyZXMpID0+IHJlcy5qc29uKCkpXHJcbiAgICAgIC50aGVuKChkYXRhKSA9PiB7XHJcbiAgICAgICAgc2V0QWxsVHJhbnNhY3Rpb25zKGRhdGEpO1xyXG4gICAgICB9KTtcclxuICB9LCBbXSk7XHJcblxyXG4gIHVzZUVmZmVjdCgoKSA9PiB7XHJcbiAgICBzZXRUb3RhbEluY29tZShcclxuICAgICAgYWxsVHJhbnNhY3Rpb25zLm1hcCgocmVzdWx0KSA9PiB7XHJcbiAgICAgICAgbGV0IGluY29tZSA9IDA7XHJcblxyXG4gICAgICAgIGFsbFRyYW5zYWN0aW9ucy5mb3JFYWNoKChlbGVtZW50KSA9PiB7XHJcbiAgICAgICAgICBpZiAoZWxlbWVudC50eXBlID09PSBcIkluY29tZVwiKSB7XHJcbiAgICAgICAgICAgIGluY29tZSArPSBwYXJzZUludChlbGVtZW50LmFtb3VudCk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgcmV0dXJuIGluY29tZTtcclxuICAgICAgfSlcclxuICAgICk7XHJcbiAgfSwgW2FsbFRyYW5zYWN0aW9uc10pO1xyXG5cclxuICB1c2VFZmZlY3QoKCkgPT4ge1xyXG4gICAgc2V0VG90YWxFeHBlbnNlcyhcclxuICAgICAgYWxsVHJhbnNhY3Rpb25zLm1hcCgocmVzdWx0KSA9PiB7XHJcbiAgICAgICAgbGV0IGV4cGVuc2UgPSAwO1xyXG5cclxuICAgICAgICBhbGxUcmFuc2FjdGlvbnMuZm9yRWFjaCgoZWxlbWVudCkgPT4ge1xyXG4gICAgICAgICAgaWYgKGVsZW1lbnQudHlwZSA9PT0gXCJFeHBlbnNlXCIpIHtcclxuICAgICAgICAgICAgZXhwZW5zZSArPSBwYXJzZUludChlbGVtZW50LmFtb3VudCk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgcmV0dXJuIGV4cGVuc2U7XHJcbiAgICAgIH0pXHJcbiAgICApO1xyXG4gIH0sIFthbGxUcmFuc2FjdGlvbnNdKTtcclxuXHJcbiAgY29uc3QgdG90YWxJbmNvbWVSZXMgPSB0b3RhbEluY29tZVswXTtcclxuICBjb25zdCB0b3RhbEV4cGVuc2VzUmVzID0gdG90YWxFeHBlbnNlc1swXTtcclxuXHJcbiAgY29uc3QgZGF0YSA9IHtcclxuICAgIGRhdGFzZXRzOiBbXHJcbiAgICAgIHtcclxuICAgICAgICBkYXRhOiBbdG90YWxJbmNvbWVSZXMsIHRvdGFsRXhwZW5zZXNSZXNdLFxyXG4gICAgICAgIGJhY2tncm91bmRDb2xvcjogW1wiI0YwOTMxRlwiLCBcImJsYWNrXCJdLFxyXG4gICAgICB9LFxyXG4gICAgXSxcclxuICAgIGxhYmVsczogW1wiVG90YWwgSW5jb21lXCIsIFwiVG90YWwgRXhwZW5zZXNcIl0sXHJcbiAgfTtcclxuXHJcbiAgcmV0dXJuIChcclxuICAgIDw+XHJcbiAgICAgIDxSb3c+XHJcbiAgICAgICAgPENvbCBtZD17Nn0gY2xhc3NOYW1lPVwib2Zmc2V0LW1kLTNcIj5cclxuICAgICAgICAgIDxQaWUgZGF0YT17ZGF0YX0gLz5cclxuICAgICAgICA8L0NvbD5cclxuICAgICAgPC9Sb3c+XHJcbiAgICA8Lz5cclxuICApO1xyXG59XHJcbiIsImltcG9ydCBCYXJDaGFydHMgZnJvbSBcIi4uLy4uL2NvbXBvbmVudHMvQmFyY2hhcnRcIjtcclxuaW1wb3J0IFBpZUNoYXJ0IGZyb20gXCIuLi8uLi9jb21wb25lbnRzL1BpZUNoYXJ0XCI7XHJcbmltcG9ydCB7IEp1bWJvdHJvbiB9IGZyb20gXCJyZWFjdC1ib290c3RyYXBcIjtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIENoYXJ0cygpIHtcclxuICByZXR1cm4gKFxyXG4gICAgPEp1bWJvdHJvbiBjbGFzc05hbWU9XCJ0ZXh0LWNlbnRlclwiPlxyXG4gICAgICA8aDE+UGllIEdyYXBoPC9oMT5cclxuICAgICAgPFBpZUNoYXJ0IC8+XHJcbiAgICAgIDxoMT5CYXIgR3JhcGg8L2gxPlxyXG4gICAgICA8QmFyQ2hhcnRzIC8+XHJcbiAgICA8L0p1bWJvdHJvbj5cclxuICApO1xyXG59XHJcbiIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIm1vbWVudFwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWFjdFwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWFjdC1ib290c3RyYXBcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwicmVhY3QtY2hhcnRqcy0yXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlYWN0L2pzeC1kZXYtcnVudGltZVwiKTsiXSwic291cmNlUm9vdCI6IiJ9