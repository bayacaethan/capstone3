module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./pages/search/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./components/Category.js":
/*!********************************!*\
  !*** ./components/Category.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Category; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-bootstrap */ "react-bootstrap");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__);

var _jsxFileName = "E:\\batch-92\\capstone3\\budget_tracker\\components\\Category.js";

 // To get the prop, destructure it

function Category({
  categoryProp
}) {
  //  after you get the prop, destructure it para makuha mo yng laman na need mo:
  const {
    _id,
    name,
    type
  } = categoryProp;
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Card"], {
    className: "my-3",
    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Card"].Body, {
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Card"].Title, {
        children: name
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 12,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Card"].Text, {
        children: type
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 13,
        columnNumber: 9
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 11,
      columnNumber: 7
    }, this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 10,
    columnNumber: 5
  }, this);
}

/***/ }),

/***/ "./components/Record.js":
/*!******************************!*\
  !*** ./components/Record.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Category; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-bootstrap */ "react-bootstrap");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__);

var _jsxFileName = "E:\\batch-92\\capstone3\\budget_tracker\\components\\Record.js";

 // To get the prop, destructure it

function Category({
  recordProp
}) {
  //  after you get the prop, destructure it para makuha mo yng laman na need mo:
  const {
    _id,
    name,
    type,
    category,
    balance,
    recordedOn,
    description,
    amount
  } = recordProp;
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Card"], {
    className: "my-3",
    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Card"].Body, {
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Card"].Title, {
        children: name
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 21,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Card"].Text, {
        children: type
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 22,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Card"].Title, {
        children: category
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 23,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Card"].Text, {
        children: description
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 24,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Card"].Text, {
        children: amount
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 25,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Card"].Text, {
        children: balance
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 26,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Card"].Text, {
        children: recordedOn
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 27,
        columnNumber: 9
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 20,
      columnNumber: 7
    }, this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 19,
    columnNumber: 5
  }, this);
}

/***/ }),

/***/ "./pages/search/index.js":
/*!*******************************!*\
  !*** ./pages/search/index.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return SearchTransaction; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_Category__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../components/Category */ "./components/Category.js");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-bootstrap */ "react-bootstrap");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "sweetalert2");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! next/router */ "next/router");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _userContext__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../userContext */ "./userContext.js");
/* harmony import */ var _components_Record__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/Record */ "./components/Record.js");


var _jsxFileName = "E:\\batch-92\\capstone3\\budget_tracker\\pages\\search\\index.js";




 //import user context



function SearchTransaction() {
  const {
    user
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useContext"])(_userContext__WEBPACK_IMPORTED_MODULE_6__["default"]);
  console.log(user); //State for token

  const {
    0: userToken,
    1: setUserToken
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])({
    setUserToken: null
  }); //State for Category Type seletion

  const {
    0: selection,
    1: setSelection
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""); //State for the all records

  const {
    0: records,
    1: setRecords
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]); //State for search

  const {
    0: search,
    1: setSearch
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""); //State for second search

  const {
    0: secondSearch,
    1: setSecondSearch
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]); //State for filtered results

  const {
    0: filteredResults,
    1: setFilteredResults
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    fetch("http://localhost:8000/api/users/allTransactions", {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    }).then(res => res.json()).then(data => {
      setSecondSearch(data);
    });
  }, []);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    setFilteredResults(secondSearch.filter(results => {
      return results.description.toLowerCase().includes(search.toLowerCase());
    }));
  }, [search, secondSearch]); //useEffect for the Token

  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    setUserToken(localStorage.getItem("token"));
  }, []);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    fetch("http://localhost:8000/api/users/allTransactions", {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    }).then(res => res.json()).then(data => {
      console.log(data);
      const allArr = [];
      const incomesArr = [];
      const expensesArr = [];

      if (selection === "Income") {
        data.filter(result => {
          if (result.type === "Income") {
            incomesArr.push(result);
          }
        });
        setRecords(incomesArr);
      } else if (selection === "Expense") {
        data.filter(result => {
          if (result.type === "Expense") {
            expensesArr.push(result);
          }
        });
        setRecords(expensesArr);
      } else if (selection === "ALL") {
        data.filter(result => {
          allArr.push(result);
        });
        setRecords(allArr);
      }
    });
  }, [selection]);
  const recordsArr = records.map(record => {
    return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Card"], {
      className: "mt-3",
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Card"].Body, {
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Card"].Text, {
          children: ["Category: ", record.category]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 111,
          columnNumber: 11
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Card"].Text, {
          children: [record.type, ": ", record.amount]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 112,
          columnNumber: 11
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Card"].Text, {
          children: ["Description: ", record.description]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 115,
          columnNumber: 11
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 110,
        columnNumber: 9
      }, this)
    }, record._id, false, {
      fileName: _jsxFileName,
      lineNumber: 109,
      columnNumber: 7
    }, this);
  });
  console.log(records);
  const secondFilter = filteredResults.map(record => {
    return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Card"], {
      className: "mt-3",
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Card"].Body, {
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Card"].Text, {
          children: ["Category: ", record.category]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 126,
          columnNumber: 11
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Card"].Text, {
          children: [record.type, ": ", record.amount]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 127,
          columnNumber: 11
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Card"].Text, {
          children: ["Description: ", record.description]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 130,
          columnNumber: 11
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 125,
        columnNumber: 9
      }, this)
    }, record._id, false, {
      fileName: _jsxFileName,
      lineNumber: 124,
      columnNumber: 7
    }, this);
  });
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Container"], {
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Row"], {
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Col"], {
          xs: 12,
          md: 6,
          className: "my-3",
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Form"], {
            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Form"].Group, {
              controlId: "selectionLabel",
              children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Form"].Label, {
                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h4", {
                  children: "Filter by Category Type"
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 144,
                  columnNumber: 19
                }, this)
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 143,
                columnNumber: 17
              }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Form"].Control, {
                as: "select",
                required: true,
                value: selection,
                onChange: e => setSelection(e.target.value),
                children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("option", {
                  value: "",
                  disabled: true,
                  children: "Please Select type"
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 152,
                  columnNumber: 19
                }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("option", {
                  children: "ALL"
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 155,
                  columnNumber: 19
                }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("option", {
                  children: "Income"
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 156,
                  columnNumber: 19
                }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("option", {
                  children: "Expense"
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 157,
                  columnNumber: 19
                }, this)]
              }, void 0, true, {
                fileName: _jsxFileName,
                lineNumber: 146,
                columnNumber: 17
              }, this)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 142,
              columnNumber: 15
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 141,
            columnNumber: 13
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 140,
          columnNumber: 11
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Col"], {
          xs: 12,
          md: 6,
          className: "my-3",
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Jumbotron"], {
            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h1", {
              children: "Result for Filter:"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 165,
              columnNumber: 15
            }, this), recordsArr]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 164,
            columnNumber: 13
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 163,
          columnNumber: 11
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 139,
        columnNumber: 9
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 138,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Container"], {
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Row"], {
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Col"], {
          xs: 12,
          md: 6,
          className: "my-3",
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Form"], {
            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Form"].Group, {
              controlId: "cdesc",
              children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Form"].Label, {
                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h4", {
                  children: "Search by Description:"
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 177,
                  columnNumber: 19
                }, this)
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 176,
                columnNumber: 17
              }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Form"].Control, {
                type: "text",
                placeholder: "Enter Description",
                value: search,
                onChange: e => setSearch(e.target.value),
                required: true
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 179,
                columnNumber: 17
              }, this)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 175,
              columnNumber: 15
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 174,
            columnNumber: 13
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 173,
          columnNumber: 11
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Col"], {
          xs: 12,
          md: 6,
          className: "my-3",
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Jumbotron"], {
            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h1", {
              children: "Result for Search By description:"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 191,
              columnNumber: 15
            }, this), secondFilter]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 190,
            columnNumber: 13
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 189,
          columnNumber: 11
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 172,
        columnNumber: 9
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 171,
      columnNumber: 7
    }, this)]
  }, void 0, true);
}

/***/ }),

/***/ "./userContext.js":
/*!************************!*\
  !*** ./userContext.js ***!
  \************************/
/*! exports provided: UserProvider, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserProvider", function() { return UserProvider; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

const UserContext = /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createContext();
const UserProvider = UserContext.Provider;
/* harmony default export */ __webpack_exports__["default"] = (UserContext);

/***/ }),

/***/ "next/router":
/*!******************************!*\
  !*** external "next/router" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/router");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "react-bootstrap":
/*!**********************************!*\
  !*** external "react-bootstrap" ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-bootstrap");

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react/jsx-dev-runtime");

/***/ }),

/***/ "sweetalert2":
/*!******************************!*\
  !*** external "sweetalert2" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("sweetalert2");

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vY29tcG9uZW50cy9DYXRlZ29yeS5qcyIsIndlYnBhY2s6Ly8vLi9jb21wb25lbnRzL1JlY29yZC5qcyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9zZWFyY2gvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vVXNlckNvbnRleHQuanMiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwibmV4dC9yb3V0ZXJcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJyZWFjdFwiIiwid2VicGFjazovLy9leHRlcm5hbCBcInJlYWN0LWJvb3RzdHJhcFwiIiwid2VicGFjazovLy9leHRlcm5hbCBcInJlYWN0L2pzeC1kZXYtcnVudGltZVwiIiwid2VicGFjazovLy9leHRlcm5hbCBcInN3ZWV0YWxlcnQyXCIiXSwibmFtZXMiOlsiQ2F0ZWdvcnkiLCJjYXRlZ29yeVByb3AiLCJfaWQiLCJuYW1lIiwidHlwZSIsInJlY29yZFByb3AiLCJjYXRlZ29yeSIsImJhbGFuY2UiLCJyZWNvcmRlZE9uIiwiZGVzY3JpcHRpb24iLCJhbW91bnQiLCJTZWFyY2hUcmFuc2FjdGlvbiIsInVzZXIiLCJ1c2VDb250ZXh0IiwiVXNlckNvbnRleHQiLCJjb25zb2xlIiwibG9nIiwidXNlclRva2VuIiwic2V0VXNlclRva2VuIiwidXNlU3RhdGUiLCJzZWxlY3Rpb24iLCJzZXRTZWxlY3Rpb24iLCJyZWNvcmRzIiwic2V0UmVjb3JkcyIsInNlYXJjaCIsInNldFNlYXJjaCIsInNlY29uZFNlYXJjaCIsInNldFNlY29uZFNlYXJjaCIsImZpbHRlcmVkUmVzdWx0cyIsInNldEZpbHRlcmVkUmVzdWx0cyIsInVzZUVmZmVjdCIsImZldGNoIiwiaGVhZGVycyIsIkF1dGhvcml6YXRpb24iLCJsb2NhbFN0b3JhZ2UiLCJnZXRJdGVtIiwidGhlbiIsInJlcyIsImpzb24iLCJkYXRhIiwiZmlsdGVyIiwicmVzdWx0cyIsInRvTG93ZXJDYXNlIiwiaW5jbHVkZXMiLCJhbGxBcnIiLCJpbmNvbWVzQXJyIiwiZXhwZW5zZXNBcnIiLCJyZXN1bHQiLCJwdXNoIiwicmVjb3Jkc0FyciIsIm1hcCIsInJlY29yZCIsInNlY29uZEZpbHRlciIsImUiLCJ0YXJnZXQiLCJ2YWx1ZSIsIlJlYWN0IiwiY3JlYXRlQ29udGV4dCIsIlVzZXJQcm92aWRlciIsIlByb3ZpZGVyIl0sIm1hcHBpbmdzIjoiOztRQUFBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0EsSUFBSTtRQUNKO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7OztRQUdBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwwQ0FBMEMsZ0NBQWdDO1FBQzFFO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0Esd0RBQXdELGtCQUFrQjtRQUMxRTtRQUNBLGlEQUFpRCxjQUFjO1FBQy9EOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQSx5Q0FBeUMsaUNBQWlDO1FBQzFFLGdIQUFnSCxtQkFBbUIsRUFBRTtRQUNySTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDJCQUEyQiwwQkFBMEIsRUFBRTtRQUN2RCxpQ0FBaUMsZUFBZTtRQUNoRDtRQUNBO1FBQ0E7O1FBRUE7UUFDQSxzREFBc0QsK0RBQStEOztRQUVySDtRQUNBOzs7UUFHQTtRQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3hGQTtDQUdBOztBQUNlLFNBQVNBLFFBQVQsQ0FBa0I7QUFBRUM7QUFBRixDQUFsQixFQUFvQztBQUNqRDtBQUNBLFFBQU07QUFBRUMsT0FBRjtBQUFPQyxRQUFQO0FBQWFDO0FBQWIsTUFBc0JILFlBQTVCO0FBRUEsc0JBQ0UscUVBQUMsb0RBQUQ7QUFBTSxhQUFTLEVBQUMsTUFBaEI7QUFBQSwyQkFDRSxxRUFBQyxvREFBRCxDQUFNLElBQU47QUFBQSw4QkFDRSxxRUFBQyxvREFBRCxDQUFNLEtBQU47QUFBQSxrQkFBYUU7QUFBYjtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBREYsZUFFRSxxRUFBQyxvREFBRCxDQUFNLElBQU47QUFBQSxrQkFBWUM7QUFBWjtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBRkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxVQURGO0FBUUQsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2hCRDtDQUdBOztBQUNlLFNBQVNKLFFBQVQsQ0FBa0I7QUFBRUs7QUFBRixDQUFsQixFQUFrQztBQUMvQztBQUNBLFFBQU07QUFDSkgsT0FESTtBQUVKQyxRQUZJO0FBR0pDLFFBSEk7QUFJSkUsWUFKSTtBQUtKQyxXQUxJO0FBTUpDLGNBTkk7QUFPSkMsZUFQSTtBQVFKQztBQVJJLE1BU0ZMLFVBVEo7QUFXQSxzQkFDRSxxRUFBQyxvREFBRDtBQUFNLGFBQVMsRUFBQyxNQUFoQjtBQUFBLDJCQUNFLHFFQUFDLG9EQUFELENBQU0sSUFBTjtBQUFBLDhCQUNFLHFFQUFDLG9EQUFELENBQU0sS0FBTjtBQUFBLGtCQUFhRjtBQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FERixlQUVFLHFFQUFDLG9EQUFELENBQU0sSUFBTjtBQUFBLGtCQUFZQztBQUFaO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FGRixlQUdFLHFFQUFDLG9EQUFELENBQU0sS0FBTjtBQUFBLGtCQUFhRTtBQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FIRixlQUlFLHFFQUFDLG9EQUFELENBQU0sSUFBTjtBQUFBLGtCQUFZRztBQUFaO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FKRixlQUtFLHFFQUFDLG9EQUFELENBQU0sSUFBTjtBQUFBLGtCQUFZQztBQUFaO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FMRixlQU1FLHFFQUFDLG9EQUFELENBQU0sSUFBTjtBQUFBLGtCQUFZSDtBQUFaO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FORixlQU9FLHFFQUFDLG9EQUFELENBQU0sSUFBTjtBQUFBLGtCQUFZQztBQUFaO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FQRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLFVBREY7QUFhRCxDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM5QkQ7QUFDQTtBQUNBO0FBU0E7Q0FHQTs7QUFDQTtBQUVBO0FBRWUsU0FBU0csaUJBQVQsR0FBNkI7QUFDMUMsUUFBTTtBQUFFQztBQUFGLE1BQVdDLHdEQUFVLENBQUNDLG9EQUFELENBQTNCO0FBQ0FDLFNBQU8sQ0FBQ0MsR0FBUixDQUFZSixJQUFaLEVBRjBDLENBSTFDOztBQUVBLFFBQU07QUFBQSxPQUFDSyxTQUFEO0FBQUEsT0FBWUM7QUFBWixNQUE0QkMsc0RBQVEsQ0FBQztBQUN6Q0QsZ0JBQVksRUFBRTtBQUQyQixHQUFELENBQTFDLENBTjBDLENBVTFDOztBQUNBLFFBQU07QUFBQSxPQUFDRSxTQUFEO0FBQUEsT0FBWUM7QUFBWixNQUE0QkYsc0RBQVEsQ0FBQyxFQUFELENBQTFDLENBWDBDLENBYTFDOztBQUNBLFFBQU07QUFBQSxPQUFDRyxPQUFEO0FBQUEsT0FBVUM7QUFBVixNQUF3Qkosc0RBQVEsQ0FBQyxFQUFELENBQXRDLENBZDBDLENBZ0IxQzs7QUFDQSxRQUFNO0FBQUEsT0FBQ0ssTUFBRDtBQUFBLE9BQVNDO0FBQVQsTUFBc0JOLHNEQUFRLENBQUMsRUFBRCxDQUFwQyxDQWpCMEMsQ0FtQjFDOztBQUNBLFFBQU07QUFBQSxPQUFDTyxZQUFEO0FBQUEsT0FBZUM7QUFBZixNQUFrQ1Isc0RBQVEsQ0FBQyxFQUFELENBQWhELENBcEIwQyxDQXNCMUM7O0FBQ0EsUUFBTTtBQUFBLE9BQUNTLGVBQUQ7QUFBQSxPQUFrQkM7QUFBbEIsTUFBd0NWLHNEQUFRLENBQUMsRUFBRCxDQUF0RDtBQUVBVyx5REFBUyxDQUFDLE1BQU07QUFDZEMsU0FBSyxDQUFDLGlEQUFELEVBQW9EO0FBQ3ZEQyxhQUFPLEVBQUU7QUFDUEMscUJBQWEsRUFBRyxVQUFTQyxZQUFZLENBQUNDLE9BQWIsQ0FBcUIsT0FBckIsQ0FBOEI7QUFEaEQ7QUFEOEMsS0FBcEQsQ0FBTCxDQUtHQyxJQUxILENBS1NDLEdBQUQsSUFBU0EsR0FBRyxDQUFDQyxJQUFKLEVBTGpCLEVBTUdGLElBTkgsQ0FNU0csSUFBRCxJQUFVO0FBQ2RaLHFCQUFlLENBQUNZLElBQUQsQ0FBZjtBQUNELEtBUkg7QUFTRCxHQVZRLEVBVU4sRUFWTSxDQUFUO0FBWUFULHlEQUFTLENBQUMsTUFBTTtBQUNkRCxzQkFBa0IsQ0FDaEJILFlBQVksQ0FBQ2MsTUFBYixDQUFxQkMsT0FBRCxJQUFhO0FBQy9CLGFBQU9BLE9BQU8sQ0FBQ2hDLFdBQVIsQ0FBb0JpQyxXQUFwQixHQUFrQ0MsUUFBbEMsQ0FBMkNuQixNQUFNLENBQUNrQixXQUFQLEVBQTNDLENBQVA7QUFDRCxLQUZELENBRGdCLENBQWxCO0FBS0QsR0FOUSxFQU1OLENBQUNsQixNQUFELEVBQVNFLFlBQVQsQ0FOTSxDQUFULENBckMwQyxDQTZDMUM7O0FBRUFJLHlEQUFTLENBQUMsTUFBTTtBQUNkWixnQkFBWSxDQUFDZ0IsWUFBWSxDQUFDQyxPQUFiLENBQXFCLE9BQXJCLENBQUQsQ0FBWjtBQUNELEdBRlEsRUFFTixFQUZNLENBQVQ7QUFJQUwseURBQVMsQ0FBQyxNQUFNO0FBQ2RDLFNBQUssQ0FBQyxpREFBRCxFQUFvRDtBQUN2REMsYUFBTyxFQUFFO0FBQ1BDLHFCQUFhLEVBQUcsVUFBU0MsWUFBWSxDQUFDQyxPQUFiLENBQXFCLE9BQXJCLENBQThCO0FBRGhEO0FBRDhDLEtBQXBELENBQUwsQ0FLR0MsSUFMSCxDQUtTQyxHQUFELElBQVNBLEdBQUcsQ0FBQ0MsSUFBSixFQUxqQixFQU1HRixJQU5ILENBTVNHLElBQUQsSUFBVTtBQUNkeEIsYUFBTyxDQUFDQyxHQUFSLENBQVl1QixJQUFaO0FBQ0EsWUFBTUssTUFBTSxHQUFHLEVBQWY7QUFDQSxZQUFNQyxVQUFVLEdBQUcsRUFBbkI7QUFDQSxZQUFNQyxXQUFXLEdBQUcsRUFBcEI7O0FBRUEsVUFBSTFCLFNBQVMsS0FBSyxRQUFsQixFQUE0QjtBQUMxQm1CLFlBQUksQ0FBQ0MsTUFBTCxDQUFhTyxNQUFELElBQVk7QUFDdEIsY0FBSUEsTUFBTSxDQUFDM0MsSUFBUCxLQUFnQixRQUFwQixFQUE4QjtBQUM1QnlDLHNCQUFVLENBQUNHLElBQVgsQ0FBZ0JELE1BQWhCO0FBQ0Q7QUFDRixTQUpEO0FBS0F4QixrQkFBVSxDQUFDc0IsVUFBRCxDQUFWO0FBQ0QsT0FQRCxNQU9PLElBQUl6QixTQUFTLEtBQUssU0FBbEIsRUFBNkI7QUFDbENtQixZQUFJLENBQUNDLE1BQUwsQ0FBYU8sTUFBRCxJQUFZO0FBQ3RCLGNBQUlBLE1BQU0sQ0FBQzNDLElBQVAsS0FBZ0IsU0FBcEIsRUFBK0I7QUFDN0IwQyx1QkFBVyxDQUFDRSxJQUFaLENBQWlCRCxNQUFqQjtBQUNEO0FBQ0YsU0FKRDtBQUtBeEIsa0JBQVUsQ0FBQ3VCLFdBQUQsQ0FBVjtBQUNELE9BUE0sTUFPQSxJQUFJMUIsU0FBUyxLQUFLLEtBQWxCLEVBQXlCO0FBQzlCbUIsWUFBSSxDQUFDQyxNQUFMLENBQWFPLE1BQUQsSUFBWTtBQUN0QkgsZ0JBQU0sQ0FBQ0ksSUFBUCxDQUFZRCxNQUFaO0FBQ0QsU0FGRDtBQUdBeEIsa0JBQVUsQ0FBQ3FCLE1BQUQsQ0FBVjtBQUNEO0FBQ0YsS0FoQ0g7QUFpQ0QsR0FsQ1EsRUFrQ04sQ0FBQ3hCLFNBQUQsQ0FsQ00sQ0FBVDtBQW9DQSxRQUFNNkIsVUFBVSxHQUFHM0IsT0FBTyxDQUFDNEIsR0FBUixDQUFhQyxNQUFELElBQVk7QUFDekMsd0JBQ0UscUVBQUMsb0RBQUQ7QUFBdUIsZUFBUyxFQUFDLE1BQWpDO0FBQUEsNkJBQ0UscUVBQUMsb0RBQUQsQ0FBTSxJQUFOO0FBQUEsZ0NBQ0UscUVBQUMsb0RBQUQsQ0FBTSxJQUFOO0FBQUEsbUNBQXNCQSxNQUFNLENBQUM3QyxRQUE3QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBREYsZUFFRSxxRUFBQyxvREFBRCxDQUFNLElBQU47QUFBQSxxQkFDRzZDLE1BQU0sQ0FBQy9DLElBRFYsUUFDa0IrQyxNQUFNLENBQUN6QyxNQUR6QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBRkYsZUFLRSxxRUFBQyxvREFBRCxDQUFNLElBQU47QUFBQSxzQ0FBeUJ5QyxNQUFNLENBQUMxQyxXQUFoQztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBTEY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREYsT0FBVzBDLE1BQU0sQ0FBQ2pELEdBQWxCO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFERjtBQVdELEdBWmtCLENBQW5CO0FBY0FhLFNBQU8sQ0FBQ0MsR0FBUixDQUFZTSxPQUFaO0FBQ0EsUUFBTThCLFlBQVksR0FBR3hCLGVBQWUsQ0FBQ3NCLEdBQWhCLENBQXFCQyxNQUFELElBQVk7QUFDbkQsd0JBQ0UscUVBQUMsb0RBQUQ7QUFBdUIsZUFBUyxFQUFDLE1BQWpDO0FBQUEsNkJBQ0UscUVBQUMsb0RBQUQsQ0FBTSxJQUFOO0FBQUEsZ0NBQ0UscUVBQUMsb0RBQUQsQ0FBTSxJQUFOO0FBQUEsbUNBQXNCQSxNQUFNLENBQUM3QyxRQUE3QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBREYsZUFFRSxxRUFBQyxvREFBRCxDQUFNLElBQU47QUFBQSxxQkFDRzZDLE1BQU0sQ0FBQy9DLElBRFYsUUFDa0IrQyxNQUFNLENBQUN6QyxNQUR6QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBRkYsZUFLRSxxRUFBQyxvREFBRCxDQUFNLElBQU47QUFBQSxzQ0FBeUJ5QyxNQUFNLENBQUMxQyxXQUFoQztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBTEY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREYsT0FBVzBDLE1BQU0sQ0FBQ2pELEdBQWxCO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFERjtBQVdELEdBWm9CLENBQXJCO0FBY0Esc0JBQ0U7QUFBQSw0QkFDRSxxRUFBQyx5REFBRDtBQUFBLDZCQUNFLHFFQUFDLG1EQUFEO0FBQUEsZ0NBQ0UscUVBQUMsbURBQUQ7QUFBSyxZQUFFLEVBQUUsRUFBVDtBQUFhLFlBQUUsRUFBRSxDQUFqQjtBQUFvQixtQkFBUyxFQUFDLE1BQTlCO0FBQUEsaUNBQ0UscUVBQUMsb0RBQUQ7QUFBQSxtQ0FDRSxxRUFBQyxvREFBRCxDQUFNLEtBQU47QUFBWSx1QkFBUyxFQUFDLGdCQUF0QjtBQUFBLHNDQUNFLHFFQUFDLG9EQUFELENBQU0sS0FBTjtBQUFBLHVDQUNFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQkFERixlQUlFLHFFQUFDLG9EQUFELENBQU0sT0FBTjtBQUNFLGtCQUFFLEVBQUMsUUFETDtBQUVFLHdCQUFRLE1BRlY7QUFHRSxxQkFBSyxFQUFFa0IsU0FIVDtBQUlFLHdCQUFRLEVBQUdpQyxDQUFELElBQU9oQyxZQUFZLENBQUNnQyxDQUFDLENBQUNDLE1BQUYsQ0FBU0MsS0FBVixDQUovQjtBQUFBLHdDQU1FO0FBQVEsdUJBQUssRUFBQyxFQUFkO0FBQWlCLDBCQUFRLE1BQXpCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdCQU5GLGVBU0U7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBVEYsZUFVRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx3QkFWRixlQVdFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdCQVhGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQkFKRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFERixlQXdCRSxxRUFBQyxtREFBRDtBQUFLLFlBQUUsRUFBRSxFQUFUO0FBQWEsWUFBRSxFQUFFLENBQWpCO0FBQW9CLG1CQUFTLEVBQUMsTUFBOUI7QUFBQSxpQ0FDRSxxRUFBQyx5REFBRDtBQUFBLG9DQUNFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQURGLEVBRUdOLFVBRkg7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkF4QkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQURGLGVBa0NFLHFFQUFDLHlEQUFEO0FBQUEsNkJBQ0UscUVBQUMsbURBQUQ7QUFBQSxnQ0FDRSxxRUFBQyxtREFBRDtBQUFLLFlBQUUsRUFBRSxFQUFUO0FBQWEsWUFBRSxFQUFFLENBQWpCO0FBQW9CLG1CQUFTLEVBQUMsTUFBOUI7QUFBQSxpQ0FDRSxxRUFBQyxvREFBRDtBQUFBLG1DQUNFLHFFQUFDLG9EQUFELENBQU0sS0FBTjtBQUFZLHVCQUFTLEVBQUMsT0FBdEI7QUFBQSxzQ0FDRSxxRUFBQyxvREFBRCxDQUFNLEtBQU47QUFBQSx1Q0FDRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0JBREYsZUFJRSxxRUFBQyxvREFBRCxDQUFNLE9BQU47QUFDRSxvQkFBSSxFQUFDLE1BRFA7QUFFRSwyQkFBVyxFQUFDLG1CQUZkO0FBR0UscUJBQUssRUFBRXpCLE1BSFQ7QUFJRSx3QkFBUSxFQUFHNkIsQ0FBRCxJQUFPNUIsU0FBUyxDQUFDNEIsQ0FBQyxDQUFDQyxNQUFGLENBQVNDLEtBQVYsQ0FKNUI7QUFLRSx3QkFBUTtBQUxWO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0JBSkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBREYsZUFpQkUscUVBQUMsbURBQUQ7QUFBSyxZQUFFLEVBQUUsRUFBVDtBQUFhLFlBQUUsRUFBRSxDQUFqQjtBQUFvQixtQkFBUyxFQUFDLE1BQTlCO0FBQUEsaUNBQ0UscUVBQUMseURBQUQ7QUFBQSxvQ0FDRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFERixFQUVHSCxZQUZIO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBakJGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFsQ0Y7QUFBQSxrQkFERjtBQStERCxDOzs7Ozs7Ozs7Ozs7QUN0TUQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBLE1BQU10QyxXQUFXLGdCQUFHMEMsNENBQUssQ0FBQ0MsYUFBTixFQUFwQjtBQUNPLE1BQU1DLFlBQVksR0FBRzVDLFdBQVcsQ0FBQzZDLFFBQWpDO0FBQ1E3QywwRUFBZixFOzs7Ozs7Ozs7OztBQ0hBLHdDOzs7Ozs7Ozs7OztBQ0FBLGtDOzs7Ozs7Ozs7OztBQ0FBLDRDOzs7Ozs7Ozs7OztBQ0FBLGtEOzs7Ozs7Ozs7OztBQ0FBLHdDIiwiZmlsZSI6InBhZ2VzL3NlYXJjaC5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0gcmVxdWlyZSgnLi4vc3NyLW1vZHVsZS1jYWNoZS5qcycpO1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHR2YXIgdGhyZXcgPSB0cnVlO1xuIFx0XHR0cnkge1xuIFx0XHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuIFx0XHRcdHRocmV3ID0gZmFsc2U7XG4gXHRcdH0gZmluYWxseSB7XG4gXHRcdFx0aWYodGhyZXcpIGRlbGV0ZSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXTtcbiBcdFx0fVxuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBcIi4vcGFnZXMvc2VhcmNoL2luZGV4LmpzXCIpO1xuIiwiaW1wb3J0IHsgdXNlU3RhdGUsIHVzZUVmZmVjdCwgdXNlQ29udGV4dCB9IGZyb20gXCJyZWFjdFwiO1xyXG5pbXBvcnQgeyBDYXJkIH0gZnJvbSBcInJlYWN0LWJvb3RzdHJhcFwiO1xyXG5cclxuLy8gVG8gZ2V0IHRoZSBwcm9wLCBkZXN0cnVjdHVyZSBpdFxyXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBDYXRlZ29yeSh7IGNhdGVnb3J5UHJvcCB9KSB7XHJcbiAgLy8gIGFmdGVyIHlvdSBnZXQgdGhlIHByb3AsIGRlc3RydWN0dXJlIGl0IHBhcmEgbWFrdWhhIG1vIHluZyBsYW1hbiBuYSBuZWVkIG1vOlxyXG4gIGNvbnN0IHsgX2lkLCBuYW1lLCB0eXBlIH0gPSBjYXRlZ29yeVByb3A7XHJcblxyXG4gIHJldHVybiAoXHJcbiAgICA8Q2FyZCBjbGFzc05hbWU9XCJteS0zXCI+XHJcbiAgICAgIDxDYXJkLkJvZHk+XHJcbiAgICAgICAgPENhcmQuVGl0bGU+e25hbWV9PC9DYXJkLlRpdGxlPlxyXG4gICAgICAgIDxDYXJkLlRleHQ+e3R5cGV9PC9DYXJkLlRleHQ+XHJcbiAgICAgIDwvQ2FyZC5Cb2R5PlxyXG4gICAgPC9DYXJkPlxyXG4gICk7XHJcbn1cclxuIiwiaW1wb3J0IHsgdXNlU3RhdGUsIHVzZUVmZmVjdCwgdXNlQ29udGV4dCB9IGZyb20gXCJyZWFjdFwiO1xyXG5pbXBvcnQgeyBDYXJkIH0gZnJvbSBcInJlYWN0LWJvb3RzdHJhcFwiO1xyXG5cclxuLy8gVG8gZ2V0IHRoZSBwcm9wLCBkZXN0cnVjdHVyZSBpdFxyXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBDYXRlZ29yeSh7IHJlY29yZFByb3AgfSkge1xyXG4gIC8vICBhZnRlciB5b3UgZ2V0IHRoZSBwcm9wLCBkZXN0cnVjdHVyZSBpdCBwYXJhIG1ha3VoYSBtbyB5bmcgbGFtYW4gbmEgbmVlZCBtbzpcclxuICBjb25zdCB7XHJcbiAgICBfaWQsXHJcbiAgICBuYW1lLFxyXG4gICAgdHlwZSxcclxuICAgIGNhdGVnb3J5LFxyXG4gICAgYmFsYW5jZSxcclxuICAgIHJlY29yZGVkT24sXHJcbiAgICBkZXNjcmlwdGlvbixcclxuICAgIGFtb3VudCxcclxuICB9ID0gcmVjb3JkUHJvcDtcclxuXHJcbiAgcmV0dXJuIChcclxuICAgIDxDYXJkIGNsYXNzTmFtZT1cIm15LTNcIj5cclxuICAgICAgPENhcmQuQm9keT5cclxuICAgICAgICA8Q2FyZC5UaXRsZT57bmFtZX08L0NhcmQuVGl0bGU+XHJcbiAgICAgICAgPENhcmQuVGV4dD57dHlwZX08L0NhcmQuVGV4dD5cclxuICAgICAgICA8Q2FyZC5UaXRsZT57Y2F0ZWdvcnl9PC9DYXJkLlRpdGxlPlxyXG4gICAgICAgIDxDYXJkLlRleHQ+e2Rlc2NyaXB0aW9ufTwvQ2FyZC5UZXh0PlxyXG4gICAgICAgIDxDYXJkLlRleHQ+e2Ftb3VudH08L0NhcmQuVGV4dD5cclxuICAgICAgICA8Q2FyZC5UZXh0PntiYWxhbmNlfTwvQ2FyZC5UZXh0PlxyXG4gICAgICAgIDxDYXJkLlRleHQ+e3JlY29yZGVkT259PC9DYXJkLlRleHQ+XHJcbiAgICAgIDwvQ2FyZC5Cb2R5PlxyXG4gICAgPC9DYXJkPlxyXG4gICk7XHJcbn1cclxuIiwiaW1wb3J0IHsgdXNlU3RhdGUsIHVzZUVmZmVjdCwgdXNlQ29udGV4dCB9IGZyb20gXCJyZWFjdFwiO1xyXG5pbXBvcnQgQ2F0ZWdvcnkgZnJvbSBcIi4uLy4uL2NvbXBvbmVudHMvQ2F0ZWdvcnlcIjtcclxuaW1wb3J0IHtcclxuICBGb3JtLFxyXG4gIEJ1dHRvbixcclxuICBDb250YWluZXIsXHJcbiAgUm93LFxyXG4gIENvbCxcclxuICBDYXJkLFxyXG4gIEp1bWJvdHJvbixcclxufSBmcm9tIFwicmVhY3QtYm9vdHN0cmFwXCI7XHJcbmltcG9ydCBTd2FsIGZyb20gXCJzd2VldGFsZXJ0MlwiO1xyXG5cclxuaW1wb3J0IFJvdXRlciBmcm9tIFwibmV4dC9yb3V0ZXJcIjtcclxuLy9pbXBvcnQgdXNlciBjb250ZXh0XHJcbmltcG9ydCBVc2VyQ29udGV4dCBmcm9tIFwiLi4vLi4vdXNlckNvbnRleHRcIjtcclxuXHJcbmltcG9ydCBSZWNvcmQgZnJvbSBcIi4uLy4uL2NvbXBvbmVudHMvUmVjb3JkXCI7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBTZWFyY2hUcmFuc2FjdGlvbigpIHtcclxuICBjb25zdCB7IHVzZXIgfSA9IHVzZUNvbnRleHQoVXNlckNvbnRleHQpO1xyXG4gIGNvbnNvbGUubG9nKHVzZXIpO1xyXG5cclxuICAvL1N0YXRlIGZvciB0b2tlblxyXG5cclxuICBjb25zdCBbdXNlclRva2VuLCBzZXRVc2VyVG9rZW5dID0gdXNlU3RhdGUoe1xyXG4gICAgc2V0VXNlclRva2VuOiBudWxsLFxyXG4gIH0pO1xyXG5cclxuICAvL1N0YXRlIGZvciBDYXRlZ29yeSBUeXBlIHNlbGV0aW9uXHJcbiAgY29uc3QgW3NlbGVjdGlvbiwgc2V0U2VsZWN0aW9uXSA9IHVzZVN0YXRlKFwiXCIpO1xyXG5cclxuICAvL1N0YXRlIGZvciB0aGUgYWxsIHJlY29yZHNcclxuICBjb25zdCBbcmVjb3Jkcywgc2V0UmVjb3Jkc10gPSB1c2VTdGF0ZShbXSk7XHJcblxyXG4gIC8vU3RhdGUgZm9yIHNlYXJjaFxyXG4gIGNvbnN0IFtzZWFyY2gsIHNldFNlYXJjaF0gPSB1c2VTdGF0ZShcIlwiKTtcclxuXHJcbiAgLy9TdGF0ZSBmb3Igc2Vjb25kIHNlYXJjaFxyXG4gIGNvbnN0IFtzZWNvbmRTZWFyY2gsIHNldFNlY29uZFNlYXJjaF0gPSB1c2VTdGF0ZShbXSk7XHJcblxyXG4gIC8vU3RhdGUgZm9yIGZpbHRlcmVkIHJlc3VsdHNcclxuICBjb25zdCBbZmlsdGVyZWRSZXN1bHRzLCBzZXRGaWx0ZXJlZFJlc3VsdHNdID0gdXNlU3RhdGUoW10pO1xyXG5cclxuICB1c2VFZmZlY3QoKCkgPT4ge1xyXG4gICAgZmV0Y2goXCJodHRwOi8vbG9jYWxob3N0OjgwMDAvYXBpL3VzZXJzL2FsbFRyYW5zYWN0aW9uc1wiLCB7XHJcbiAgICAgIGhlYWRlcnM6IHtcclxuICAgICAgICBBdXRob3JpemF0aW9uOiBgQmVhcmVyICR7bG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJ0b2tlblwiKX1gLFxyXG4gICAgICB9LFxyXG4gICAgfSlcclxuICAgICAgLnRoZW4oKHJlcykgPT4gcmVzLmpzb24oKSlcclxuICAgICAgLnRoZW4oKGRhdGEpID0+IHtcclxuICAgICAgICBzZXRTZWNvbmRTZWFyY2goZGF0YSk7XHJcbiAgICAgIH0pO1xyXG4gIH0sIFtdKTtcclxuXHJcbiAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgIHNldEZpbHRlcmVkUmVzdWx0cyhcclxuICAgICAgc2Vjb25kU2VhcmNoLmZpbHRlcigocmVzdWx0cykgPT4ge1xyXG4gICAgICAgIHJldHVybiByZXN1bHRzLmRlc2NyaXB0aW9uLnRvTG93ZXJDYXNlKCkuaW5jbHVkZXMoc2VhcmNoLnRvTG93ZXJDYXNlKCkpO1xyXG4gICAgICB9KVxyXG4gICAgKTtcclxuICB9LCBbc2VhcmNoLCBzZWNvbmRTZWFyY2hdKTtcclxuXHJcbiAgLy91c2VFZmZlY3QgZm9yIHRoZSBUb2tlblxyXG5cclxuICB1c2VFZmZlY3QoKCkgPT4ge1xyXG4gICAgc2V0VXNlclRva2VuKGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwidG9rZW5cIikpO1xyXG4gIH0sIFtdKTtcclxuXHJcbiAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgIGZldGNoKFwiaHR0cDovL2xvY2FsaG9zdDo4MDAwL2FwaS91c2Vycy9hbGxUcmFuc2FjdGlvbnNcIiwge1xyXG4gICAgICBoZWFkZXJzOiB7XHJcbiAgICAgICAgQXV0aG9yaXphdGlvbjogYEJlYXJlciAke2xvY2FsU3RvcmFnZS5nZXRJdGVtKFwidG9rZW5cIil9YCxcclxuICAgICAgfSxcclxuICAgIH0pXHJcbiAgICAgIC50aGVuKChyZXMpID0+IHJlcy5qc29uKCkpXHJcbiAgICAgIC50aGVuKChkYXRhKSA9PiB7XHJcbiAgICAgICAgY29uc29sZS5sb2coZGF0YSk7XHJcbiAgICAgICAgY29uc3QgYWxsQXJyID0gW107XHJcbiAgICAgICAgY29uc3QgaW5jb21lc0FyciA9IFtdO1xyXG4gICAgICAgIGNvbnN0IGV4cGVuc2VzQXJyID0gW107XHJcblxyXG4gICAgICAgIGlmIChzZWxlY3Rpb24gPT09IFwiSW5jb21lXCIpIHtcclxuICAgICAgICAgIGRhdGEuZmlsdGVyKChyZXN1bHQpID0+IHtcclxuICAgICAgICAgICAgaWYgKHJlc3VsdC50eXBlID09PSBcIkluY29tZVwiKSB7XHJcbiAgICAgICAgICAgICAgaW5jb21lc0Fyci5wdXNoKHJlc3VsdCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgc2V0UmVjb3JkcyhpbmNvbWVzQXJyKTtcclxuICAgICAgICB9IGVsc2UgaWYgKHNlbGVjdGlvbiA9PT0gXCJFeHBlbnNlXCIpIHtcclxuICAgICAgICAgIGRhdGEuZmlsdGVyKChyZXN1bHQpID0+IHtcclxuICAgICAgICAgICAgaWYgKHJlc3VsdC50eXBlID09PSBcIkV4cGVuc2VcIikge1xyXG4gICAgICAgICAgICAgIGV4cGVuc2VzQXJyLnB1c2gocmVzdWx0KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgICBzZXRSZWNvcmRzKGV4cGVuc2VzQXJyKTtcclxuICAgICAgICB9IGVsc2UgaWYgKHNlbGVjdGlvbiA9PT0gXCJBTExcIikge1xyXG4gICAgICAgICAgZGF0YS5maWx0ZXIoKHJlc3VsdCkgPT4ge1xyXG4gICAgICAgICAgICBhbGxBcnIucHVzaChyZXN1bHQpO1xyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgICBzZXRSZWNvcmRzKGFsbEFycik7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICB9LCBbc2VsZWN0aW9uXSk7XHJcblxyXG4gIGNvbnN0IHJlY29yZHNBcnIgPSByZWNvcmRzLm1hcCgocmVjb3JkKSA9PiB7XHJcbiAgICByZXR1cm4gKFxyXG4gICAgICA8Q2FyZCBrZXk9e3JlY29yZC5faWR9IGNsYXNzTmFtZT1cIm10LTNcIj5cclxuICAgICAgICA8Q2FyZC5Cb2R5PlxyXG4gICAgICAgICAgPENhcmQuVGV4dD5DYXRlZ29yeToge3JlY29yZC5jYXRlZ29yeX08L0NhcmQuVGV4dD5cclxuICAgICAgICAgIDxDYXJkLlRleHQ+XHJcbiAgICAgICAgICAgIHtyZWNvcmQudHlwZX06IHtyZWNvcmQuYW1vdW50fVxyXG4gICAgICAgICAgPC9DYXJkLlRleHQ+XHJcbiAgICAgICAgICA8Q2FyZC5UZXh0PkRlc2NyaXB0aW9uOiB7cmVjb3JkLmRlc2NyaXB0aW9ufTwvQ2FyZC5UZXh0PlxyXG4gICAgICAgIDwvQ2FyZC5Cb2R5PlxyXG4gICAgICA8L0NhcmQ+XHJcbiAgICApO1xyXG4gIH0pO1xyXG5cclxuICBjb25zb2xlLmxvZyhyZWNvcmRzKTtcclxuICBjb25zdCBzZWNvbmRGaWx0ZXIgPSBmaWx0ZXJlZFJlc3VsdHMubWFwKChyZWNvcmQpID0+IHtcclxuICAgIHJldHVybiAoXHJcbiAgICAgIDxDYXJkIGtleT17cmVjb3JkLl9pZH0gY2xhc3NOYW1lPVwibXQtM1wiPlxyXG4gICAgICAgIDxDYXJkLkJvZHk+XHJcbiAgICAgICAgICA8Q2FyZC5UZXh0PkNhdGVnb3J5OiB7cmVjb3JkLmNhdGVnb3J5fTwvQ2FyZC5UZXh0PlxyXG4gICAgICAgICAgPENhcmQuVGV4dD5cclxuICAgICAgICAgICAge3JlY29yZC50eXBlfToge3JlY29yZC5hbW91bnR9XHJcbiAgICAgICAgICA8L0NhcmQuVGV4dD5cclxuICAgICAgICAgIDxDYXJkLlRleHQ+RGVzY3JpcHRpb246IHtyZWNvcmQuZGVzY3JpcHRpb259PC9DYXJkLlRleHQ+XHJcbiAgICAgICAgPC9DYXJkLkJvZHk+XHJcbiAgICAgIDwvQ2FyZD5cclxuICAgICk7XHJcbiAgfSk7XHJcblxyXG4gIHJldHVybiAoXHJcbiAgICA8PlxyXG4gICAgICA8Q29udGFpbmVyPlxyXG4gICAgICAgIDxSb3c+XHJcbiAgICAgICAgICA8Q29sIHhzPXsxMn0gbWQ9ezZ9IGNsYXNzTmFtZT1cIm15LTNcIj5cclxuICAgICAgICAgICAgPEZvcm0+XHJcbiAgICAgICAgICAgICAgPEZvcm0uR3JvdXAgY29udHJvbElkPVwic2VsZWN0aW9uTGFiZWxcIj5cclxuICAgICAgICAgICAgICAgIDxGb3JtLkxhYmVsPlxyXG4gICAgICAgICAgICAgICAgICA8aDQ+RmlsdGVyIGJ5IENhdGVnb3J5IFR5cGU8L2g0PlxyXG4gICAgICAgICAgICAgICAgPC9Gb3JtLkxhYmVsPlxyXG4gICAgICAgICAgICAgICAgPEZvcm0uQ29udHJvbFxyXG4gICAgICAgICAgICAgICAgICBhcz1cInNlbGVjdFwiXHJcbiAgICAgICAgICAgICAgICAgIHJlcXVpcmVkXHJcbiAgICAgICAgICAgICAgICAgIHZhbHVlPXtzZWxlY3Rpb259XHJcbiAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsoZSkgPT4gc2V0U2VsZWN0aW9uKGUudGFyZ2V0LnZhbHVlKX1cclxuICAgICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgICAgPG9wdGlvbiB2YWx1ZT1cIlwiIGRpc2FibGVkPlxyXG4gICAgICAgICAgICAgICAgICAgIFBsZWFzZSBTZWxlY3QgdHlwZVxyXG4gICAgICAgICAgICAgICAgICA8L29wdGlvbj5cclxuICAgICAgICAgICAgICAgICAgPG9wdGlvbj5BTEw8L29wdGlvbj5cclxuICAgICAgICAgICAgICAgICAgPG9wdGlvbj5JbmNvbWU8L29wdGlvbj5cclxuICAgICAgICAgICAgICAgICAgPG9wdGlvbj5FeHBlbnNlPC9vcHRpb24+XHJcbiAgICAgICAgICAgICAgICA8L0Zvcm0uQ29udHJvbD5cclxuICAgICAgICAgICAgICA8L0Zvcm0uR3JvdXA+XHJcbiAgICAgICAgICAgIDwvRm9ybT5cclxuICAgICAgICAgIDwvQ29sPlxyXG5cclxuICAgICAgICAgIDxDb2wgeHM9ezEyfSBtZD17Nn0gY2xhc3NOYW1lPVwibXktM1wiPlxyXG4gICAgICAgICAgICA8SnVtYm90cm9uPlxyXG4gICAgICAgICAgICAgIDxoMT5SZXN1bHQgZm9yIEZpbHRlcjo8L2gxPlxyXG4gICAgICAgICAgICAgIHtyZWNvcmRzQXJyfVxyXG4gICAgICAgICAgICA8L0p1bWJvdHJvbj5cclxuICAgICAgICAgIDwvQ29sPlxyXG4gICAgICAgIDwvUm93PlxyXG4gICAgICA8L0NvbnRhaW5lcj5cclxuICAgICAgPENvbnRhaW5lcj5cclxuICAgICAgICA8Um93PlxyXG4gICAgICAgICAgPENvbCB4cz17MTJ9IG1kPXs2fSBjbGFzc05hbWU9XCJteS0zXCI+XHJcbiAgICAgICAgICAgIDxGb3JtPlxyXG4gICAgICAgICAgICAgIDxGb3JtLkdyb3VwIGNvbnRyb2xJZD1cImNkZXNjXCI+XHJcbiAgICAgICAgICAgICAgICA8Rm9ybS5MYWJlbD5cclxuICAgICAgICAgICAgICAgICAgPGg0PlNlYXJjaCBieSBEZXNjcmlwdGlvbjo8L2g0PlxyXG4gICAgICAgICAgICAgICAgPC9Gb3JtLkxhYmVsPlxyXG4gICAgICAgICAgICAgICAgPEZvcm0uQ29udHJvbFxyXG4gICAgICAgICAgICAgICAgICB0eXBlPVwidGV4dFwiXHJcbiAgICAgICAgICAgICAgICAgIHBsYWNlaG9sZGVyPVwiRW50ZXIgRGVzY3JpcHRpb25cIlxyXG4gICAgICAgICAgICAgICAgICB2YWx1ZT17c2VhcmNofVxyXG4gICAgICAgICAgICAgICAgICBvbkNoYW5nZT17KGUpID0+IHNldFNlYXJjaChlLnRhcmdldC52YWx1ZSl9XHJcbiAgICAgICAgICAgICAgICAgIHJlcXVpcmVkXHJcbiAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgIDwvRm9ybS5Hcm91cD5cclxuICAgICAgICAgICAgPC9Gb3JtPlxyXG4gICAgICAgICAgPC9Db2w+XHJcbiAgICAgICAgICA8Q29sIHhzPXsxMn0gbWQ9ezZ9IGNsYXNzTmFtZT1cIm15LTNcIj5cclxuICAgICAgICAgICAgPEp1bWJvdHJvbj5cclxuICAgICAgICAgICAgICA8aDE+UmVzdWx0IGZvciBTZWFyY2ggQnkgZGVzY3JpcHRpb246PC9oMT5cclxuICAgICAgICAgICAgICB7c2Vjb25kRmlsdGVyfVxyXG4gICAgICAgICAgICA8L0p1bWJvdHJvbj5cclxuICAgICAgICAgIDwvQ29sPlxyXG4gICAgICAgIDwvUm93PlxyXG4gICAgICA8L0NvbnRhaW5lcj5cclxuICAgIDwvPlxyXG4gICk7XHJcbn1cclxuIiwiaW1wb3J0IFJlYWN0IGZyb20gXCJyZWFjdFwiO1xyXG5jb25zdCBVc2VyQ29udGV4dCA9IFJlYWN0LmNyZWF0ZUNvbnRleHQoKTtcclxuZXhwb3J0IGNvbnN0IFVzZXJQcm92aWRlciA9IFVzZXJDb250ZXh0LlByb3ZpZGVyO1xyXG5leHBvcnQgZGVmYXVsdCBVc2VyQ29udGV4dDtcclxuIiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwibmV4dC9yb3V0ZXJcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwicmVhY3RcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwicmVhY3QtYm9vdHN0cmFwXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlYWN0L2pzeC1kZXYtcnVudGltZVwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJzd2VldGFsZXJ0MlwiKTsiXSwic291cmNlUm9vdCI6IiJ9