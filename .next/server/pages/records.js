module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./pages/records/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./components/Record.js":
/*!******************************!*\
  !*** ./components/Record.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Category; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-bootstrap */ "react-bootstrap");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__);

var _jsxFileName = "E:\\batch-92\\capstone3\\budget_tracker\\components\\Record.js";

 // To get the prop, destructure it

function Category({
  recordProp
}) {
  //  after you get the prop, destructure it para makuha mo yng laman na need mo:
  const {
    _id,
    name,
    type,
    category,
    balance,
    recordedOn,
    description,
    amount
  } = recordProp;
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Card"], {
    className: "my-3",
    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Card"].Body, {
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Card"].Title, {
        children: name
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 21,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Card"].Text, {
        children: type
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 22,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Card"].Title, {
        children: category
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 23,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Card"].Text, {
        children: description
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 24,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Card"].Text, {
        children: amount
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 25,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Card"].Text, {
        children: balance
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 26,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Card"].Text, {
        children: recordedOn
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 27,
        columnNumber: 9
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 20,
      columnNumber: 7
    }, this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 19,
    columnNumber: 5
  }, this);
}

/***/ }),

/***/ "./pages/records/index.js":
/*!********************************!*\
  !*** ./pages/records/index.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Records; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-bootstrap */ "react-bootstrap");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! sweetalert2 */ "sweetalert2");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! next/router */ "next/router");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _userContext__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../userContext */ "./userContext.js");
/* harmony import */ var _components_Record__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../components/Record */ "./components/Record.js");

var _jsxFileName = "E:\\batch-92\\capstone3\\budget_tracker\\pages\\records\\index.js";



 //import user context



function Records() {
  // Replaced Categories to 'Records'
  const {
    user
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useContext"])(_userContext__WEBPACK_IMPORTED_MODULE_5__["default"]);
  console.log(user); //State for token

  const {
    0: userToken,
    1: setUserToken
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])({
    setUserToken: null
  }); //State for Category Type seletion

  const {
    0: selection,
    1: setSelection
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""); //State for Category Name selection

  const {
    0: categoryNameSelection,
    1: setCategoryNameSelection
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]); //State for final Category Name selection

  const {
    0: categoryName,
    1: setCategoryName
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""); //State for amount field

  const {
    0: recordAmount,
    1: setRecordAmount
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(0); //State for description field

  const {
    0: recordDescription,
    1: setRecordDescription
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""); //State for balance

  const {
    0: balance,
    1: setBalance
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(0); //State for Date

  const {
    0: date,
    1: setDate
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(""); //State for the all records

  const {
    0: records,
    1: setRecords
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]); //useEffect for the Token

  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    setUserToken(localStorage.getItem("token"));
  }, []); //useEffect for Balance

  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    fetch("http://localhost:8000/api/users/balance", {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    }).then(res => res.json()).then(data => {
      setBalance(data);
    });
  }, [balance]); //useEffect for the all records

  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    fetch("http://localhost:8000/api/users/allTransactions", {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    }).then(res => res.json()).then(data => {
      setRecords(data);
    });
  }, []);
  console.log("Records", records);
  const recordsArray = records.map(record => {
    return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_Record__WEBPACK_IMPORTED_MODULE_6__["default"], {
      recordProp: record
    }, record._id, false, {
      fileName: _jsxFileName,
      lineNumber: 78,
      columnNumber: 12
    }, this);
  });
  const {
    0: allCategories,
    1: setAllCategories
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    fetch("http://localhost:8000/api/users/allCategories", {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    }).then(res => res.json()).then(data => {
      console.log(data); // setAllCategories(data)

      let incomeArray = [];
      let expenseArray = [];

      if (selection === "Income") {
        data.filter(result => {
          if (result.type === "Income") {
            incomeArray.push({
              _id: result._id,
              name: result.name
            });
          }
        });
        setCategoryNameSelection(incomeArray);
      } else {
        data.filter(result => {
          if (result.type === "Expense") {
            expenseArray.push({
              _id: result._id,
              name: result.name
            });
          }
        });
        setCategoryNameSelection(expenseArray);
      }
    });
  }, [selection]); // console.log(categoryNameSelection)

  const categoriesSelect = categoryNameSelection.map(category => {
    return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("option", {
      children: category.name
    }, category._id, false, {
      fileName: _jsxFileName,
      lineNumber: 119,
      columnNumber: 12
    }, this);
  });

  function addRecord(e) {
    e.preventDefault(); // console.log('userToken', userToken)
    // console.log('selection',selection)
    // console.log('categoryName',categoryName)

    let newBalance = 0;

    if (selection === "Income") {
      newBalance = balance + parseInt(recordAmount);
    } else {
      newBalance = balance - parseInt(recordAmount);
    }

    fetch("http://localhost:8000/api/users/transaction", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${userToken}`
      },
      body: JSON.stringify({
        id: user.id,
        type: selection,
        category: categoryName,
        amount: recordAmount,
        description: recordDescription,
        balance: newBalance,
        recordedOn: date
      })
    }).then(res => res.json()).then(data => {
      console.log("newBalance", data);
      fetch("http://localhost:8000/api/users/updateBalance", {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${userToken}`
        },
        body: JSON.stringify({
          id: user.id,
          balance: newBalance
        })
      }).then(res => res.json()).then(data => {
        setBalance(newBalance);
      });

      if (data) {
        sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
          icon: "success",
          title: " Success!.",
          text: "New transaction is recorded."
        });
        setCategoryName("");
        setSelection("");
        setRecordAmount(0);
        setRecordDescription("");
        next_router__WEBPACK_IMPORTED_MODULE_4___default.a.reload();
      } else {
        sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
          icon: "error",
          title: "Category Creation failed",
          text: "Please try again."
        });
      }
    });
  }

  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Container"], {
    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Row"], {
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Col"], {
        xs: 12,
        md: 6,
        className: "my-3",
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h4", {
          className: "text-center my-3",
          children: ["Balance: PHP ", balance]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 197,
          columnNumber: 11
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h4", {
          children: "Add Transaction"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 198,
          columnNumber: 11
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"], {
          onSubmit: e => addRecord(e),
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Group, {
            controlId: "selectionLabel",
            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Label, {
              children: "Category Type"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 201,
              columnNumber: 15
            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Control, {
              as: "select",
              required: true,
              value: selection,
              onChange: e => setSelection(e.target.value),
              children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("option", {
                value: "",
                disabled: true,
                children: "Please Select type"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 208,
                columnNumber: 17
              }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("option", {
                children: "Income"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 211,
                columnNumber: 17
              }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("option", {
                children: "Expense"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 212,
                columnNumber: 17
              }, this)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 202,
              columnNumber: 15
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 200,
            columnNumber: 13
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Group, {
            controlId: "selectionLabe2",
            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Label, {
              children: "Category Name"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 216,
              columnNumber: 15
            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Control, {
              as: "select",
              value: categoryName,
              onChange: e => setCategoryName(e.target.value),
              children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("option", {
                value: "",
                disabled: true,
                children: "Select Category Name"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 222,
                columnNumber: 17
              }, this), categoriesSelect]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 217,
              columnNumber: 15
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 215,
            columnNumber: 13
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Group, {
            controlId: "camount",
            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Label, {
              children: "Amount:"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 230,
              columnNumber: 15
            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Control, {
              type: "number",
              placeholder: "Enter Amount",
              value: recordAmount,
              onChange: e => setRecordAmount(e.target.value),
              required: true
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 231,
              columnNumber: 15
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 229,
            columnNumber: 13
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Group, {
            controlId: "cdesc",
            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Label, {
              children: "Description:"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 240,
              columnNumber: 15
            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Control, {
              type: "text",
              placeholder: "Enter Description",
              value: recordDescription,
              onChange: e => setRecordDescription(e.target.value),
              required: true
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 241,
              columnNumber: 15
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 239,
            columnNumber: 13
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Group, {
            controlId: "cdate",
            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Label, {
              children: "Description:"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 250,
              columnNumber: 15
            }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Form"].Control, {
              type: "date",
              placeholder: "Enter Date",
              value: date,
              onChange: e => setDate(e.target.value),
              required: true
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 251,
              columnNumber: 15
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 249,
            columnNumber: 13
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Button"], {
            variant: "primary",
            type: "submit",
            children: "Record Transaction"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 259,
            columnNumber: 13
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 199,
          columnNumber: 11
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 196,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Col"], {
        xs: 12,
        md: 6,
        className: "my-3",
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h4", {
          className: "my-3",
          children: "Transactions Overview:"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 265,
          columnNumber: 11
        }, this), recordsArray]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 264,
        columnNumber: 9
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 195,
      columnNumber: 7
    }, this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 194,
    columnNumber: 5
  }, this);
}

/***/ }),

/***/ "./userContext.js":
/*!************************!*\
  !*** ./userContext.js ***!
  \************************/
/*! exports provided: UserProvider, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserProvider", function() { return UserProvider; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

const UserContext = /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createContext();
const UserProvider = UserContext.Provider;
/* harmony default export */ __webpack_exports__["default"] = (UserContext);

/***/ }),

/***/ "next/router":
/*!******************************!*\
  !*** external "next/router" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/router");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "react-bootstrap":
/*!**********************************!*\
  !*** external "react-bootstrap" ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-bootstrap");

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react/jsx-dev-runtime");

/***/ }),

/***/ "sweetalert2":
/*!******************************!*\
  !*** external "sweetalert2" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("sweetalert2");

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vY29tcG9uZW50cy9SZWNvcmQuanMiLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvcmVjb3Jkcy9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9Vc2VyQ29udGV4dC5qcyIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJuZXh0L3JvdXRlclwiIiwid2VicGFjazovLy9leHRlcm5hbCBcInJlYWN0XCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwicmVhY3QtYm9vdHN0cmFwXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwicmVhY3QvanN4LWRldi1ydW50aW1lXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwic3dlZXRhbGVydDJcIiJdLCJuYW1lcyI6WyJDYXRlZ29yeSIsInJlY29yZFByb3AiLCJfaWQiLCJuYW1lIiwidHlwZSIsImNhdGVnb3J5IiwiYmFsYW5jZSIsInJlY29yZGVkT24iLCJkZXNjcmlwdGlvbiIsImFtb3VudCIsIlJlY29yZHMiLCJ1c2VyIiwidXNlQ29udGV4dCIsIlVzZXJDb250ZXh0IiwiY29uc29sZSIsImxvZyIsInVzZXJUb2tlbiIsInNldFVzZXJUb2tlbiIsInVzZVN0YXRlIiwic2VsZWN0aW9uIiwic2V0U2VsZWN0aW9uIiwiY2F0ZWdvcnlOYW1lU2VsZWN0aW9uIiwic2V0Q2F0ZWdvcnlOYW1lU2VsZWN0aW9uIiwiY2F0ZWdvcnlOYW1lIiwic2V0Q2F0ZWdvcnlOYW1lIiwicmVjb3JkQW1vdW50Iiwic2V0UmVjb3JkQW1vdW50IiwicmVjb3JkRGVzY3JpcHRpb24iLCJzZXRSZWNvcmREZXNjcmlwdGlvbiIsInNldEJhbGFuY2UiLCJkYXRlIiwic2V0RGF0ZSIsInJlY29yZHMiLCJzZXRSZWNvcmRzIiwidXNlRWZmZWN0IiwibG9jYWxTdG9yYWdlIiwiZ2V0SXRlbSIsImZldGNoIiwiaGVhZGVycyIsIkF1dGhvcml6YXRpb24iLCJ0aGVuIiwicmVzIiwianNvbiIsImRhdGEiLCJyZWNvcmRzQXJyYXkiLCJtYXAiLCJyZWNvcmQiLCJhbGxDYXRlZ29yaWVzIiwic2V0QWxsQ2F0ZWdvcmllcyIsImluY29tZUFycmF5IiwiZXhwZW5zZUFycmF5IiwiZmlsdGVyIiwicmVzdWx0IiwicHVzaCIsImNhdGVnb3JpZXNTZWxlY3QiLCJhZGRSZWNvcmQiLCJlIiwicHJldmVudERlZmF1bHQiLCJuZXdCYWxhbmNlIiwicGFyc2VJbnQiLCJtZXRob2QiLCJib2R5IiwiSlNPTiIsInN0cmluZ2lmeSIsImlkIiwiU3dhbCIsImZpcmUiLCJpY29uIiwidGl0bGUiLCJ0ZXh0IiwiUm91dGVyIiwicmVsb2FkIiwidGFyZ2V0IiwidmFsdWUiLCJSZWFjdCIsImNyZWF0ZUNvbnRleHQiLCJVc2VyUHJvdmlkZXIiLCJQcm92aWRlciJdLCJtYXBwaW5ncyI6Ijs7UUFBQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBLElBQUk7UUFDSjtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBOzs7UUFHQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMENBQTBDLGdDQUFnQztRQUMxRTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLHdEQUF3RCxrQkFBa0I7UUFDMUU7UUFDQSxpREFBaUQsY0FBYztRQUMvRDs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0EseUNBQXlDLGlDQUFpQztRQUMxRSxnSEFBZ0gsbUJBQW1CLEVBQUU7UUFDckk7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwyQkFBMkIsMEJBQTBCLEVBQUU7UUFDdkQsaUNBQWlDLGVBQWU7UUFDaEQ7UUFDQTtRQUNBOztRQUVBO1FBQ0Esc0RBQXNELCtEQUErRDs7UUFFckg7UUFDQTs7O1FBR0E7UUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN4RkE7Q0FHQTs7QUFDZSxTQUFTQSxRQUFULENBQWtCO0FBQUVDO0FBQUYsQ0FBbEIsRUFBa0M7QUFDL0M7QUFDQSxRQUFNO0FBQ0pDLE9BREk7QUFFSkMsUUFGSTtBQUdKQyxRQUhJO0FBSUpDLFlBSkk7QUFLSkMsV0FMSTtBQU1KQyxjQU5JO0FBT0pDLGVBUEk7QUFRSkM7QUFSSSxNQVNGUixVQVRKO0FBV0Esc0JBQ0UscUVBQUMsb0RBQUQ7QUFBTSxhQUFTLEVBQUMsTUFBaEI7QUFBQSwyQkFDRSxxRUFBQyxvREFBRCxDQUFNLElBQU47QUFBQSw4QkFDRSxxRUFBQyxvREFBRCxDQUFNLEtBQU47QUFBQSxrQkFBYUU7QUFBYjtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBREYsZUFFRSxxRUFBQyxvREFBRCxDQUFNLElBQU47QUFBQSxrQkFBWUM7QUFBWjtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBRkYsZUFHRSxxRUFBQyxvREFBRCxDQUFNLEtBQU47QUFBQSxrQkFBYUM7QUFBYjtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBSEYsZUFJRSxxRUFBQyxvREFBRCxDQUFNLElBQU47QUFBQSxrQkFBWUc7QUFBWjtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBSkYsZUFLRSxxRUFBQyxvREFBRCxDQUFNLElBQU47QUFBQSxrQkFBWUM7QUFBWjtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBTEYsZUFNRSxxRUFBQyxvREFBRCxDQUFNLElBQU47QUFBQSxrQkFBWUg7QUFBWjtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBTkYsZUFPRSxxRUFBQyxvREFBRCxDQUFNLElBQU47QUFBQSxrQkFBWUM7QUFBWjtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBUEY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxVQURGO0FBYUQsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzlCRDtBQUNBO0FBQ0E7Q0FHQTs7QUFDQTtBQUVBO0FBRWUsU0FBU0csT0FBVCxHQUFtQjtBQUNoQztBQUVBLFFBQU07QUFBRUM7QUFBRixNQUFXQyx3REFBVSxDQUFDQyxvREFBRCxDQUEzQjtBQUNBQyxTQUFPLENBQUNDLEdBQVIsQ0FBWUosSUFBWixFQUpnQyxDQU1oQzs7QUFFQSxRQUFNO0FBQUEsT0FBQ0ssU0FBRDtBQUFBLE9BQVlDO0FBQVosTUFBNEJDLHNEQUFRLENBQUM7QUFDekNELGdCQUFZLEVBQUU7QUFEMkIsR0FBRCxDQUExQyxDQVJnQyxDQVloQzs7QUFDQSxRQUFNO0FBQUEsT0FBQ0UsU0FBRDtBQUFBLE9BQVlDO0FBQVosTUFBNEJGLHNEQUFRLENBQUMsRUFBRCxDQUExQyxDQWJnQyxDQWNoQzs7QUFDQSxRQUFNO0FBQUEsT0FBQ0cscUJBQUQ7QUFBQSxPQUF3QkM7QUFBeEIsTUFBb0RKLHNEQUFRLENBQUMsRUFBRCxDQUFsRSxDQWZnQyxDQWdCaEM7O0FBQ0EsUUFBTTtBQUFBLE9BQUNLLFlBQUQ7QUFBQSxPQUFlQztBQUFmLE1BQWtDTixzREFBUSxDQUFDLEVBQUQsQ0FBaEQsQ0FqQmdDLENBa0JoQzs7QUFDQSxRQUFNO0FBQUEsT0FBQ08sWUFBRDtBQUFBLE9BQWVDO0FBQWYsTUFBa0NSLHNEQUFRLENBQUMsQ0FBRCxDQUFoRCxDQW5CZ0MsQ0FvQmhDOztBQUNBLFFBQU07QUFBQSxPQUFDUyxpQkFBRDtBQUFBLE9BQW9CQztBQUFwQixNQUE0Q1Ysc0RBQVEsQ0FBQyxFQUFELENBQTFELENBckJnQyxDQXVCaEM7O0FBQ0EsUUFBTTtBQUFBLE9BQUNaLE9BQUQ7QUFBQSxPQUFVdUI7QUFBVixNQUF3Qlgsc0RBQVEsQ0FBQyxDQUFELENBQXRDLENBeEJnQyxDQTBCaEM7O0FBQ0EsUUFBTTtBQUFBLE9BQUNZLElBQUQ7QUFBQSxPQUFPQztBQUFQLE1BQWtCYixzREFBUSxDQUFDLEVBQUQsQ0FBaEMsQ0EzQmdDLENBNkJoQzs7QUFDQSxRQUFNO0FBQUEsT0FBQ2MsT0FBRDtBQUFBLE9BQVVDO0FBQVYsTUFBd0JmLHNEQUFRLENBQUMsRUFBRCxDQUF0QyxDQTlCZ0MsQ0FnQ2hDOztBQUVBZ0IseURBQVMsQ0FBQyxNQUFNO0FBQ2RqQixnQkFBWSxDQUFDa0IsWUFBWSxDQUFDQyxPQUFiLENBQXFCLE9BQXJCLENBQUQsQ0FBWjtBQUNELEdBRlEsRUFFTixFQUZNLENBQVQsQ0FsQ2dDLENBc0NoQzs7QUFDQUYseURBQVMsQ0FBQyxNQUFNO0FBQ2RHLFNBQUssQ0FBQyx5Q0FBRCxFQUE0QztBQUMvQ0MsYUFBTyxFQUFFO0FBQ1BDLHFCQUFhLEVBQUcsVUFBU0osWUFBWSxDQUFDQyxPQUFiLENBQXFCLE9BQXJCLENBQThCO0FBRGhEO0FBRHNDLEtBQTVDLENBQUwsQ0FLR0ksSUFMSCxDQUtTQyxHQUFELElBQVNBLEdBQUcsQ0FBQ0MsSUFBSixFQUxqQixFQU1HRixJQU5ILENBTVNHLElBQUQsSUFBVTtBQUNkZCxnQkFBVSxDQUFDYyxJQUFELENBQVY7QUFDRCxLQVJIO0FBU0QsR0FWUSxFQVVOLENBQUNyQyxPQUFELENBVk0sQ0FBVCxDQXZDZ0MsQ0FtRGhDOztBQUVBNEIseURBQVMsQ0FBQyxNQUFNO0FBQ2RHLFNBQUssQ0FBQyxpREFBRCxFQUFvRDtBQUN2REMsYUFBTyxFQUFFO0FBQ1BDLHFCQUFhLEVBQUcsVUFBU0osWUFBWSxDQUFDQyxPQUFiLENBQXFCLE9BQXJCLENBQThCO0FBRGhEO0FBRDhDLEtBQXBELENBQUwsQ0FLR0ksSUFMSCxDQUtTQyxHQUFELElBQVNBLEdBQUcsQ0FBQ0MsSUFBSixFQUxqQixFQU1HRixJQU5ILENBTVNHLElBQUQsSUFBVTtBQUNkVixnQkFBVSxDQUFDVSxJQUFELENBQVY7QUFDRCxLQVJIO0FBU0QsR0FWUSxFQVVOLEVBVk0sQ0FBVDtBQVdBN0IsU0FBTyxDQUFDQyxHQUFSLENBQVksU0FBWixFQUF1QmlCLE9BQXZCO0FBRUEsUUFBTVksWUFBWSxHQUFHWixPQUFPLENBQUNhLEdBQVIsQ0FBYUMsTUFBRCxJQUFZO0FBQzNDLHdCQUFPLHFFQUFDLDBEQUFEO0FBQXlCLGdCQUFVLEVBQUVBO0FBQXJDLE9BQWFBLE1BQU0sQ0FBQzVDLEdBQXBCO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFBUDtBQUNELEdBRm9CLENBQXJCO0FBSUEsUUFBTTtBQUFBLE9BQUM2QyxhQUFEO0FBQUEsT0FBZ0JDO0FBQWhCLE1BQW9DOUIsc0RBQVEsQ0FBQyxFQUFELENBQWxEO0FBRUFnQix5REFBUyxDQUFDLE1BQU07QUFDZEcsU0FBSyxDQUFDLCtDQUFELEVBQWtEO0FBQ3JEQyxhQUFPLEVBQUU7QUFDUEMscUJBQWEsRUFBRyxVQUFTSixZQUFZLENBQUNDLE9BQWIsQ0FBcUIsT0FBckIsQ0FBOEI7QUFEaEQ7QUFENEMsS0FBbEQsQ0FBTCxDQUtHSSxJQUxILENBS1NDLEdBQUQsSUFBU0EsR0FBRyxDQUFDQyxJQUFKLEVBTGpCLEVBTUdGLElBTkgsQ0FNU0csSUFBRCxJQUFVO0FBQ2Q3QixhQUFPLENBQUNDLEdBQVIsQ0FBWTRCLElBQVosRUFEYyxDQUVkOztBQUNBLFVBQUlNLFdBQVcsR0FBRyxFQUFsQjtBQUNBLFVBQUlDLFlBQVksR0FBRyxFQUFuQjs7QUFFQSxVQUFJL0IsU0FBUyxLQUFLLFFBQWxCLEVBQTRCO0FBQzFCd0IsWUFBSSxDQUFDUSxNQUFMLENBQWFDLE1BQUQsSUFBWTtBQUN0QixjQUFJQSxNQUFNLENBQUNoRCxJQUFQLEtBQWdCLFFBQXBCLEVBQThCO0FBQzVCNkMsdUJBQVcsQ0FBQ0ksSUFBWixDQUFpQjtBQUFFbkQsaUJBQUcsRUFBRWtELE1BQU0sQ0FBQ2xELEdBQWQ7QUFBbUJDLGtCQUFJLEVBQUVpRCxNQUFNLENBQUNqRDtBQUFoQyxhQUFqQjtBQUNEO0FBQ0YsU0FKRDtBQU1BbUIsZ0NBQXdCLENBQUMyQixXQUFELENBQXhCO0FBQ0QsT0FSRCxNQVFPO0FBQ0xOLFlBQUksQ0FBQ1EsTUFBTCxDQUFhQyxNQUFELElBQVk7QUFDdEIsY0FBSUEsTUFBTSxDQUFDaEQsSUFBUCxLQUFnQixTQUFwQixFQUErQjtBQUM3QjhDLHdCQUFZLENBQUNHLElBQWIsQ0FBa0I7QUFBRW5ELGlCQUFHLEVBQUVrRCxNQUFNLENBQUNsRCxHQUFkO0FBQW1CQyxrQkFBSSxFQUFFaUQsTUFBTSxDQUFDakQ7QUFBaEMsYUFBbEI7QUFDRDtBQUNGLFNBSkQ7QUFNQW1CLGdDQUF3QixDQUFDNEIsWUFBRCxDQUF4QjtBQUNEO0FBQ0YsS0E3Qkg7QUE4QkQsR0EvQlEsRUErQk4sQ0FBQy9CLFNBQUQsQ0EvQk0sQ0FBVCxDQXhFZ0MsQ0F5R2hDOztBQUVBLFFBQU1tQyxnQkFBZ0IsR0FBR2pDLHFCQUFxQixDQUFDd0IsR0FBdEIsQ0FBMkJ4QyxRQUFELElBQWM7QUFDL0Qsd0JBQU87QUFBQSxnQkFBNEJBLFFBQVEsQ0FBQ0Y7QUFBckMsT0FBYUUsUUFBUSxDQUFDSCxHQUF0QjtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBQVA7QUFDRCxHQUZ3QixDQUF6Qjs7QUFJQSxXQUFTcUQsU0FBVCxDQUFtQkMsQ0FBbkIsRUFBc0I7QUFDcEJBLEtBQUMsQ0FBQ0MsY0FBRixHQURvQixDQUVwQjtBQUNBO0FBQ0E7O0FBRUEsUUFBSUMsVUFBVSxHQUFHLENBQWpCOztBQUVBLFFBQUl2QyxTQUFTLEtBQUssUUFBbEIsRUFBNEI7QUFDMUJ1QyxnQkFBVSxHQUFHcEQsT0FBTyxHQUFHcUQsUUFBUSxDQUFDbEMsWUFBRCxDQUEvQjtBQUNELEtBRkQsTUFFTztBQUNMaUMsZ0JBQVUsR0FBR3BELE9BQU8sR0FBR3FELFFBQVEsQ0FBQ2xDLFlBQUQsQ0FBL0I7QUFDRDs7QUFFRFksU0FBSyxDQUFDLDZDQUFELEVBQWdEO0FBQ25EdUIsWUFBTSxFQUFFLE1BRDJDO0FBRW5EdEIsYUFBTyxFQUFFO0FBQ1Asd0JBQWdCLGtCQURUO0FBRVBDLHFCQUFhLEVBQUcsVUFBU3ZCLFNBQVU7QUFGNUIsT0FGMEM7QUFNbkQ2QyxVQUFJLEVBQUVDLElBQUksQ0FBQ0MsU0FBTCxDQUFlO0FBQ25CQyxVQUFFLEVBQUVyRCxJQUFJLENBQUNxRCxFQURVO0FBRW5CNUQsWUFBSSxFQUFFZSxTQUZhO0FBR25CZCxnQkFBUSxFQUFFa0IsWUFIUztBQUluQmQsY0FBTSxFQUFFZ0IsWUFKVztBQUtuQmpCLG1CQUFXLEVBQUVtQixpQkFMTTtBQU1uQnJCLGVBQU8sRUFBRW9ELFVBTlU7QUFPbkJuRCxrQkFBVSxFQUFFdUI7QUFQTyxPQUFmO0FBTjZDLEtBQWhELENBQUwsQ0FnQkdVLElBaEJILENBZ0JTQyxHQUFELElBQVNBLEdBQUcsQ0FBQ0MsSUFBSixFQWhCakIsRUFpQkdGLElBakJILENBaUJTRyxJQUFELElBQVU7QUFDZDdCLGFBQU8sQ0FBQ0MsR0FBUixDQUFZLFlBQVosRUFBMEI0QixJQUExQjtBQUVBTixXQUFLLENBQUMsK0NBQUQsRUFBa0Q7QUFDckR1QixjQUFNLEVBQUUsS0FENkM7QUFFckR0QixlQUFPLEVBQUU7QUFDUCwwQkFBZ0Isa0JBRFQ7QUFFUEMsdUJBQWEsRUFBRyxVQUFTdkIsU0FBVTtBQUY1QixTQUY0QztBQU1yRDZDLFlBQUksRUFBRUMsSUFBSSxDQUFDQyxTQUFMLENBQWU7QUFDbkJDLFlBQUUsRUFBRXJELElBQUksQ0FBQ3FELEVBRFU7QUFFbkIxRCxpQkFBTyxFQUFFb0Q7QUFGVSxTQUFmO0FBTitDLE9BQWxELENBQUwsQ0FXR2xCLElBWEgsQ0FXU0MsR0FBRCxJQUFTQSxHQUFHLENBQUNDLElBQUosRUFYakIsRUFZR0YsSUFaSCxDQVlTRyxJQUFELElBQVU7QUFDZGQsa0JBQVUsQ0FBQzZCLFVBQUQsQ0FBVjtBQUNELE9BZEg7O0FBZ0JBLFVBQUlmLElBQUosRUFBVTtBQUNSc0IsMERBQUksQ0FBQ0MsSUFBTCxDQUFVO0FBQ1JDLGNBQUksRUFBRSxTQURFO0FBRVJDLGVBQUssRUFBRSxZQUZDO0FBR1JDLGNBQUksRUFBRTtBQUhFLFNBQVY7QUFLQTdDLHVCQUFlLENBQUMsRUFBRCxDQUFmO0FBQ0FKLG9CQUFZLENBQUMsRUFBRCxDQUFaO0FBQ0FNLHVCQUFlLENBQUMsQ0FBRCxDQUFmO0FBQ0FFLDRCQUFvQixDQUFDLEVBQUQsQ0FBcEI7QUFDQTBDLDBEQUFNLENBQUNDLE1BQVA7QUFDRCxPQVhELE1BV087QUFDTE4sMERBQUksQ0FBQ0MsSUFBTCxDQUFVO0FBQ1JDLGNBQUksRUFBRSxPQURFO0FBRVJDLGVBQUssRUFBRSwwQkFGQztBQUdSQyxjQUFJLEVBQUU7QUFIRSxTQUFWO0FBS0Q7QUFDRixLQXRESDtBQXVERDs7QUFFRCxzQkFDRSxxRUFBQyx5REFBRDtBQUFBLDJCQUNFLHFFQUFDLG1EQUFEO0FBQUEsOEJBQ0UscUVBQUMsbURBQUQ7QUFBSyxVQUFFLEVBQUUsRUFBVDtBQUFhLFVBQUUsRUFBRSxDQUFqQjtBQUFvQixpQkFBUyxFQUFDLE1BQTlCO0FBQUEsZ0NBQ0U7QUFBSSxtQkFBUyxFQUFDLGtCQUFkO0FBQUEsc0NBQStDL0QsT0FBL0M7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQURGLGVBRUU7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBRkYsZUFHRSxxRUFBQyxvREFBRDtBQUFNLGtCQUFRLEVBQUdrRCxDQUFELElBQU9ELFNBQVMsQ0FBQ0MsQ0FBRCxDQUFoQztBQUFBLGtDQUNFLHFFQUFDLG9EQUFELENBQU0sS0FBTjtBQUFZLHFCQUFTLEVBQUMsZ0JBQXRCO0FBQUEsb0NBQ0UscUVBQUMsb0RBQUQsQ0FBTSxLQUFOO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQURGLGVBRUUscUVBQUMsb0RBQUQsQ0FBTSxPQUFOO0FBQ0UsZ0JBQUUsRUFBQyxRQURMO0FBRUUsc0JBQVEsTUFGVjtBQUdFLG1CQUFLLEVBQUVyQyxTQUhUO0FBSUUsc0JBQVEsRUFBR3FDLENBQUQsSUFBT3BDLFlBQVksQ0FBQ29DLENBQUMsQ0FBQ2dCLE1BQUYsQ0FBU0MsS0FBVixDQUovQjtBQUFBLHNDQU1FO0FBQVEscUJBQUssRUFBQyxFQUFkO0FBQWlCLHdCQUFRLE1BQXpCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQU5GLGVBU0U7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0JBVEYsZUFVRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQkFWRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBRkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQURGLGVBZ0JFLHFFQUFDLG9EQUFELENBQU0sS0FBTjtBQUFZLHFCQUFTLEVBQUMsZ0JBQXRCO0FBQUEsb0NBQ0UscUVBQUMsb0RBQUQsQ0FBTSxLQUFOO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQURGLGVBRUUscUVBQUMsb0RBQUQsQ0FBTSxPQUFOO0FBQ0UsZ0JBQUUsRUFBQyxRQURMO0FBRUUsbUJBQUssRUFBRWxELFlBRlQ7QUFHRSxzQkFBUSxFQUFHaUMsQ0FBRCxJQUFPaEMsZUFBZSxDQUFDZ0MsQ0FBQyxDQUFDZ0IsTUFBRixDQUFTQyxLQUFWLENBSGxDO0FBQUEsc0NBS0U7QUFBUSxxQkFBSyxFQUFDLEVBQWQ7QUFBaUIsd0JBQVEsTUFBekI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0JBTEYsRUFTR25CLGdCQVRIO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFGRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBaEJGLGVBOEJFLHFFQUFDLG9EQUFELENBQU0sS0FBTjtBQUFZLHFCQUFTLEVBQUMsU0FBdEI7QUFBQSxvQ0FDRSxxRUFBQyxvREFBRCxDQUFNLEtBQU47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBREYsZUFFRSxxRUFBQyxvREFBRCxDQUFNLE9BQU47QUFDRSxrQkFBSSxFQUFDLFFBRFA7QUFFRSx5QkFBVyxFQUFDLGNBRmQ7QUFHRSxtQkFBSyxFQUFFN0IsWUFIVDtBQUlFLHNCQUFRLEVBQUcrQixDQUFELElBQU85QixlQUFlLENBQUM4QixDQUFDLENBQUNnQixNQUFGLENBQVNDLEtBQVYsQ0FKbEM7QUFLRSxzQkFBUTtBQUxWO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBRkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQTlCRixlQXdDRSxxRUFBQyxvREFBRCxDQUFNLEtBQU47QUFBWSxxQkFBUyxFQUFDLE9BQXRCO0FBQUEsb0NBQ0UscUVBQUMsb0RBQUQsQ0FBTSxLQUFOO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQURGLGVBRUUscUVBQUMsb0RBQUQsQ0FBTSxPQUFOO0FBQ0Usa0JBQUksRUFBQyxNQURQO0FBRUUseUJBQVcsRUFBQyxtQkFGZDtBQUdFLG1CQUFLLEVBQUU5QyxpQkFIVDtBQUlFLHNCQUFRLEVBQUc2QixDQUFELElBQU81QixvQkFBb0IsQ0FBQzRCLENBQUMsQ0FBQ2dCLE1BQUYsQ0FBU0MsS0FBVixDQUp2QztBQUtFLHNCQUFRO0FBTFY7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFGRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBeENGLGVBa0RFLHFFQUFDLG9EQUFELENBQU0sS0FBTjtBQUFZLHFCQUFTLEVBQUMsT0FBdEI7QUFBQSxvQ0FDRSxxRUFBQyxvREFBRCxDQUFNLEtBQU47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBREYsZUFFRSxxRUFBQyxvREFBRCxDQUFNLE9BQU47QUFDRSxrQkFBSSxFQUFDLE1BRFA7QUFFRSx5QkFBVyxFQUFDLFlBRmQ7QUFHRSxtQkFBSyxFQUFFM0MsSUFIVDtBQUlFLHNCQUFRLEVBQUcwQixDQUFELElBQU96QixPQUFPLENBQUN5QixDQUFDLENBQUNnQixNQUFGLENBQVNDLEtBQVYsQ0FKMUI7QUFLRSxzQkFBUTtBQUxWO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBRkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQWxERixlQTRERSxxRUFBQyxzREFBRDtBQUFRLG1CQUFPLEVBQUMsU0FBaEI7QUFBMEIsZ0JBQUksRUFBQyxRQUEvQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkE1REY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQUhGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQURGLGVBcUVFLHFFQUFDLG1EQUFEO0FBQUssVUFBRSxFQUFFLEVBQVQ7QUFBYSxVQUFFLEVBQUUsQ0FBakI7QUFBb0IsaUJBQVMsRUFBQyxNQUE5QjtBQUFBLGdDQUNFO0FBQUksbUJBQVMsRUFBQyxNQUFkO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQURGLEVBRUc3QixZQUZIO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQXJFRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLFVBREY7QUE4RUQsQzs7Ozs7Ozs7Ozs7O0FDOVFEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQSxNQUFNL0IsV0FBVyxnQkFBRzZELDRDQUFLLENBQUNDLGFBQU4sRUFBcEI7QUFDTyxNQUFNQyxZQUFZLEdBQUcvRCxXQUFXLENBQUNnRSxRQUFqQztBQUNRaEUsMEVBQWYsRTs7Ozs7Ozs7Ozs7QUNIQSx3Qzs7Ozs7Ozs7Ozs7QUNBQSxrQzs7Ozs7Ozs7Ozs7QUNBQSw0Qzs7Ozs7Ozs7Ozs7QUNBQSxrRDs7Ozs7Ozs7Ozs7QUNBQSx3QyIsImZpbGUiOiJwYWdlcy9yZWNvcmRzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSByZXF1aXJlKCcuLi9zc3ItbW9kdWxlLWNhY2hlLmpzJyk7XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdHZhciB0aHJldyA9IHRydWU7XG4gXHRcdHRyeSB7XG4gXHRcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG4gXHRcdFx0dGhyZXcgPSBmYWxzZTtcbiBcdFx0fSBmaW5hbGx5IHtcbiBcdFx0XHRpZih0aHJldykgZGVsZXRlIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdO1xuIFx0XHR9XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZ2V0dGVyIH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSBmdW5jdGlvbihleHBvcnRzKSB7XG4gXHRcdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuIFx0XHR9XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG4gXHR9O1xuXG4gXHQvLyBjcmVhdGUgYSBmYWtlIG5hbWVzcGFjZSBvYmplY3RcbiBcdC8vIG1vZGUgJiAxOiB2YWx1ZSBpcyBhIG1vZHVsZSBpZCwgcmVxdWlyZSBpdFxuIFx0Ly8gbW9kZSAmIDI6IG1lcmdlIGFsbCBwcm9wZXJ0aWVzIG9mIHZhbHVlIGludG8gdGhlIG5zXG4gXHQvLyBtb2RlICYgNDogcmV0dXJuIHZhbHVlIHdoZW4gYWxyZWFkeSBucyBvYmplY3RcbiBcdC8vIG1vZGUgJiA4fDE6IGJlaGF2ZSBsaWtlIHJlcXVpcmVcbiBcdF9fd2VicGFja19yZXF1aXJlX18udCA9IGZ1bmN0aW9uKHZhbHVlLCBtb2RlKSB7XG4gXHRcdGlmKG1vZGUgJiAxKSB2YWx1ZSA9IF9fd2VicGFja19yZXF1aXJlX18odmFsdWUpO1xuIFx0XHRpZihtb2RlICYgOCkgcmV0dXJuIHZhbHVlO1xuIFx0XHRpZigobW9kZSAmIDQpICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiYgdmFsdWUgJiYgdmFsdWUuX19lc01vZHVsZSkgcmV0dXJuIHZhbHVlO1xuIFx0XHR2YXIgbnMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIobnMpO1xuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkobnMsICdkZWZhdWx0JywgeyBlbnVtZXJhYmxlOiB0cnVlLCB2YWx1ZTogdmFsdWUgfSk7XG4gXHRcdGlmKG1vZGUgJiAyICYmIHR5cGVvZiB2YWx1ZSAhPSAnc3RyaW5nJykgZm9yKHZhciBrZXkgaW4gdmFsdWUpIF9fd2VicGFja19yZXF1aXJlX18uZChucywga2V5LCBmdW5jdGlvbihrZXkpIHsgcmV0dXJuIHZhbHVlW2tleV07IH0uYmluZChudWxsLCBrZXkpKTtcbiBcdFx0cmV0dXJuIG5zO1xuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IFwiLi9wYWdlcy9yZWNvcmRzL2luZGV4LmpzXCIpO1xuIiwiaW1wb3J0IHsgdXNlU3RhdGUsIHVzZUVmZmVjdCwgdXNlQ29udGV4dCB9IGZyb20gXCJyZWFjdFwiO1xyXG5pbXBvcnQgeyBDYXJkIH0gZnJvbSBcInJlYWN0LWJvb3RzdHJhcFwiO1xyXG5cclxuLy8gVG8gZ2V0IHRoZSBwcm9wLCBkZXN0cnVjdHVyZSBpdFxyXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBDYXRlZ29yeSh7IHJlY29yZFByb3AgfSkge1xyXG4gIC8vICBhZnRlciB5b3UgZ2V0IHRoZSBwcm9wLCBkZXN0cnVjdHVyZSBpdCBwYXJhIG1ha3VoYSBtbyB5bmcgbGFtYW4gbmEgbmVlZCBtbzpcclxuICBjb25zdCB7XHJcbiAgICBfaWQsXHJcbiAgICBuYW1lLFxyXG4gICAgdHlwZSxcclxuICAgIGNhdGVnb3J5LFxyXG4gICAgYmFsYW5jZSxcclxuICAgIHJlY29yZGVkT24sXHJcbiAgICBkZXNjcmlwdGlvbixcclxuICAgIGFtb3VudCxcclxuICB9ID0gcmVjb3JkUHJvcDtcclxuXHJcbiAgcmV0dXJuIChcclxuICAgIDxDYXJkIGNsYXNzTmFtZT1cIm15LTNcIj5cclxuICAgICAgPENhcmQuQm9keT5cclxuICAgICAgICA8Q2FyZC5UaXRsZT57bmFtZX08L0NhcmQuVGl0bGU+XHJcbiAgICAgICAgPENhcmQuVGV4dD57dHlwZX08L0NhcmQuVGV4dD5cclxuICAgICAgICA8Q2FyZC5UaXRsZT57Y2F0ZWdvcnl9PC9DYXJkLlRpdGxlPlxyXG4gICAgICAgIDxDYXJkLlRleHQ+e2Rlc2NyaXB0aW9ufTwvQ2FyZC5UZXh0PlxyXG4gICAgICAgIDxDYXJkLlRleHQ+e2Ftb3VudH08L0NhcmQuVGV4dD5cclxuICAgICAgICA8Q2FyZC5UZXh0PntiYWxhbmNlfTwvQ2FyZC5UZXh0PlxyXG4gICAgICAgIDxDYXJkLlRleHQ+e3JlY29yZGVkT259PC9DYXJkLlRleHQ+XHJcbiAgICAgIDwvQ2FyZC5Cb2R5PlxyXG4gICAgPC9DYXJkPlxyXG4gICk7XHJcbn1cclxuIiwiaW1wb3J0IHsgdXNlU3RhdGUsIHVzZUVmZmVjdCwgdXNlQ29udGV4dCB9IGZyb20gXCJyZWFjdFwiO1xyXG5pbXBvcnQgeyBGb3JtLCBCdXR0b24sIENvbnRhaW5lciwgUm93LCBDb2wsIENhcmQgfSBmcm9tIFwicmVhY3QtYm9vdHN0cmFwXCI7XHJcbmltcG9ydCBTd2FsIGZyb20gXCJzd2VldGFsZXJ0MlwiO1xyXG5cclxuaW1wb3J0IFJvdXRlciBmcm9tIFwibmV4dC9yb3V0ZXJcIjtcclxuLy9pbXBvcnQgdXNlciBjb250ZXh0XHJcbmltcG9ydCBVc2VyQ29udGV4dCBmcm9tIFwiLi4vLi4vdXNlckNvbnRleHRcIjtcclxuXHJcbmltcG9ydCBSZWNvcmQgZnJvbSBcIi4uLy4uL2NvbXBvbmVudHMvUmVjb3JkXCI7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBSZWNvcmRzKCkge1xyXG4gIC8vIFJlcGxhY2VkIENhdGVnb3JpZXMgdG8gJ1JlY29yZHMnXHJcblxyXG4gIGNvbnN0IHsgdXNlciB9ID0gdXNlQ29udGV4dChVc2VyQ29udGV4dCk7XHJcbiAgY29uc29sZS5sb2codXNlcik7XHJcblxyXG4gIC8vU3RhdGUgZm9yIHRva2VuXHJcblxyXG4gIGNvbnN0IFt1c2VyVG9rZW4sIHNldFVzZXJUb2tlbl0gPSB1c2VTdGF0ZSh7XHJcbiAgICBzZXRVc2VyVG9rZW46IG51bGwsXHJcbiAgfSk7XHJcblxyXG4gIC8vU3RhdGUgZm9yIENhdGVnb3J5IFR5cGUgc2VsZXRpb25cclxuICBjb25zdCBbc2VsZWN0aW9uLCBzZXRTZWxlY3Rpb25dID0gdXNlU3RhdGUoXCJcIik7XHJcbiAgLy9TdGF0ZSBmb3IgQ2F0ZWdvcnkgTmFtZSBzZWxlY3Rpb25cclxuICBjb25zdCBbY2F0ZWdvcnlOYW1lU2VsZWN0aW9uLCBzZXRDYXRlZ29yeU5hbWVTZWxlY3Rpb25dID0gdXNlU3RhdGUoW10pO1xyXG4gIC8vU3RhdGUgZm9yIGZpbmFsIENhdGVnb3J5IE5hbWUgc2VsZWN0aW9uXHJcbiAgY29uc3QgW2NhdGVnb3J5TmFtZSwgc2V0Q2F0ZWdvcnlOYW1lXSA9IHVzZVN0YXRlKFwiXCIpO1xyXG4gIC8vU3RhdGUgZm9yIGFtb3VudCBmaWVsZFxyXG4gIGNvbnN0IFtyZWNvcmRBbW91bnQsIHNldFJlY29yZEFtb3VudF0gPSB1c2VTdGF0ZSgwKTtcclxuICAvL1N0YXRlIGZvciBkZXNjcmlwdGlvbiBmaWVsZFxyXG4gIGNvbnN0IFtyZWNvcmREZXNjcmlwdGlvbiwgc2V0UmVjb3JkRGVzY3JpcHRpb25dID0gdXNlU3RhdGUoXCJcIik7XHJcblxyXG4gIC8vU3RhdGUgZm9yIGJhbGFuY2VcclxuICBjb25zdCBbYmFsYW5jZSwgc2V0QmFsYW5jZV0gPSB1c2VTdGF0ZSgwKTtcclxuXHJcbiAgLy9TdGF0ZSBmb3IgRGF0ZVxyXG4gIGNvbnN0IFtkYXRlLCBzZXREYXRlXSA9IHVzZVN0YXRlKFwiXCIpO1xyXG5cclxuICAvL1N0YXRlIGZvciB0aGUgYWxsIHJlY29yZHNcclxuICBjb25zdCBbcmVjb3Jkcywgc2V0UmVjb3Jkc10gPSB1c2VTdGF0ZShbXSk7XHJcblxyXG4gIC8vdXNlRWZmZWN0IGZvciB0aGUgVG9rZW5cclxuXHJcbiAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgIHNldFVzZXJUb2tlbihsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcInRva2VuXCIpKTtcclxuICB9LCBbXSk7XHJcblxyXG4gIC8vdXNlRWZmZWN0IGZvciBCYWxhbmNlXHJcbiAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgIGZldGNoKFwiaHR0cDovL2xvY2FsaG9zdDo4MDAwL2FwaS91c2Vycy9iYWxhbmNlXCIsIHtcclxuICAgICAgaGVhZGVyczoge1xyXG4gICAgICAgIEF1dGhvcml6YXRpb246IGBCZWFyZXIgJHtsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcInRva2VuXCIpfWAsXHJcbiAgICAgIH0sXHJcbiAgICB9KVxyXG4gICAgICAudGhlbigocmVzKSA9PiByZXMuanNvbigpKVxyXG4gICAgICAudGhlbigoZGF0YSkgPT4ge1xyXG4gICAgICAgIHNldEJhbGFuY2UoZGF0YSk7XHJcbiAgICAgIH0pO1xyXG4gIH0sIFtiYWxhbmNlXSk7XHJcblxyXG4gIC8vdXNlRWZmZWN0IGZvciB0aGUgYWxsIHJlY29yZHNcclxuXHJcbiAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgIGZldGNoKFwiaHR0cDovL2xvY2FsaG9zdDo4MDAwL2FwaS91c2Vycy9hbGxUcmFuc2FjdGlvbnNcIiwge1xyXG4gICAgICBoZWFkZXJzOiB7XHJcbiAgICAgICAgQXV0aG9yaXphdGlvbjogYEJlYXJlciAke2xvY2FsU3RvcmFnZS5nZXRJdGVtKFwidG9rZW5cIil9YCxcclxuICAgICAgfSxcclxuICAgIH0pXHJcbiAgICAgIC50aGVuKChyZXMpID0+IHJlcy5qc29uKCkpXHJcbiAgICAgIC50aGVuKChkYXRhKSA9PiB7XHJcbiAgICAgICAgc2V0UmVjb3JkcyhkYXRhKTtcclxuICAgICAgfSk7XHJcbiAgfSwgW10pO1xyXG4gIGNvbnNvbGUubG9nKFwiUmVjb3Jkc1wiLCByZWNvcmRzKTtcclxuXHJcbiAgY29uc3QgcmVjb3Jkc0FycmF5ID0gcmVjb3Jkcy5tYXAoKHJlY29yZCkgPT4ge1xyXG4gICAgcmV0dXJuIDxSZWNvcmQga2V5PXtyZWNvcmQuX2lkfSByZWNvcmRQcm9wPXtyZWNvcmR9IC8+O1xyXG4gIH0pO1xyXG5cclxuICBjb25zdCBbYWxsQ2F0ZWdvcmllcywgc2V0QWxsQ2F0ZWdvcmllc10gPSB1c2VTdGF0ZShbXSk7XHJcblxyXG4gIHVzZUVmZmVjdCgoKSA9PiB7XHJcbiAgICBmZXRjaChcImh0dHA6Ly9sb2NhbGhvc3Q6ODAwMC9hcGkvdXNlcnMvYWxsQ2F0ZWdvcmllc1wiLCB7XHJcbiAgICAgIGhlYWRlcnM6IHtcclxuICAgICAgICBBdXRob3JpemF0aW9uOiBgQmVhcmVyICR7bG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJ0b2tlblwiKX1gLFxyXG4gICAgICB9LFxyXG4gICAgfSlcclxuICAgICAgLnRoZW4oKHJlcykgPT4gcmVzLmpzb24oKSlcclxuICAgICAgLnRoZW4oKGRhdGEpID0+IHtcclxuICAgICAgICBjb25zb2xlLmxvZyhkYXRhKTtcclxuICAgICAgICAvLyBzZXRBbGxDYXRlZ29yaWVzKGRhdGEpXHJcbiAgICAgICAgbGV0IGluY29tZUFycmF5ID0gW107XHJcbiAgICAgICAgbGV0IGV4cGVuc2VBcnJheSA9IFtdO1xyXG5cclxuICAgICAgICBpZiAoc2VsZWN0aW9uID09PSBcIkluY29tZVwiKSB7XHJcbiAgICAgICAgICBkYXRhLmZpbHRlcigocmVzdWx0KSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChyZXN1bHQudHlwZSA9PT0gXCJJbmNvbWVcIikge1xyXG4gICAgICAgICAgICAgIGluY29tZUFycmF5LnB1c2goeyBfaWQ6IHJlc3VsdC5faWQsIG5hbWU6IHJlc3VsdC5uYW1lIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICBzZXRDYXRlZ29yeU5hbWVTZWxlY3Rpb24oaW5jb21lQXJyYXkpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICBkYXRhLmZpbHRlcigocmVzdWx0KSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChyZXN1bHQudHlwZSA9PT0gXCJFeHBlbnNlXCIpIHtcclxuICAgICAgICAgICAgICBleHBlbnNlQXJyYXkucHVzaCh7IF9pZDogcmVzdWx0Ll9pZCwgbmFtZTogcmVzdWx0Lm5hbWUgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgIHNldENhdGVnb3J5TmFtZVNlbGVjdGlvbihleHBlbnNlQXJyYXkpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgfSwgW3NlbGVjdGlvbl0pO1xyXG5cclxuICAvLyBjb25zb2xlLmxvZyhjYXRlZ29yeU5hbWVTZWxlY3Rpb24pXHJcblxyXG4gIGNvbnN0IGNhdGVnb3JpZXNTZWxlY3QgPSBjYXRlZ29yeU5hbWVTZWxlY3Rpb24ubWFwKChjYXRlZ29yeSkgPT4ge1xyXG4gICAgcmV0dXJuIDxvcHRpb24ga2V5PXtjYXRlZ29yeS5faWR9PntjYXRlZ29yeS5uYW1lfTwvb3B0aW9uPjtcclxuICB9KTtcclxuXHJcbiAgZnVuY3Rpb24gYWRkUmVjb3JkKGUpIHtcclxuICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgIC8vIGNvbnNvbGUubG9nKCd1c2VyVG9rZW4nLCB1c2VyVG9rZW4pXHJcbiAgICAvLyBjb25zb2xlLmxvZygnc2VsZWN0aW9uJyxzZWxlY3Rpb24pXHJcbiAgICAvLyBjb25zb2xlLmxvZygnY2F0ZWdvcnlOYW1lJyxjYXRlZ29yeU5hbWUpXHJcblxyXG4gICAgbGV0IG5ld0JhbGFuY2UgPSAwO1xyXG5cclxuICAgIGlmIChzZWxlY3Rpb24gPT09IFwiSW5jb21lXCIpIHtcclxuICAgICAgbmV3QmFsYW5jZSA9IGJhbGFuY2UgKyBwYXJzZUludChyZWNvcmRBbW91bnQpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgbmV3QmFsYW5jZSA9IGJhbGFuY2UgLSBwYXJzZUludChyZWNvcmRBbW91bnQpO1xyXG4gICAgfVxyXG5cclxuICAgIGZldGNoKFwiaHR0cDovL2xvY2FsaG9zdDo4MDAwL2FwaS91c2Vycy90cmFuc2FjdGlvblwiLCB7XHJcbiAgICAgIG1ldGhvZDogXCJQT1NUXCIsXHJcbiAgICAgIGhlYWRlcnM6IHtcclxuICAgICAgICBcIkNvbnRlbnQtVHlwZVwiOiBcImFwcGxpY2F0aW9uL2pzb25cIixcclxuICAgICAgICBBdXRob3JpemF0aW9uOiBgQmVhcmVyICR7dXNlclRva2VufWAsXHJcbiAgICAgIH0sXHJcbiAgICAgIGJvZHk6IEpTT04uc3RyaW5naWZ5KHtcclxuICAgICAgICBpZDogdXNlci5pZCxcclxuICAgICAgICB0eXBlOiBzZWxlY3Rpb24sXHJcbiAgICAgICAgY2F0ZWdvcnk6IGNhdGVnb3J5TmFtZSxcclxuICAgICAgICBhbW91bnQ6IHJlY29yZEFtb3VudCxcclxuICAgICAgICBkZXNjcmlwdGlvbjogcmVjb3JkRGVzY3JpcHRpb24sXHJcbiAgICAgICAgYmFsYW5jZTogbmV3QmFsYW5jZSxcclxuICAgICAgICByZWNvcmRlZE9uOiBkYXRlLFxyXG4gICAgICB9KSxcclxuICAgIH0pXHJcbiAgICAgIC50aGVuKChyZXMpID0+IHJlcy5qc29uKCkpXHJcbiAgICAgIC50aGVuKChkYXRhKSA9PiB7XHJcbiAgICAgICAgY29uc29sZS5sb2coXCJuZXdCYWxhbmNlXCIsIGRhdGEpO1xyXG5cclxuICAgICAgICBmZXRjaChcImh0dHA6Ly9sb2NhbGhvc3Q6ODAwMC9hcGkvdXNlcnMvdXBkYXRlQmFsYW5jZVwiLCB7XHJcbiAgICAgICAgICBtZXRob2Q6IFwiUFVUXCIsXHJcbiAgICAgICAgICBoZWFkZXJzOiB7XHJcbiAgICAgICAgICAgIFwiQ29udGVudC1UeXBlXCI6IFwiYXBwbGljYXRpb24vanNvblwiLFxyXG4gICAgICAgICAgICBBdXRob3JpemF0aW9uOiBgQmVhcmVyICR7dXNlclRva2VufWAsXHJcbiAgICAgICAgICB9LFxyXG4gICAgICAgICAgYm9keTogSlNPTi5zdHJpbmdpZnkoe1xyXG4gICAgICAgICAgICBpZDogdXNlci5pZCxcclxuICAgICAgICAgICAgYmFsYW5jZTogbmV3QmFsYW5jZSxcclxuICAgICAgICAgIH0pLFxyXG4gICAgICAgIH0pXHJcbiAgICAgICAgICAudGhlbigocmVzKSA9PiByZXMuanNvbigpKVxyXG4gICAgICAgICAgLnRoZW4oKGRhdGEpID0+IHtcclxuICAgICAgICAgICAgc2V0QmFsYW5jZShuZXdCYWxhbmNlKTtcclxuICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICBpZiAoZGF0YSkge1xyXG4gICAgICAgICAgU3dhbC5maXJlKHtcclxuICAgICAgICAgICAgaWNvbjogXCJzdWNjZXNzXCIsXHJcbiAgICAgICAgICAgIHRpdGxlOiBcIiBTdWNjZXNzIS5cIixcclxuICAgICAgICAgICAgdGV4dDogXCJOZXcgdHJhbnNhY3Rpb24gaXMgcmVjb3JkZWQuXCIsXHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICAgIHNldENhdGVnb3J5TmFtZShcIlwiKTtcclxuICAgICAgICAgIHNldFNlbGVjdGlvbihcIlwiKTtcclxuICAgICAgICAgIHNldFJlY29yZEFtb3VudCgwKTtcclxuICAgICAgICAgIHNldFJlY29yZERlc2NyaXB0aW9uKFwiXCIpO1xyXG4gICAgICAgICAgUm91dGVyLnJlbG9hZCgpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICBTd2FsLmZpcmUoe1xyXG4gICAgICAgICAgICBpY29uOiBcImVycm9yXCIsXHJcbiAgICAgICAgICAgIHRpdGxlOiBcIkNhdGVnb3J5IENyZWF0aW9uIGZhaWxlZFwiLFxyXG4gICAgICAgICAgICB0ZXh0OiBcIlBsZWFzZSB0cnkgYWdhaW4uXCIsXHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgcmV0dXJuIChcclxuICAgIDxDb250YWluZXI+XHJcbiAgICAgIDxSb3c+XHJcbiAgICAgICAgPENvbCB4cz17MTJ9IG1kPXs2fSBjbGFzc05hbWU9XCJteS0zXCI+XHJcbiAgICAgICAgICA8aDQgY2xhc3NOYW1lPVwidGV4dC1jZW50ZXIgbXktM1wiPkJhbGFuY2U6IFBIUCB7YmFsYW5jZX08L2g0PlxyXG4gICAgICAgICAgPGg0PkFkZCBUcmFuc2FjdGlvbjwvaDQ+XHJcbiAgICAgICAgICA8Rm9ybSBvblN1Ym1pdD17KGUpID0+IGFkZFJlY29yZChlKX0+XHJcbiAgICAgICAgICAgIDxGb3JtLkdyb3VwIGNvbnRyb2xJZD1cInNlbGVjdGlvbkxhYmVsXCI+XHJcbiAgICAgICAgICAgICAgPEZvcm0uTGFiZWw+Q2F0ZWdvcnkgVHlwZTwvRm9ybS5MYWJlbD5cclxuICAgICAgICAgICAgICA8Rm9ybS5Db250cm9sXHJcbiAgICAgICAgICAgICAgICBhcz1cInNlbGVjdFwiXHJcbiAgICAgICAgICAgICAgICByZXF1aXJlZFxyXG4gICAgICAgICAgICAgICAgdmFsdWU9e3NlbGVjdGlvbn1cclxuICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsoZSkgPT4gc2V0U2VsZWN0aW9uKGUudGFyZ2V0LnZhbHVlKX1cclxuICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICA8b3B0aW9uIHZhbHVlPVwiXCIgZGlzYWJsZWQ+XHJcbiAgICAgICAgICAgICAgICAgIFBsZWFzZSBTZWxlY3QgdHlwZVxyXG4gICAgICAgICAgICAgICAgPC9vcHRpb24+XHJcbiAgICAgICAgICAgICAgICA8b3B0aW9uPkluY29tZTwvb3B0aW9uPlxyXG4gICAgICAgICAgICAgICAgPG9wdGlvbj5FeHBlbnNlPC9vcHRpb24+XHJcbiAgICAgICAgICAgICAgPC9Gb3JtLkNvbnRyb2w+XHJcbiAgICAgICAgICAgIDwvRm9ybS5Hcm91cD5cclxuICAgICAgICAgICAgPEZvcm0uR3JvdXAgY29udHJvbElkPVwic2VsZWN0aW9uTGFiZTJcIj5cclxuICAgICAgICAgICAgICA8Rm9ybS5MYWJlbD5DYXRlZ29yeSBOYW1lPC9Gb3JtLkxhYmVsPlxyXG4gICAgICAgICAgICAgIDxGb3JtLkNvbnRyb2xcclxuICAgICAgICAgICAgICAgIGFzPVwic2VsZWN0XCJcclxuICAgICAgICAgICAgICAgIHZhbHVlPXtjYXRlZ29yeU5hbWV9XHJcbiAgICAgICAgICAgICAgICBvbkNoYW5nZT17KGUpID0+IHNldENhdGVnb3J5TmFtZShlLnRhcmdldC52YWx1ZSl9XHJcbiAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgPG9wdGlvbiB2YWx1ZT1cIlwiIGRpc2FibGVkPlxyXG4gICAgICAgICAgICAgICAgICBTZWxlY3QgQ2F0ZWdvcnkgTmFtZVxyXG4gICAgICAgICAgICAgICAgPC9vcHRpb24+XHJcblxyXG4gICAgICAgICAgICAgICAge2NhdGVnb3JpZXNTZWxlY3R9XHJcbiAgICAgICAgICAgICAgPC9Gb3JtLkNvbnRyb2w+XHJcbiAgICAgICAgICAgIDwvRm9ybS5Hcm91cD5cclxuICAgICAgICAgICAgPEZvcm0uR3JvdXAgY29udHJvbElkPVwiY2Ftb3VudFwiPlxyXG4gICAgICAgICAgICAgIDxGb3JtLkxhYmVsPkFtb3VudDo8L0Zvcm0uTGFiZWw+XHJcbiAgICAgICAgICAgICAgPEZvcm0uQ29udHJvbFxyXG4gICAgICAgICAgICAgICAgdHlwZT1cIm51bWJlclwiXHJcbiAgICAgICAgICAgICAgICBwbGFjZWhvbGRlcj1cIkVudGVyIEFtb3VudFwiXHJcbiAgICAgICAgICAgICAgICB2YWx1ZT17cmVjb3JkQW1vdW50fVxyXG4gICAgICAgICAgICAgICAgb25DaGFuZ2U9eyhlKSA9PiBzZXRSZWNvcmRBbW91bnQoZS50YXJnZXQudmFsdWUpfVxyXG4gICAgICAgICAgICAgICAgcmVxdWlyZWRcclxuICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICA8L0Zvcm0uR3JvdXA+XHJcbiAgICAgICAgICAgIDxGb3JtLkdyb3VwIGNvbnRyb2xJZD1cImNkZXNjXCI+XHJcbiAgICAgICAgICAgICAgPEZvcm0uTGFiZWw+RGVzY3JpcHRpb246PC9Gb3JtLkxhYmVsPlxyXG4gICAgICAgICAgICAgIDxGb3JtLkNvbnRyb2xcclxuICAgICAgICAgICAgICAgIHR5cGU9XCJ0ZXh0XCJcclxuICAgICAgICAgICAgICAgIHBsYWNlaG9sZGVyPVwiRW50ZXIgRGVzY3JpcHRpb25cIlxyXG4gICAgICAgICAgICAgICAgdmFsdWU9e3JlY29yZERlc2NyaXB0aW9ufVxyXG4gICAgICAgICAgICAgICAgb25DaGFuZ2U9eyhlKSA9PiBzZXRSZWNvcmREZXNjcmlwdGlvbihlLnRhcmdldC52YWx1ZSl9XHJcbiAgICAgICAgICAgICAgICByZXF1aXJlZFxyXG4gICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgIDwvRm9ybS5Hcm91cD5cclxuICAgICAgICAgICAgPEZvcm0uR3JvdXAgY29udHJvbElkPVwiY2RhdGVcIj5cclxuICAgICAgICAgICAgICA8Rm9ybS5MYWJlbD5EZXNjcmlwdGlvbjo8L0Zvcm0uTGFiZWw+XHJcbiAgICAgICAgICAgICAgPEZvcm0uQ29udHJvbFxyXG4gICAgICAgICAgICAgICAgdHlwZT1cImRhdGVcIlxyXG4gICAgICAgICAgICAgICAgcGxhY2Vob2xkZXI9XCJFbnRlciBEYXRlXCJcclxuICAgICAgICAgICAgICAgIHZhbHVlPXtkYXRlfVxyXG4gICAgICAgICAgICAgICAgb25DaGFuZ2U9eyhlKSA9PiBzZXREYXRlKGUudGFyZ2V0LnZhbHVlKX1cclxuICAgICAgICAgICAgICAgIHJlcXVpcmVkXHJcbiAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgPC9Gb3JtLkdyb3VwPlxyXG4gICAgICAgICAgICA8QnV0dG9uIHZhcmlhbnQ9XCJwcmltYXJ5XCIgdHlwZT1cInN1Ym1pdFwiPlxyXG4gICAgICAgICAgICAgIFJlY29yZCBUcmFuc2FjdGlvblxyXG4gICAgICAgICAgICA8L0J1dHRvbj5cclxuICAgICAgICAgIDwvRm9ybT5cclxuICAgICAgICA8L0NvbD5cclxuICAgICAgICA8Q29sIHhzPXsxMn0gbWQ9ezZ9IGNsYXNzTmFtZT1cIm15LTNcIj5cclxuICAgICAgICAgIDxoNCBjbGFzc05hbWU9XCJteS0zXCI+VHJhbnNhY3Rpb25zIE92ZXJ2aWV3OjwvaDQ+XHJcbiAgICAgICAgICB7cmVjb3Jkc0FycmF5fVxyXG4gICAgICAgIDwvQ29sPlxyXG4gICAgICA8L1Jvdz5cclxuICAgIDwvQ29udGFpbmVyPlxyXG4gICk7XHJcbn1cclxuIiwiaW1wb3J0IFJlYWN0IGZyb20gXCJyZWFjdFwiO1xyXG5jb25zdCBVc2VyQ29udGV4dCA9IFJlYWN0LmNyZWF0ZUNvbnRleHQoKTtcclxuZXhwb3J0IGNvbnN0IFVzZXJQcm92aWRlciA9IFVzZXJDb250ZXh0LlByb3ZpZGVyO1xyXG5leHBvcnQgZGVmYXVsdCBVc2VyQ29udGV4dDtcclxuIiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwibmV4dC9yb3V0ZXJcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwicmVhY3RcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwicmVhY3QtYm9vdHN0cmFwXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlYWN0L2pzeC1kZXYtcnVudGltZVwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJzd2VldGFsZXJ0MlwiKTsiXSwic291cmNlUm9vdCI6IiJ9