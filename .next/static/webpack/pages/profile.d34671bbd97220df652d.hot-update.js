webpackHotUpdate_N_E("pages/profile",{

/***/ "./pages/profile/index.js":
/*!********************************!*\
  !*** ./pages/profile/index.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Profile; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-bootstrap */ "./node_modules/react-bootstrap/esm/index.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! next/router */ "./node_modules/next/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _userContext__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../userContext */ "./userContext.js");


var _jsxFileName = "E:\\batch-92\\capstone3\\budget_tracker\\pages\\profile\\index.js",
    _s = $RefreshSig$();




 //import user context


function Profile() {
  _s();

  var _useContext = Object(react__WEBPACK_IMPORTED_MODULE_1__["useContext"])(_userContext__WEBPACK_IMPORTED_MODULE_5__["default"]),
      user = _useContext.user;

  console.log(user); //State for token

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])({
    setUserToken: null
  }),
      userToken = _useState[0],
      setUserToken = _useState[1]; //useEffect for the Token


  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    setUserToken(localStorage.getItem("token"));
  }, []);
  console.log(userToken); //State for the User details

  var _useState2 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      userRecord = _useState2[0],
      setUserRecord = _useState2[1]; //useEffect for User Details


  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    fetch("http://localhost:8000/api/users/details", {
      headers: {
        Authorization: "Bearer ".concat(localStorage.getItem("token"))
      }
    }).then(function (res) {
      return res.json();
    }).then(function (data) {
      console.log(data);
      setUserRecord(data);
    });
  }, []);
  console.log(userRecord);
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Container"], {
    className: "my-50",
    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Jumbotron"], {
      className: "text-center bg-info",
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h3", {
        children: ["First Name: ", userRecord.firstName]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 57,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h3", {
        children: ["Last Name: ", userRecord.lastName]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 58,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h3", {
        children: ["Email: ", userRecord.email]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 59,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h3", {
        children: ["Current Balance: PHP: ", userRecord.balance]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 60,
        columnNumber: 9
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 56,
      columnNumber: 7
    }, this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 55,
    columnNumber: 5
  }, this);
}

_s(Profile, "robq/DRBedPsrayYBkKZgoVH5GQ=");

_c = Profile;

var _c;

$RefreshReg$(_c, "Profile");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../node_modules/next/dist/compiled/webpack/harmony-module.js */ "./node_modules/next/dist/compiled/webpack/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vcGFnZXMvcHJvZmlsZS9pbmRleC5qcyJdLCJuYW1lcyI6WyJQcm9maWxlIiwidXNlQ29udGV4dCIsIlVzZXJDb250ZXh0IiwidXNlciIsImNvbnNvbGUiLCJsb2ciLCJ1c2VTdGF0ZSIsInNldFVzZXJUb2tlbiIsInVzZXJUb2tlbiIsInVzZUVmZmVjdCIsImxvY2FsU3RvcmFnZSIsImdldEl0ZW0iLCJ1c2VyUmVjb3JkIiwic2V0VXNlclJlY29yZCIsImZldGNoIiwiaGVhZGVycyIsIkF1dGhvcml6YXRpb24iLCJ0aGVuIiwicmVzIiwianNvbiIsImRhdGEiLCJmaXJzdE5hbWUiLCJsYXN0TmFtZSIsImVtYWlsIiwiYmFsYW5jZSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQVNBO0NBR0E7O0FBQ0E7QUFFZSxTQUFTQSxPQUFULEdBQW1CO0FBQUE7O0FBQUEsb0JBQ2ZDLHdEQUFVLENBQUNDLG9EQUFELENBREs7QUFBQSxNQUN4QkMsSUFEd0IsZUFDeEJBLElBRHdCOztBQUVoQ0MsU0FBTyxDQUFDQyxHQUFSLENBQVlGLElBQVosRUFGZ0MsQ0FJaEM7O0FBSmdDLGtCQU1FRyxzREFBUSxDQUFDO0FBQ3pDQyxnQkFBWSxFQUFFO0FBRDJCLEdBQUQsQ0FOVjtBQUFBLE1BTXpCQyxTQU55QjtBQUFBLE1BTWRELFlBTmMsaUJBVWhDOzs7QUFFQUUseURBQVMsQ0FBQyxZQUFNO0FBQ2RGLGdCQUFZLENBQUNHLFlBQVksQ0FBQ0MsT0FBYixDQUFxQixPQUFyQixDQUFELENBQVo7QUFDRCxHQUZRLEVBRU4sRUFGTSxDQUFUO0FBSUFQLFNBQU8sQ0FBQ0MsR0FBUixDQUFZRyxTQUFaLEVBaEJnQyxDQWtCaEM7O0FBbEJnQyxtQkFtQklGLHNEQUFRLENBQUMsRUFBRCxDQW5CWjtBQUFBLE1BbUJ6Qk0sVUFuQnlCO0FBQUEsTUFtQmJDLGFBbkJhLGtCQXFCaEM7OztBQUNBSix5REFBUyxDQUFDLFlBQU07QUFDZEssU0FBSyxDQUFDLHlDQUFELEVBQTRDO0FBQy9DQyxhQUFPLEVBQUU7QUFDUEMscUJBQWEsbUJBQVlOLFlBQVksQ0FBQ0MsT0FBYixDQUFxQixPQUFyQixDQUFaO0FBRE47QUFEc0MsS0FBNUMsQ0FBTCxDQUtHTSxJQUxILENBS1EsVUFBQ0MsR0FBRDtBQUFBLGFBQVNBLEdBQUcsQ0FBQ0MsSUFBSixFQUFUO0FBQUEsS0FMUixFQU1HRixJQU5ILENBTVEsVUFBQ0csSUFBRCxFQUFVO0FBQ2RoQixhQUFPLENBQUNDLEdBQVIsQ0FBWWUsSUFBWjtBQUNBUCxtQkFBYSxDQUFDTyxJQUFELENBQWI7QUFDRCxLQVRIO0FBVUQsR0FYUSxFQVdOLEVBWE0sQ0FBVDtBQWFBaEIsU0FBTyxDQUFDQyxHQUFSLENBQVlPLFVBQVo7QUFFQSxzQkFDRSxxRUFBQyx5REFBRDtBQUFXLGFBQVMsRUFBQyxPQUFyQjtBQUFBLDJCQUNFLHFFQUFDLHlEQUFEO0FBQVcsZUFBUyxFQUFDLHFCQUFyQjtBQUFBLDhCQUNFO0FBQUEsbUNBQWlCQSxVQUFVLENBQUNTLFNBQTVCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQURGLGVBRUU7QUFBQSxrQ0FBZ0JULFVBQVUsQ0FBQ1UsUUFBM0I7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBRkYsZUFHRTtBQUFBLDhCQUFZVixVQUFVLENBQUNXLEtBQXZCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQUhGLGVBSUU7QUFBQSw2Q0FBMkJYLFVBQVUsQ0FBQ1ksT0FBdEM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBSkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxVQURGO0FBVUQ7O0dBL0N1QnhCLE87O0tBQUFBLE8iLCJmaWxlIjoic3RhdGljL3dlYnBhY2svcGFnZXMvcHJvZmlsZS5kMzQ2NzFiYmQ5NzIyMGRmNjUyZC5ob3QtdXBkYXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgdXNlU3RhdGUsIHVzZUVmZmVjdCwgdXNlQ29udGV4dCB9IGZyb20gXCJyZWFjdFwiO1xyXG5pbXBvcnQge1xyXG4gIEZvcm0sXHJcbiAgQnV0dG9uLFxyXG4gIENvbnRhaW5lcixcclxuICBSb3csXHJcbiAgQ29sLFxyXG4gIENhcmQsXHJcbiAgSnVtYm90cm9uLFxyXG59IGZyb20gXCJyZWFjdC1ib290c3RyYXBcIjtcclxuaW1wb3J0IFN3YWwgZnJvbSBcInN3ZWV0YWxlcnQyXCI7XHJcblxyXG5pbXBvcnQgUm91dGVyIGZyb20gXCJuZXh0L3JvdXRlclwiO1xyXG4vL2ltcG9ydCB1c2VyIGNvbnRleHRcclxuaW1wb3J0IFVzZXJDb250ZXh0IGZyb20gXCIuLi8uLi91c2VyQ29udGV4dFwiO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gUHJvZmlsZSgpIHtcclxuICBjb25zdCB7IHVzZXIgfSA9IHVzZUNvbnRleHQoVXNlckNvbnRleHQpO1xyXG4gIGNvbnNvbGUubG9nKHVzZXIpO1xyXG5cclxuICAvL1N0YXRlIGZvciB0b2tlblxyXG5cclxuICBjb25zdCBbdXNlclRva2VuLCBzZXRVc2VyVG9rZW5dID0gdXNlU3RhdGUoe1xyXG4gICAgc2V0VXNlclRva2VuOiBudWxsLFxyXG4gIH0pO1xyXG5cclxuICAvL3VzZUVmZmVjdCBmb3IgdGhlIFRva2VuXHJcblxyXG4gIHVzZUVmZmVjdCgoKSA9PiB7XHJcbiAgICBzZXRVc2VyVG9rZW4obG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJ0b2tlblwiKSk7XHJcbiAgfSwgW10pO1xyXG5cclxuICBjb25zb2xlLmxvZyh1c2VyVG9rZW4pO1xyXG5cclxuICAvL1N0YXRlIGZvciB0aGUgVXNlciBkZXRhaWxzXHJcbiAgY29uc3QgW3VzZXJSZWNvcmQsIHNldFVzZXJSZWNvcmRdID0gdXNlU3RhdGUoW10pO1xyXG5cclxuICAvL3VzZUVmZmVjdCBmb3IgVXNlciBEZXRhaWxzXHJcbiAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgIGZldGNoKFwiaHR0cDovL2xvY2FsaG9zdDo4MDAwL2FwaS91c2Vycy9kZXRhaWxzXCIsIHtcclxuICAgICAgaGVhZGVyczoge1xyXG4gICAgICAgIEF1dGhvcml6YXRpb246IGBCZWFyZXIgJHtsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcInRva2VuXCIpfWAsXHJcbiAgICAgIH0sXHJcbiAgICB9KVxyXG4gICAgICAudGhlbigocmVzKSA9PiByZXMuanNvbigpKVxyXG4gICAgICAudGhlbigoZGF0YSkgPT4ge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKGRhdGEpO1xyXG4gICAgICAgIHNldFVzZXJSZWNvcmQoZGF0YSk7XHJcbiAgICAgIH0pO1xyXG4gIH0sIFtdKTtcclxuXHJcbiAgY29uc29sZS5sb2codXNlclJlY29yZCk7XHJcblxyXG4gIHJldHVybiAoXHJcbiAgICA8Q29udGFpbmVyIGNsYXNzTmFtZT1cIm15LTUwXCI+XHJcbiAgICAgIDxKdW1ib3Ryb24gY2xhc3NOYW1lPVwidGV4dC1jZW50ZXIgYmctaW5mb1wiPlxyXG4gICAgICAgIDxoMz5GaXJzdCBOYW1lOiB7dXNlclJlY29yZC5maXJzdE5hbWV9PC9oMz5cclxuICAgICAgICA8aDM+TGFzdCBOYW1lOiB7dXNlclJlY29yZC5sYXN0TmFtZX08L2gzPlxyXG4gICAgICAgIDxoMz5FbWFpbDoge3VzZXJSZWNvcmQuZW1haWx9PC9oMz5cclxuICAgICAgICA8aDM+Q3VycmVudCBCYWxhbmNlOiBQSFA6IHt1c2VyUmVjb3JkLmJhbGFuY2V9PC9oMz5cclxuICAgICAgPC9KdW1ib3Ryb24+XHJcbiAgICA8L0NvbnRhaW5lcj5cclxuICApO1xyXG59XHJcbiJdLCJzb3VyY2VSb290IjoiIn0=