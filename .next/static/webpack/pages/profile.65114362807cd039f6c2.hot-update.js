webpackHotUpdate_N_E("pages/profile",{

/***/ "./pages/profile/index.js":
/*!********************************!*\
  !*** ./pages/profile/index.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Profile; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-bootstrap */ "./node_modules/react-bootstrap/esm/index.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! next/router */ "./node_modules/next/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _userContext__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../userContext */ "./userContext.js");


var _jsxFileName = "E:\\batch-92\\capstone3\\budget_tracker\\pages\\profile\\index.js",
    _s = $RefreshSig$();




 //import user context


function Profile() {
  _s();

  var _useContext = Object(react__WEBPACK_IMPORTED_MODULE_1__["useContext"])(_userContext__WEBPACK_IMPORTED_MODULE_5__["default"]),
      user = _useContext.user;

  console.log(user); //State for token

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])({
    setUserToken: null
  }),
      userToken = _useState[0],
      setUserToken = _useState[1]; //useEffect for the Token


  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    setUserToken(localStorage.getItem("token"));
  }, []);
  console.log(userToken); //State for the User details

  var _useState2 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      userRecord = _useState2[0],
      setUserRecord = _useState2[1]; //useEffect for User Details


  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    fetch("http://localhost:8000/api/users/details", {
      headers: {
        Authorization: "Bearer ".concat(localStorage.getItem("token"))
      }
    }).then(function (res) {
      return res.json();
    }).then(function (data) {
      console.log(data);
      setUserRecord(data);
    });
  }, []);
  console.log(userRecord);
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Jumbotron"], {
    className: "text-center",
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h3", {
      children: ["First Name: ", userRecord.firstName]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 56,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h3", {
      children: ["Last Name: ", userRecord.lastName]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 57,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h3", {
      children: ["Email: ", userRecord.email]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 58,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h3", {
      children: ["Current Balance: PHP: ", userRecord.balance]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 59,
      columnNumber: 7
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 55,
    columnNumber: 5
  }, this);
}

_s(Profile, "robq/DRBedPsrayYBkKZgoVH5GQ=");

_c = Profile;

var _c;

$RefreshReg$(_c, "Profile");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../node_modules/next/dist/compiled/webpack/harmony-module.js */ "./node_modules/next/dist/compiled/webpack/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vcGFnZXMvcHJvZmlsZS9pbmRleC5qcyJdLCJuYW1lcyI6WyJQcm9maWxlIiwidXNlQ29udGV4dCIsIlVzZXJDb250ZXh0IiwidXNlciIsImNvbnNvbGUiLCJsb2ciLCJ1c2VTdGF0ZSIsInNldFVzZXJUb2tlbiIsInVzZXJUb2tlbiIsInVzZUVmZmVjdCIsImxvY2FsU3RvcmFnZSIsImdldEl0ZW0iLCJ1c2VyUmVjb3JkIiwic2V0VXNlclJlY29yZCIsImZldGNoIiwiaGVhZGVycyIsIkF1dGhvcml6YXRpb24iLCJ0aGVuIiwicmVzIiwianNvbiIsImRhdGEiLCJmaXJzdE5hbWUiLCJsYXN0TmFtZSIsImVtYWlsIiwiYmFsYW5jZSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQVNBO0NBR0E7O0FBQ0E7QUFFZSxTQUFTQSxPQUFULEdBQW1CO0FBQUE7O0FBQUEsb0JBQ2ZDLHdEQUFVLENBQUNDLG9EQUFELENBREs7QUFBQSxNQUN4QkMsSUFEd0IsZUFDeEJBLElBRHdCOztBQUVoQ0MsU0FBTyxDQUFDQyxHQUFSLENBQVlGLElBQVosRUFGZ0MsQ0FJaEM7O0FBSmdDLGtCQU1FRyxzREFBUSxDQUFDO0FBQ3pDQyxnQkFBWSxFQUFFO0FBRDJCLEdBQUQsQ0FOVjtBQUFBLE1BTXpCQyxTQU55QjtBQUFBLE1BTWRELFlBTmMsaUJBVWhDOzs7QUFFQUUseURBQVMsQ0FBQyxZQUFNO0FBQ2RGLGdCQUFZLENBQUNHLFlBQVksQ0FBQ0MsT0FBYixDQUFxQixPQUFyQixDQUFELENBQVo7QUFDRCxHQUZRLEVBRU4sRUFGTSxDQUFUO0FBSUFQLFNBQU8sQ0FBQ0MsR0FBUixDQUFZRyxTQUFaLEVBaEJnQyxDQWtCaEM7O0FBbEJnQyxtQkFtQklGLHNEQUFRLENBQUMsRUFBRCxDQW5CWjtBQUFBLE1BbUJ6Qk0sVUFuQnlCO0FBQUEsTUFtQmJDLGFBbkJhLGtCQXFCaEM7OztBQUNBSix5REFBUyxDQUFDLFlBQU07QUFDZEssU0FBSyxDQUFDLHlDQUFELEVBQTRDO0FBQy9DQyxhQUFPLEVBQUU7QUFDUEMscUJBQWEsbUJBQVlOLFlBQVksQ0FBQ0MsT0FBYixDQUFxQixPQUFyQixDQUFaO0FBRE47QUFEc0MsS0FBNUMsQ0FBTCxDQUtHTSxJQUxILENBS1EsVUFBQ0MsR0FBRDtBQUFBLGFBQVNBLEdBQUcsQ0FBQ0MsSUFBSixFQUFUO0FBQUEsS0FMUixFQU1HRixJQU5ILENBTVEsVUFBQ0csSUFBRCxFQUFVO0FBQ2RoQixhQUFPLENBQUNDLEdBQVIsQ0FBWWUsSUFBWjtBQUNBUCxtQkFBYSxDQUFDTyxJQUFELENBQWI7QUFDRCxLQVRIO0FBVUQsR0FYUSxFQVdOLEVBWE0sQ0FBVDtBQWFBaEIsU0FBTyxDQUFDQyxHQUFSLENBQVlPLFVBQVo7QUFFQSxzQkFDRSxxRUFBQyx5REFBRDtBQUFXLGFBQVMsRUFBQyxhQUFyQjtBQUFBLDRCQUNFO0FBQUEsaUNBQWlCQSxVQUFVLENBQUNTLFNBQTVCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQURGLGVBRUU7QUFBQSxnQ0FBZ0JULFVBQVUsQ0FBQ1UsUUFBM0I7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBRkYsZUFHRTtBQUFBLDRCQUFZVixVQUFVLENBQUNXLEtBQXZCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQUhGLGVBSUU7QUFBQSwyQ0FBMkJYLFVBQVUsQ0FBQ1ksT0FBdEM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBSkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFVBREY7QUFRRDs7R0E3Q3VCeEIsTzs7S0FBQUEsTyIsImZpbGUiOiJzdGF0aWMvd2VicGFjay9wYWdlcy9wcm9maWxlLjY1MTE0MzYyODA3Y2QwMzlmNmMyLmhvdC11cGRhdGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyB1c2VTdGF0ZSwgdXNlRWZmZWN0LCB1c2VDb250ZXh0IH0gZnJvbSBcInJlYWN0XCI7XHJcbmltcG9ydCB7XHJcbiAgRm9ybSxcclxuICBCdXR0b24sXHJcbiAgQ29udGFpbmVyLFxyXG4gIFJvdyxcclxuICBDb2wsXHJcbiAgQ2FyZCxcclxuICBKdW1ib3Ryb24sXHJcbn0gZnJvbSBcInJlYWN0LWJvb3RzdHJhcFwiO1xyXG5pbXBvcnQgU3dhbCBmcm9tIFwic3dlZXRhbGVydDJcIjtcclxuXHJcbmltcG9ydCBSb3V0ZXIgZnJvbSBcIm5leHQvcm91dGVyXCI7XHJcbi8vaW1wb3J0IHVzZXIgY29udGV4dFxyXG5pbXBvcnQgVXNlckNvbnRleHQgZnJvbSBcIi4uLy4uL3VzZXJDb250ZXh0XCI7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBQcm9maWxlKCkge1xyXG4gIGNvbnN0IHsgdXNlciB9ID0gdXNlQ29udGV4dChVc2VyQ29udGV4dCk7XHJcbiAgY29uc29sZS5sb2codXNlcik7XHJcblxyXG4gIC8vU3RhdGUgZm9yIHRva2VuXHJcblxyXG4gIGNvbnN0IFt1c2VyVG9rZW4sIHNldFVzZXJUb2tlbl0gPSB1c2VTdGF0ZSh7XHJcbiAgICBzZXRVc2VyVG9rZW46IG51bGwsXHJcbiAgfSk7XHJcblxyXG4gIC8vdXNlRWZmZWN0IGZvciB0aGUgVG9rZW5cclxuXHJcbiAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgIHNldFVzZXJUb2tlbihsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcInRva2VuXCIpKTtcclxuICB9LCBbXSk7XHJcblxyXG4gIGNvbnNvbGUubG9nKHVzZXJUb2tlbik7XHJcblxyXG4gIC8vU3RhdGUgZm9yIHRoZSBVc2VyIGRldGFpbHNcclxuICBjb25zdCBbdXNlclJlY29yZCwgc2V0VXNlclJlY29yZF0gPSB1c2VTdGF0ZShbXSk7XHJcblxyXG4gIC8vdXNlRWZmZWN0IGZvciBVc2VyIERldGFpbHNcclxuICB1c2VFZmZlY3QoKCkgPT4ge1xyXG4gICAgZmV0Y2goXCJodHRwOi8vbG9jYWxob3N0OjgwMDAvYXBpL3VzZXJzL2RldGFpbHNcIiwge1xyXG4gICAgICBoZWFkZXJzOiB7XHJcbiAgICAgICAgQXV0aG9yaXphdGlvbjogYEJlYXJlciAke2xvY2FsU3RvcmFnZS5nZXRJdGVtKFwidG9rZW5cIil9YCxcclxuICAgICAgfSxcclxuICAgIH0pXHJcbiAgICAgIC50aGVuKChyZXMpID0+IHJlcy5qc29uKCkpXHJcbiAgICAgIC50aGVuKChkYXRhKSA9PiB7XHJcbiAgICAgICAgY29uc29sZS5sb2coZGF0YSk7XHJcbiAgICAgICAgc2V0VXNlclJlY29yZChkYXRhKTtcclxuICAgICAgfSk7XHJcbiAgfSwgW10pO1xyXG5cclxuICBjb25zb2xlLmxvZyh1c2VyUmVjb3JkKTtcclxuXHJcbiAgcmV0dXJuIChcclxuICAgIDxKdW1ib3Ryb24gY2xhc3NOYW1lPVwidGV4dC1jZW50ZXJcIj5cclxuICAgICAgPGgzPkZpcnN0IE5hbWU6IHt1c2VyUmVjb3JkLmZpcnN0TmFtZX08L2gzPlxyXG4gICAgICA8aDM+TGFzdCBOYW1lOiB7dXNlclJlY29yZC5sYXN0TmFtZX08L2gzPlxyXG4gICAgICA8aDM+RW1haWw6IHt1c2VyUmVjb3JkLmVtYWlsfTwvaDM+XHJcbiAgICAgIDxoMz5DdXJyZW50IEJhbGFuY2U6IFBIUDoge3VzZXJSZWNvcmQuYmFsYW5jZX08L2gzPlxyXG4gICAgPC9KdW1ib3Ryb24+XHJcbiAgKTtcclxufVxyXG4iXSwic291cmNlUm9vdCI6IiJ9