import Head from "next/head";
import styles from "../styles/Home.module.css";
import NavBar from "../components/NavBar";
import { Container, Form, Button } from "react-bootstrap";
// import Footer from "../components/Footer";
import { Fragment } from "react";
import "bootstrap/dist/css/bootstrap.min.css";

export default function Home() {
  return (
    <Fragment>
      <Container>
        <div className={styles.body}>
          <div className={styles.container}>
            <main className={styles.main}>
              <h1 className={styles.title}>&Xi;</h1>

              <p className={styles.description}>Shop</p>

              <h2>Welcome to &Xi;-Shop,A Budget Tracking App</h2>
            </main>
          </div>
        </div>
      </Container>
    </Fragment>
  );
}
