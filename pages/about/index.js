import { Jumbotron, Button, Container, Row, Col, Card } from "react-bootstrap";
import UserContext from "../../UserContext";
import Head from "next/head";
import NavBar from "../../components/NavBar";
// import Footer from "../components/Footer";
import { Fragment } from "react";
import "bootstrap/dist/css/bootstrap.min.css";

export default function About() {
  return (
    <Fragment>
      <Jumbotron className="bg-info">
        <h1 className="text-center ">What About &Xi; Shop</h1>
      </Jumbotron>
      <Container>
        <Row>
          <Col>
            <Card className="cardHighlight">
              <Card.Body>
                <Card.Title>
                  <h2 className="text-center">Budget Tracking App </h2>
                </Card.Title>
                <Card.Text>
                  This Budget Tracking App is made Possible by the compilations
                  of knowledge that is shared by Zuitt! It shows Dedication,
                  Focus and Passion. This App allows you to compile all incomes
                  and expenses so that you could track Every cent of your
                  savings. It will test your discpline and ability to SAVE your
                  Kwarta! .
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
          <Col>
            <Card className="cardHighlight">
              <Card.Body>
                <Card.Title>
                  <h2 className="text-center">Logo</h2>
                </Card.Title>
                <Card.Text>
                  The Logo Stands for Three Lines that mostly describes me, it
                  also shows the letter "E" Which is the First Letter of my
                  Name. The Three Lines Shows Dedication, Focus, And Passion in
                  which empowers an individual to give its Full Potential to the
                  task or challenge being presented. So Be the Letter E
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
          <Col>
            <Card className="cardHighlight">
              <Card.Body>
                <Card.Title>
                  <h2 className="text-center">Creator</h2>
                </Card.Title>
                <Card.Text>
                  Hi! Im Ethan Bayaca, I am an aspiring Software Engineer, A
                  Petroleum Engineering Student and a Chef in the House. I
                  always wanted to learn the art of Computer Technology and this
                  is the time i take a leap of faith to somehow cope with the
                  Worlds Struggle and develop my skills so that no crisis could
                  hold me back.
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    </Fragment>
  );
}
