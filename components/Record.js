import { useState, useEffect, useContext } from "react";
import { Card } from "react-bootstrap";

// To get the prop, destructure it
export default function Category({ recordProp }) {
  //  after you get the prop, destructure it para makuha mo yng laman na need mo:
  const {
    _id,
    name,
    type,
    category,
    balance,
    recordedOn,
    description,
    amount,
  } = recordProp;

  return (
    <Card className="my-3">
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Text>{type}</Card.Text>
        <Card.Title>{category}</Card.Title>
        <Card.Text>{description}</Card.Text>
        <Card.Text>{amount}</Card.Text>
        <Card.Text>{balance}</Card.Text>
        <Card.Text>{recordedOn}</Card.Text>
      </Card.Body>
    </Card>
  );
}
